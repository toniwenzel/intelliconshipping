﻿CREATE TABLE dbo.icPaketversandConfiguration
	(
	KeyName varchar(50) NOT NULL,
	Value varchar(400) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.icPaketversandConfiguration ADD CONSTRAINT
	PK_icPaketversandConfiguration PRIMARY KEY CLUSTERED
	(
	KeyName
	) ON [PRIMARY]
