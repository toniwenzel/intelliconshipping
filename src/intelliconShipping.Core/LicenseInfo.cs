﻿namespace intelliconShipping.Core
{
    public class LicenseInfo
    {
        public string? CustomerNumber { get; set; }
        public string? CustomerName { get; set; }
    }
}
