﻿namespace intelliconShipping.Core
{
    /// <summary>
    /// Generic result class containing the check/validation result and an optional error message
    /// </summary>
    public class CheckResult
    {
        public bool IsValid { get; set; }
        public string Error { get; set; }

        public CheckResult(bool isValid, string error)
        {
            IsValid = isValid;
            Error = error;
        }
    }
}
