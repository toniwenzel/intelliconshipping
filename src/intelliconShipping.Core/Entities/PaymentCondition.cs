﻿using System.ComponentModel.DataAnnotations.Schema;

namespace intelliconShipping.Core.Entities
{
    [Table("KHKVKBelegeZKD")]
    public class PaymentCondition
    {
        [Column("BelID")]
        public int ReceiptId { get; set; }

        [Column("Mandant")]
        public short Mandant { get; set; }

        [Column("Zahlungskond")]
        public string? Condition { get; set; }
    }
}
