﻿namespace intelliconShipping.Core.Entities
{
    public interface IReceipt
    {
        int Id { get; set; }

        short Mandant { get; set; }
    }
}
