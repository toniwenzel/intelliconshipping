﻿namespace intelliconShipping.Core.Entities
{
    public class AdditionalAddressData : AdditionalData
    {
        public AdditionalAddressData(Dictionary<string, object> data)
            : base(data)
        {

        }

        public string? Email => Get<string>("EMail");
        public int ShippingStyle => Get<int>("USER_Versandeinstellung");

        public string? ShippingEmail => Get<string>("USER_EMailVersandnachricht");

    }
}
