﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intelliconShipping.Core.Entities
{
    [Table("icPaketversandConfiguration")]
    public class ConfigurationValue
    {
        [Key]
        public string? KeyName { get; set; }
        public string? Value { get; set; }
    }
}
