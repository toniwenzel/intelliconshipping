﻿using System.Text;

namespace intelliconShipping.Core.Entities
{
    public class Parcel
    {
        public int ReceiptId { get; set; }
        public string ReceiptNumber { get; set; }
        public int Mandant { get; set; }
        public bool IsPurchase { get; set; }

        public List<Package> Packages { get; } = new List<Package>();

        public string? Saluation { get; set; }
        public string? Name1 { get; set; }
        public string? Name2 { get; set; }
        public string? Name3 { get; set; }
        public string? Name4 { get; set; }

        public string? Additional { get; set; }
        public string? Street { get; set; }
        public string? City { get; set; }
        public string? PostCode { get; set; }
        public string? Country { get; set; }
        public string? Currency { get; set; }
        public int PreviousId { get; set; }
        public string? ReferenceSign { get; set; }
        public string? ReferenceNumber { get; set; }

        public string Reference => $"{ReferenceSign} {ReferenceNumber}".Trim();

        public string? Phone { get; set; }
        public string? PhoneSource { get; set; }
        public string? ContactPerson { get; set; }
        public long ContactPersonId { get; set; }
        public bool UserAddressFields { get; set; }
        public string? CustomerNumber { get; set; }
        public string? ShippingType { get; set; }
        public string? DeliveryCondition { get; set; }
        public string? Error { get; set; }
        /// <summary>
        /// Zahlungskondition; zur Erkennung von Nachnahme aus diesem Feld
        /// </summary>
        public string? PaymentCondition { get; set; }
        public DateTime DeliveryDate { get; set; } //dtLiefertermin
        public string? Email { get; set; }
        public decimal DebtAmount { get; set; } //cInkassobetrag Endbetrag / Betrag f�r Nachnahmeinkasso
        public bool IsDebtDelivery => GetIsDebtDelivery();

        public string? EmailAddressForDelivery { get; set; }  //sEMailadresseZustellNachricht As String  'EMail Adresse f�r Nachricht �ber erfolgreiche Zustellung
        public string? EmailAddressForShipping { get; set; } // sEMailadresseNachricht As String        'EMail Adresse für Nachricht �ber Versand (Zustellnachricht)
        public string? EmailAddressForFailedShipping { get; set; } // sEMailadresseFehlerNachricht As String  'EMail Adresse f�r Nachricht �ber Fehler beim Versand

        public Dictionary<string, string> UserFields { get; } = new Dictionary<string, string>();

        public Parcel()
        {
        }

        public Parcel(IReceipt receipt)
        {
            IsPurchase = receipt is PurchaseReceipt;
            Mandant = receipt.Mandant;
            ReceiptId = receipt.Id;
        }

        public string GetDeliveryAddress(bool withReceiptNumber = true, bool withPhoneNumber = true)
        {
            var builder = new StringBuilder();
            var hasDeliveryAddress = !(string.IsNullOrWhiteSpace(Saluation)
                && string.IsNullOrWhiteSpace(Name1)
                && string.IsNullOrWhiteSpace(Name2)
                && string.IsNullOrWhiteSpace(Name3)
                && string.IsNullOrWhiteSpace(Name4)
                && string.IsNullOrWhiteSpace(Street)
                && string.IsNullOrWhiteSpace(PostCode)
                && string.IsNullOrWhiteSpace(City)
                && string.IsNullOrWhiteSpace(Additional));

            if (withReceiptNumber && !string.IsNullOrWhiteSpace(ReceiptNumber))
                builder.AppendLine(ReceiptNumber);

            if (!string.IsNullOrWhiteSpace(Saluation))
                builder.AppendLine(Saluation);

            if (!string.IsNullOrWhiteSpace(Name1))
                builder.AppendLine(Name1);

            if (!string.IsNullOrWhiteSpace(Name2))
                builder.AppendLine(Name2);

            if (!string.IsNullOrWhiteSpace(Name3))
                builder.AppendLine(Name3);

            if (!string.IsNullOrWhiteSpace(Name4))
                builder.AppendLine(Name4);

            if (!string.IsNullOrWhiteSpace(Additional))
                builder.AppendLine(Additional);

            if (!string.IsNullOrWhiteSpace(Street))
                builder.AppendLine(Street);

            //  Keine Anschrift, dann auch Land leer lassen
            if (hasDeliveryAddress && !string.IsNullOrWhiteSpace(Country))
                builder.Append($"{Country} ");

            if (!string.IsNullOrWhiteSpace(PostCode))
                builder.Append($"{PostCode} ");

            if (!string.IsNullOrWhiteSpace(City))
                builder.AppendLine(City);

            if (withPhoneNumber)
                builder.Append($"Telefon: {Phone}");

            return builder.ToString().Trim();
        }

        private bool GetIsDebtDelivery()
        {
            var result = ShippingType?.Contains("COD") == true || ShippingType?.Contains("NN") == true || ShippingType?.Contains("Nachnahme") == true;

            if (!result)
                result = DeliveryCondition?.Contains("COD") == true || DeliveryCondition?.Contains("NN") == true || DeliveryCondition?.Contains("Nachnahme") == true;

            if (!result)
                result = PaymentCondition?.Contains("COD") == true || PaymentCondition?.Contains("NN") == true || PaymentCondition?.Contains("Nachnahme") == true;

            return result;
        }

        public void CalculateDebtPerPackage()
        {
            var amountPerPackage = Math.Round(DebtAmount / Packages.Count);
            var remaining = DebtAmount;

            for (var i = 0; i < Packages.Count; i++)
            {
                if (i == Packages.Count - 1)
                {
                    Packages[i].DebtAmount = remaining;
                }
                else
                {
                    Packages[i].DebtAmount = amountPerPackage;
                    remaining -= amountPerPackage;
                }
            }
        }

        public void SetPackagesAmount(int amount)
        {
            var currentPackags = Packages.Count;

            if (amount > currentPackags)
            {
                for (var i = 0; i < amount - currentPackags; i++)
                {
                    Packages.Add(new Package());
                }
            }
            else if (amount < currentPackags)
            {
                for (var i = currentPackags; i > amount; i--)
                {
                    Packages.RemoveAt(i - 1);
                }
            }

            CalculateDebtPerPackage();
        }

        /// <summary>
        /// Gets the tota weight of all packages
        /// </summary>
        /// <returns></returns>
        public decimal GetTotalWeight()
        {
            return Packages.Sum(p => p.Weight);
        }

        //Public nVersandeinstellung As Integer   'Multibox

        //Public lVorID As Long

        //Public dtZeitpunkt As Date
        //Public sBearbeiter As String
        //Public sVersanddienst As String
        //Public sTarif As String   'Tarifinformation

        //Public sAbsendername As String    'Mandantenname
        //Public sEMailNachricht As String
        //'ip neu 4.7.18





        //Public nAnzahlpakete As Integer


        //Public nTmpBelegjahr As Integer
        //Public lTmpBelegnummer As Long


        //Dim sDateiname As String
        //Dim sVerzeichnis As String
        //Dim sEndung As String

        //Public cSendungsgewicht As Currency  'Summe aller Pakete

        //Public bEmailFehler As Boolean

        //'werte aus Versandeinstellungen
        //Public sService As String
        //Public sProdukt As String
        //Public sVerfahren As String
        //Public sSonderservice As String
        //Public sZollinhaltserklaerung As String
        //Public sTeilnahme As String
        //Public sFreifeld1 As String
        //Public sFreifeld2 As String
        //Public sFreifeld3 As String
    }

    public class Package
    {
        public decimal Weight { get; set; }
        public decimal DebtAmount { get; set; }
        public decimal Length { get; set; }
        public decimal Width { get; set; }
        public decimal Height { get; set; }
        public string? Number { get; set; }
    }
}
