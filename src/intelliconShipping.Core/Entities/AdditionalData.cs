﻿namespace intelliconShipping.Core.Entities
{
    public class AdditionalData
    {
        protected readonly Dictionary<string, object> _data;

        public AdditionalData(Dictionary<string, object> data)
        {
            _data = data ?? throw new ArgumentNullException(nameof(data));
        }

        public T Get<T>(string columnName)
        {
            if (_data.TryGetValue(columnName, out var value))
                return (T)value;

            return default;
        }

        public bool HasColumn(string columnName) => _data.ContainsKey(columnName);

        public IEnumerable<string> GetColumnNames() => _data.Keys;
    }
}
