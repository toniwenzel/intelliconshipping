﻿using System.ComponentModel.DataAnnotations.Schema;

namespace intelliconShipping.Core.Entities
{
    [Table("KHKAdressen")]
    public class Address
    {
        [Column("Adresse")]
        public int Id { get; set; }

        [Column("Mandant")]
        public short Mandant { get; set; }

        [Column("Telefon")]
        public string? Phone { get; set; }

        [Column("EMail")]
        public string? Mail { get; set; }
    }
}
