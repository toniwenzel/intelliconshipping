﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace intelliconShipping.Core.Entities
{
    [Table("KHKGruppen")]
    public class ShippingType
    {
        [Column("Gruppe")]
        [Key]
        public string Type { get; set; }

        [Column("Bezeichnung")]
        public string Description { get; set; }
    }
}
