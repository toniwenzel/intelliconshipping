﻿using System.ComponentModel.DataAnnotations.Schema;

namespace intelliconShipping.Core.Entities
{
    [Table("KHKEKBelege")]
    public class PurchaseReceipt : IReceipt
    {
        [Column("BelID")]
        public int Id { get; set; }

        [Column("Mandant")]
        public short Mandant { get; set; }

        [Column("Belegnummer")]
        public int? Number { get; set; }

        [Column("Belegjahr")]
        public short? Year { get; set; }

        [Column("A1Name1")]
        public string? Name1 { get; set; }

        [Column("A1Name2")]
        public string? Name2 { get; set; }

        [Column("A1Zusatz")]
        public string? Additional { get; set; }
        [Column("A1Strasse")]
        public string? Street { get; set; }

        [Column("A1Ort")]
        public string? City { get; set; }
        [Column("A1PLZ")]
        public string? PostCode { get; set; }
        [Column("A1Land")]
        public string? Country { get; set; }
        [Column("WKz")]
        public string? Currency { get; set; }
        [Column("A1Anrede")]
        public string? Saluation { get; set; }
        [Column("A0Empfaenger")]
        public string? CustomerNumber { get; set; }
        // ---
        [Column("A0Strasse")]
        public string? CustomerStreet { get; set; }
        [Column("A0Ort")]
        public string? CustomerCity { get; set; }
        [Column("A0PLZ")]
        public string? CustomerPostCode { get; set; }
        [Column("A0Land")]
        public string? CustomerCountry { get; set; }
        [Column("A0Anrede")]
        public string? CustomerSaluation { get; set; }
        [Column("A0Name1")]
        public string? CustomerName1 { get; set; }
        [Column("A0Name2")]
        public string? CustomerName2 { get; set; }
        [Column("A0Zusatz")]
        public string? CustomerAdditional { get; set; }
    }
}
