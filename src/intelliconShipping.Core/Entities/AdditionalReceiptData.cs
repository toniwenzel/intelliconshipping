﻿namespace intelliconShipping.Core.Entities
{
    public class AdditionalReceiptData : AdditionalData
    {
        public AdditionalReceiptData(Dictionary<string, object> data)
            : base(data)
        {

        }

        public int Id => Get<int>("BelID");
        public short Mandant => Get<short>("Mandant");
        public string? Name1 => Get<string>("USER_A1Name1");
        public string? Name2 => Get<string>("USER_A1Name2");
        public string? Name3 => Get<string>("USER_A1Name3");
        public string? Name4 => Get<string>("USER_A1Name4");
        public string? Additional => Get<string>("USER_A1Zusatz");
        public string? Saluation => Get<string>("USER_A1Anrede");
        public string? ContactPerson => Get<string>("USER_A1Ansprechpartner");
        public long ContactPersonId => Get<long>("USER_A1LAAP");
        public string? Street => Get<string>("USER_A1Strasse");
        public string? City => Get<string>("USER_A1Ort");
        public string? PostCode => Get<string>("USER_A1PLZ");
        public string? Country => Get<string>("USER_A1Land");
        public bool ShippingPrintDone => Get<bool>("USER_Versanddruckerledigt");
    }
}
