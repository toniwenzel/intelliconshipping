﻿using System.ComponentModel.DataAnnotations.Schema;

namespace intelliconShipping.Core.Entities
{
    [Table("KHKAnsprechpartner")]
    public class ContactPerson
    {
        [Column("Nummer")]
        public int Number { get; set; }

        [Column("Mandant")]
        public short Mandant { get; set; }

        [Column("Adresse")]
        public int Address { get; set; }

        [Column("Telefon")]
        public string? Phone { get; set; }
        [Column("Ansprechpartner")]
        public string? Name { get; set; }
        [Column("EMail")]
        public string? Email { get; set; }
    }
}
