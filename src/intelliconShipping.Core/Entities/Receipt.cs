﻿using System.ComponentModel.DataAnnotations.Schema;

namespace intelliconShipping.Core.Entities
{
    [Table("KHKVKBelege")]
    public class Receipt : IReceipt
    {
        [Column("BelID")]
        public int Id { get; set; }

        [Column("Mandant")]
        public short Mandant { get; set; }

        [Column("Belegnummer")]
        public int? Number { get; set; }

        [Column("Belegjahr")]
        public short? Year { get; set; }

        [Column("A1Anrede")]
        public string? Saluation { get; set; }

        [Column("A1Name1")]
        public string? Name1 { get; set; }

        [Column("A1Name2")]
        public string? Name2 { get; set; }

        [Column("A1Zusatz")]
        public string? Additional { get; set; }
        [Column("A1Strasse")]
        public string? Street { get; set; }
        [Column("A1Ort")]
        public string? City { get; set; }
        [Column("A1PLZ")]
        public string? PostCode { get; set; }
        [Column("A1Land")]
        public string? Country { get; set; }
        [Column("WKz")]
        public string? Currency { get; set; }
        [Column("VorID")]
        public int PreviousId { get; set; }
        [Column("Referenzzeichen")]
        public string? ReferenceSign { get; set; }
        [Column("Referenznummer")]
        public string? ReferenceNumber { get; set; }
        [Column("A1AdressNr")]
        public int? DestinationAddressId { get; set; }
        [Column("A0Adressnr")]//Auftraggeberadresse
        public int? CustomerAddressId { get; set; }
        [Column("A1Ansprechpartner")]
        public string? ContactPerson { get; set; }
        [Column("A0Empfaenger")]
        public string? CustomerNumber { get; set; }
        [Column("Versand")]
        public string? ShippingType { get; set; }
        [Column("Lieferbedingung")]
        public string? DeliveryCondition { get; set; }
        [Column("Liefertermin")]
        public DateTime? DeliveryDate { get; set; }
        [Column("Gewicht")]
        public decimal? Weight { get; set; }

        /// <summary>A0AnsprechpartnerID</summary>
        [Column("A0AnsprechpartnerID")]
        public int? CustomerContactPersonId { get; set; }

        [Column("Bruttobetrag")]
        public decimal? GrossAmount { get; set; }
    }
}
