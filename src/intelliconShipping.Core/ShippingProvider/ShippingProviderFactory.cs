﻿using intelliconShipping.Core.Services;

namespace intelliconShipping.Core.ShippingProvider
{
    public class ShippingProviderFactory : IShippingProviderFactory
    {
        private readonly IEnumerable<IShippingProvider> _shippingProviders;
        private readonly ILicenseService _licenseService;
        private readonly IDictionary<IShippingProvider, bool> _licenseInformation;

        public ShippingProviderFactory(IEnumerable<IShippingProvider> shippingProviders, ILicenseService licenseService)
        {
            _shippingProviders = shippingProviders ?? throw new ArgumentNullException(nameof(shippingProviders));
            _licenseService = licenseService ?? throw new ArgumentNullException(nameof(licenseService));
            _licenseInformation = new Dictionary<IShippingProvider, bool>();
        }

        public Task<IShippingProvider> CreateShippingProvider(string? name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("Keine Versandart angegeben!", nameof(name));

            foreach (var provider in _shippingProviders)
            {
                if (string.Equals(name, provider.GetProviderName(), StringComparison.OrdinalIgnoreCase))
                {
                    // Check license
                    //if (await IsProviderLicensed(provider).ConfigureAwait(false))
                    return Task.FromResult(provider);
                    //else
                    //    throw new ArgumentException($"Keine gültige Lizenz für die Nutzung der {provider.GetProviderName()}-Anbindung aktiv", nameof(name));
                }
            }

            throw new ArgumentException($"Shipping provider for name '{name}' was not found!", nameof(name));
        }

        private async Task<bool> IsProviderLicensed(IShippingProvider provider)
        {
            if (_licenseInformation.TryGetValue(provider, out var isLicensed))
                return isLicensed;
            else
            {
                var licenseInfo = _licenseService.ReadLicense();
                if (licenseInfo == null)
                    throw new InvalidOperationException("Keine Lizenzinformationen hinterlegt");

                var check = await _licenseService.ValidateLicense(licenseInfo, provider.GetLicenseModuleName());

                _licenseInformation.Add(provider, check.IsValid);

                return check.IsValid;
            }
        }
    }
}
