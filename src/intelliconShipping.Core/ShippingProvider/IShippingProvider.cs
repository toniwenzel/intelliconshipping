﻿using intelliconShipping.Core.Entities;

namespace intelliconShipping.Core.ShippingProvider
{
    public interface IShippingProvider
    {
        string GetProviderName();
        string GetLicenseModuleName();

        Task Ship(Parcel parcel);
    }
}
