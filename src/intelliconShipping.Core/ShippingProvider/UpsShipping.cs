﻿using intelliconShipping.Core.Entities;

namespace intelliconShipping.Core.ShippingProvider
{
    public class UpsShipping : IShippingProvider
    {
        public string GetLicenseModuleName() => "versandmodul_ups";

        public string GetProviderName() => "UPS";

        public Task Ship(Parcel parcel)
        {
            throw new NotImplementedException();
        }
    }
}
