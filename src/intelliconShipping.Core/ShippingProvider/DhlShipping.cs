﻿using intelliconShipping.Core.Entities;

namespace intelliconShipping.Core.ShippingProvider
{
    public class DhlShipping : IShippingProvider
    {
        public string GetLicenseModuleName() => "versandmodul_dhl";

        public string GetProviderName() => "DHL";

        public Task Ship(Parcel parcel)
        {
            throw new NotImplementedException();
        }
    }
}
