﻿namespace intelliconShipping.Core.ShippingProvider
{
    public interface IShippingProviderFactory
    {
        Task<IShippingProvider> CreateShippingProvider(string? name);
    }
}
