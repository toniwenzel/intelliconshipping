﻿using intellicon.Utilities.DataAccess;
using intellicon.Utilities.Entities;
using intellicon.Utilities.Services;
using intelliconShipping.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace intelliconShipping.Core.DataAccess
{
    public class ShippingDbContext : DbContext, ISageDbContext
    {
        private readonly ICurrentMandantProvider _mandantProvider;

        public DbSet<ConfigurationValue> ConfigurationValues { get; set; }
        public DbSet<ShippingType> ShippingTypes { get; set; }
        public DbSet<Mandant> Mandants { get; set; }
        public DbSet<Receipt> Receipts { get; set; }
        public DbSet<PurchaseReceipt> PurchaseReceipts { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<ContactPerson> ContactPersons { get; set; }
        public DbSet<PaymentCondition> PaymentConditions { get; set; }


        public ShippingDbContext(DbContextOptions<ShippingDbContext> options, ICurrentMandantProvider mandantProvider)
           : base(options)
        {
            _mandantProvider = mandantProvider ?? throw new ArgumentNullException(nameof(mandantProvider));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Receipt>()
                .HasQueryFilter(p => p.Mandant == _mandantProvider.GetMandantId())
                .HasKey(p => new { p.Id, p.Mandant });

            modelBuilder.Entity<PurchaseReceipt>()
               .HasQueryFilter(p => p.Mandant == _mandantProvider.GetMandantId())
               .HasKey(p => new { p.Id, p.Mandant });

            modelBuilder.Entity<ContactPerson>().HasKey(p => new { p.Number, p.Mandant });
            modelBuilder.Entity<Address>().HasKey(p => new { p.Id, p.Mandant });
            modelBuilder.Entity<PaymentCondition>().HasKey(p => new { p.ReceiptId, p.Mandant });
        }
    }
}
