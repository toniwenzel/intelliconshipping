﻿using intelliconShipping.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace intelliconShipping.Core.DataAccess
{
    public class ShippingTypesRepository : IShippingTypesRepository
    {
        private readonly ShippingDbContext _shippingDbContext;

        public ShippingTypesRepository(ShippingDbContext shippingDbContext)
        {
            _shippingDbContext = shippingDbContext ?? throw new ArgumentNullException(nameof(shippingDbContext));
        }

        public IEnumerable<ShippingType> GetShippingTypes()
        {
            return _shippingDbContext.ShippingTypes.FromSqlRaw("Select Gruppe, Max(Bezeichnung) As Bezeichnung from KHKGruppen Where typ = 40008 Group by Gruppe").ToList();
        }

    }
}
