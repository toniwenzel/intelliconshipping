﻿using intelliconShipping.Core.Entities;

namespace intelliconShipping.Core.DataAccess
{
    public interface IShippingTypesRepository
    {
        IEnumerable<ShippingType> GetShippingTypes();
    }
}
