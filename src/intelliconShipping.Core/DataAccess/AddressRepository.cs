﻿using intellicon.Utilities.Services;
using intelliconShipping.Core.Entities;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace intelliconShipping.Core.DataAccess
{
    public class AddressRepository : IAddressRepository
    {
        private readonly ShippingDbContext _shippingDbContext;
        private readonly IDbConnectionInfoService _dbConnectionInfoService;

        public AddressRepository(ShippingDbContext shippingDbContext, IDbConnectionInfoService dbConnectionInfoService)
        {
            _shippingDbContext = shippingDbContext ?? throw new ArgumentNullException(nameof(shippingDbContext));
            _dbConnectionInfoService = dbConnectionInfoService ?? throw new ArgumentNullException(nameof(dbConnectionInfoService));
        }

        public Address? GetAddress(int id, short mandant)
        {
            return _shippingDbContext.Addresses.Find(id, mandant);
        }

        public Address? GetAddressByMandant(short mandant)
        {
            return _shippingDbContext.Addresses.FromSqlRaw("SELECT KHKAdressen.* FROM KHKAdressenverweise INNER JOIN KHKAdressen ON KHKAdressenverweise.Mandant= KHKAdressen.Mandant AND KHKAdressenverweise.Adresse=KHKAdressen.Adresse WHERE Verweis='Mandant' AND KHKAdressenverweise.Mandant={0}", mandant).FirstOrDefault();
        }

        public AdditionalAddressData? GetAdditionalAddressData(int id, short mandant)
        {
            using (var connection = new SqlConnection(_dbConnectionInfoService.GetConnectionString()))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT * FROM KHKAdressen WHERE Adresse=@id AND Mandant=@mandant";
                    command.Parameters.Add(new SqlParameter("@id", id));
                    command.Parameters.Add(new SqlParameter("@mandant", mandant));
                    var reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        var data = new Dictionary<string, object>();

                        for (var i = 0; i < reader.FieldCount; i++)
                        {
                            data.Add(reader.GetName(i), reader.GetValue(i));
                        }

                        return new AdditionalAddressData(data);
                    }
                }
            }

            return null;
        }
    }
}
