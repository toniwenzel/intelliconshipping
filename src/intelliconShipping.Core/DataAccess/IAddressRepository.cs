﻿using intelliconShipping.Core.Entities;

namespace intelliconShipping.Core.DataAccess
{
    public interface IAddressRepository
    {
        Address? GetAddress(int id, short mandant);
        Address? GetAddressByMandant(short mandant);
        AdditionalAddressData? GetAdditionalAddressData(int id, short mandant);
    }
}
