﻿using intelliconShipping.Core.Entities;
using intelliconShipping.Core.Services;

namespace intelliconShipping.Core.DataAccess
{
    public interface IReceiptRepository
    {
        int GetLastReceiptId();
        IReceipt? GetReceiptByScan(ReceiptScanResult scanResult);
        AdditionalReceiptData? GetAdditionalReceiptData(int id, short mandant);
        AdditionalData? GetAdditionalPurchaseReceiptData(int id, short mandant);

        PaymentCondition? GetPaymentCondition(int receiptId, short mandant);
    }
}
