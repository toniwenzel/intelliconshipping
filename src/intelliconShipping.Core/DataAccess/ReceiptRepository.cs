﻿using intellicon.Utilities.Services;
using intelliconShipping.Core.Entities;
using intelliconShipping.Core.Services;
using Microsoft.Data.SqlClient;

namespace intelliconShipping.Core.DataAccess
{
    public class ReceiptRepository : IReceiptRepository
    {
        private readonly ShippingDbContext _shippingDbContext;
        private readonly ICurrentMandantProvider _currentMandantProvider;
        private readonly IDbConnectionInfoService _dbConnectionInfoService;

        public ReceiptRepository(ShippingDbContext shippingDbContext, ICurrentMandantProvider currentMandantProvider, IDbConnectionInfoService dbConnectionInfoService)
        {
            _shippingDbContext = shippingDbContext ?? throw new ArgumentNullException(nameof(shippingDbContext));
            _currentMandantProvider = currentMandantProvider ?? throw new ArgumentNullException(nameof(currentMandantProvider));
            _dbConnectionInfoService = dbConnectionInfoService ?? throw new ArgumentNullException(nameof(dbConnectionInfoService));
        }

        public int GetLastReceiptId()
        {
            return _shippingDbContext.Receipts.Max(x => x.Id);
        }

        public IReceipt? GetReceiptByScan(ReceiptScanResult scanResult)
        {
            if (scanResult.Mandant == 0)
                scanResult.Mandant = _currentMandantProvider.GetMandantId();

            if (scanResult.IsPurchase)
            {
                if (scanResult.ReceiptId > 0)
                {
                    return _shippingDbContext.PurchaseReceipts.FirstOrDefault(r => r.Id == scanResult.ReceiptId && r.Mandant == scanResult.Mandant);
                }
                else
                {
                    return _shippingDbContext.PurchaseReceipts.FirstOrDefault(r => r.Number == scanResult.ReceiptNumber && r.Year == scanResult.ReceiptYear && r.Mandant == scanResult.Mandant);
                }
            }
            else
            {
                if (scanResult.ReceiptId > 0)
                {
                    return _shippingDbContext.Receipts.FirstOrDefault(r => r.Id == scanResult.ReceiptId && r.Mandant == scanResult.Mandant);
                }
                else
                {
                    return _shippingDbContext.Receipts.FirstOrDefault(r => r.Number == scanResult.ReceiptNumber && r.Year == scanResult.ReceiptYear && r.Mandant == scanResult.Mandant);
                }
            }
        }

        public AdditionalReceiptData? GetAdditionalReceiptData(int id, short mandant)
        {
            var data = GetAdditionalData(id, mandant, "KHKVKBelege");

            if (data != null)
                return new AdditionalReceiptData(data);
            else
                return null;
        }

        public AdditionalData? GetAdditionalPurchaseReceiptData(int id, short mandant)
        {
            var data = GetAdditionalData(id, mandant, "KHKEKBelege");

            if (data != null)
                return new AdditionalData(data);
            else
                return null;
        }

        public PaymentCondition? GetPaymentCondition(int receiptId, short mandant)
        {
            return _shippingDbContext.PaymentConditions.Find(receiptId, mandant);
        }

        private Dictionary<string, object> GetAdditionalData(int id, short mandant, string tableName)
        {
            using (var connection = new SqlConnection(_dbConnectionInfoService.GetConnectionString()))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = $"SELECT * FROM {tableName} WHERE BelID=@id AND Mandant=@mandant";
                    command.Parameters.Add(new SqlParameter("@id", id));
                    command.Parameters.Add(new SqlParameter("@mandant", mandant));
                    var reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        var data = new Dictionary<string, object>();

                        for (var i = 0; i < reader.FieldCount; i++)
                        {
                            data.Add(reader.GetName(i), reader.GetValue(i));
                        }

                        return data;
                    }
                }
            }

            return null;
        }
    }
}
