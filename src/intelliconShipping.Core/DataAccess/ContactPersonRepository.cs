﻿using intelliconShipping.Core.Entities;

namespace intelliconShipping.Core.DataAccess
{
    public class ContactPersonRepository : IContactPersonRepository
    {
        private readonly ShippingDbContext _shippingDbContext;

        public ContactPersonRepository(ShippingDbContext shippingDbContext)
        {
            _shippingDbContext = shippingDbContext ?? throw new ArgumentNullException(nameof(shippingDbContext));
        }

        public ContactPerson? GetByName(int addressNumber, short mandant, string contactPerson)
        {
            return _shippingDbContext.ContactPersons.FirstOrDefault(c => c.Number == addressNumber && c.Mandant == mandant && c.Name == contactPerson);
        }

        public ContactPerson? GetContactPerson(int number, short mandant)
        {
            return _shippingDbContext.ContactPersons.Find(number, mandant);
        }
    }
}
