﻿using intelliconShipping.Core.Entities;

namespace intelliconShipping.Core.DataAccess
{
    public interface IContactPersonRepository
    {
        ContactPerson? GetContactPerson(int number, short mandant);
        ContactPerson? GetByName(int addressNumber, short mandant, string contactPerson);
    }
}
