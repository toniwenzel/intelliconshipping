﻿using intelliconShipping.Core.Entities;

namespace intelliconShipping.Core.Services
{
    public interface IConfigurationService
    {
        public string? ReadValue(string key);
        public void WriteValue(string key, string? value);
        IEnumerable<ConfigurationValue> GetAll();
    }
}
