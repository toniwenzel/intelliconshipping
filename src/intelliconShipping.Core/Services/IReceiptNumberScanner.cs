﻿namespace intelliconShipping.Core.Services
{
    public interface IReceiptNumberScanner
    {
        ReceiptScanResult Scan(string number);
    }

    public class ReceiptScanResult
    {
        public int Mandant { get; set; }
        public int ReceiptId { get; set; }
        public bool IsPurchase { get; set; }
        public int ReceiptNumber { get; set; }
        public short ReceiptYear { get; set; }

        public bool IsValid { get; set; }
    }
}
