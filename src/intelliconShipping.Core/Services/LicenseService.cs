﻿using Microsoft.Extensions.Logging;

namespace intelliconShipping.Core.Services
{
    public class LicenseService : ILicenseService
    {
        private readonly IConfigurationService _configurationService;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ILogger<LicenseService> _logger;
        private const string KEY_CUSTOMERNUMBER = "CustomerNumber";
        private const string KEY_CUSTOMERNAME = "CustomerName";

        public LicenseService(IConfigurationService configurationService, IHttpClientFactory httpClientFactory, ILogger<LicenseService> logger)
        {
            _configurationService = configurationService ?? throw new ArgumentNullException(nameof(configurationService));
            _httpClientFactory = httpClientFactory ?? throw new ArgumentNullException(nameof(httpClientFactory));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public LicenseInfo? ReadLicense()
        {
            var customerNumber = _configurationService.ReadValue(KEY_CUSTOMERNUMBER);
            var customerName = _configurationService.ReadValue(KEY_CUSTOMERNAME);

            if (!string.IsNullOrWhiteSpace(customerNumber))
            {
                return new LicenseInfo
                {
                    CustomerNumber = customerNumber,
                    CustomerName = customerName
                };
            }

            return null;
        }

        public async Task<CheckResult> ValidateLicense(LicenseInfo license)
        {
            return await ValidateLicense(license, "versandmodul").ConfigureAwait(false);
        }

        public async Task<CheckResult> ValidateLicense(LicenseInfo license, string moduleName)
        {
            var client = _httpClientFactory.CreateClient();

            try
            {
                var customerNumber = license.CustomerNumber;

                if (customerNumber?.Length > 9)
                    customerNumber = customerNumber.Substring(0, 9);

                var response = await client.GetStringAsync($"https://license.intellicon.de/checklicense.php?lizenznehmer={license.CustomerNumber}&modul={moduleName.ToLower()}");
                var isValid = response.Contains("<license>true</license>");
                return new CheckResult(isValid, isValid ? string.Empty : "Es wurde leider kein gütliger Lizenzeintrag gefunden.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error checking license by web service");
                return new CheckResult(false, ex.Message);
            }
        }

        public void WriteLicense(LicenseInfo license)
        {
            _configurationService.WriteValue(KEY_CUSTOMERNUMBER, license.CustomerNumber);
            _configurationService.WriteValue(KEY_CUSTOMERNAME, license.CustomerName);
        }
    }
}
