﻿namespace intelliconShipping.Core.Services
{
    /// <summary>
    /// Interface to read registry values
    /// </summary>
    public interface IRegistryReader
    {
        object? ReadValue(string key);
        object? ReadUserValue(string key);
    }
}
