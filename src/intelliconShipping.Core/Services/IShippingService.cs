﻿using intelliconShipping.Core.Entities;

namespace intelliconShipping.Core.Services
{
    public interface IShippingService
    {
        Parcel CreateParcel(IReceipt receipt);
        Task Ship(Parcel parcel);
    }
}
