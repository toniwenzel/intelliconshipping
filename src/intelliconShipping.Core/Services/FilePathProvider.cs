﻿using intellicon.Utilities.Services;

namespace intelliconShipping.Core.Services
{
    public class FilePathProvider : IFilePathProvider
    {
        public string GetLoggingPath()
        {
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "intellicon", "Shipping");
        }
    }
}
