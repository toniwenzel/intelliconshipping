﻿using intelliconShipping.Core.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace intelliconShipping.Core.Services
{
    public class ConfigurationService : IConfigurationService
    {
        private readonly ShippingDbContext _dbContext;

        public ConfigurationService(ShippingDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public IEnumerable<Entities.ConfigurationValue> GetAll()
        {
            return _dbContext.ConfigurationValues.AsNoTracking().ToList();
        }

        public string? ReadValue(string key)
        {
            return _dbContext.ConfigurationValues.AsNoTracking().FirstOrDefault(c => c.KeyName == key)?.Value;
        }

        public void WriteValue(string key, string? value)
        {
            var entry = _dbContext.ConfigurationValues.FirstOrDefault(c => c.KeyName == key);

            if (entry == null)
                _dbContext.ConfigurationValues.Add(new Entities.ConfigurationValue { KeyName = key, Value = value });
            else
                entry.Value = value;

            _dbContext.SaveChanges();
        }
    }
}
