﻿using intellicon.Utilities.Services;
using intelliconShipping.Core.DataAccess;
using intelliconShipping.Core.Entities;
using intelliconShipping.Core.ShippingProvider;

namespace intelliconShipping.Core.Services
{
    public class ShippingService : IShippingService
    {

        private readonly IGlobalSettings _globalSettings;
        private readonly IAddressRepository _addressRepository;
        private readonly IReceiptRepository _receiptRepository;
        private readonly IContactPersonRepository _contactPersonRepository;
        private readonly ICurrentMandantProvider _currentMandantProvider;
        private readonly IShippingProviderFactory _shippingProviderFactory;

        public ShippingService(IGlobalSettings globalSettings,
                                 IAddressRepository addressRepository,
                                 IReceiptRepository receiptRepository,
                                 IContactPersonRepository contactPersonRepository,
                                 ICurrentMandantProvider currentMandantProvider,
                                 IShippingProviderFactory shippingProviderFactory)
        {
            _globalSettings = globalSettings ?? throw new ArgumentNullException(nameof(globalSettings));
            _addressRepository = addressRepository ?? throw new ArgumentNullException(nameof(addressRepository));
            _receiptRepository = receiptRepository ?? throw new ArgumentNullException(nameof(receiptRepository));
            _contactPersonRepository = contactPersonRepository ?? throw new ArgumentNullException(nameof(contactPersonRepository));
            _currentMandantProvider = currentMandantProvider ?? throw new ArgumentNullException(nameof(currentMandantProvider));
            _shippingProviderFactory = shippingProviderFactory ?? throw new ArgumentNullException(nameof(shippingProviderFactory));
        }

        public Parcel CreateParcel(IReceipt receipt)
        {
            var parcel = new Parcel(receipt);
            parcel.Packages.Add(new Package());

            if (_globalSettings.FixedWeightDomestic > 0)
            {
                parcel.Packages[0].Weight = _globalSettings.FixedWeightDomestic;
            }

            LoadParcelData(parcel, receipt, false);

            return parcel;
        }

        public async Task Ship(Parcel parcel)
        {
            var provider = await _shippingProviderFactory.CreateShippingProvider(parcel.ShippingType);
            await provider.Ship(parcel);
        }

        private void LoadParcelData(Parcel parcel, IReceipt receipt, bool isImport)
        {
            if (parcel.IsPurchase)
                LoadParcelDataForPurchase(parcel, (PurchaseReceipt)receipt);
            else
                LoadParcelDataForSale(parcel, (Receipt)receipt, isImport);

            LoadEmailMessages(parcel);
        }

        private void LoadParcelDataForSale(Parcel parcel, Receipt receipt, bool isImport)
        {
            parcel.ReceiptNumber = $"{receipt.Year}-{receipt.Number}";
            parcel.Saluation = receipt.Saluation;
            parcel.Name1 = receipt.Name1;
            parcel.Name2 = receipt.Name2;
            parcel.Name3 = string.Empty;
            parcel.Name4 = string.Empty;
            parcel.Additional = receipt.Additional;
            parcel.Street = receipt.Street;
            parcel.City = receipt.City;
            parcel.PostCode = receipt.PostCode;
            parcel.Country = receipt.Country;
            parcel.Currency = receipt.Currency;
            parcel.PreviousId = receipt.PreviousId;
            parcel.ReferenceSign = receipt.ReferenceSign;
            parcel.ReferenceNumber = receipt.ReferenceNumber;
            parcel.ContactPerson = receipt.ContactPerson;
            parcel.CustomerNumber = receipt.CustomerNumber;
            parcel.ShippingType = receipt.ShippingType;
            parcel.DeliveryCondition = receipt.DeliveryCondition;
            parcel.DeliveryDate = receipt.DeliveryDate ?? DateTime.MinValue;
            parcel.DebtAmount = 0;

            LoadPhone(parcel, receipt);

            var additionalData = _receiptRepository.GetAdditionalReceiptData(receipt.Id, receipt.Mandant);

            if (additionalData == null)
                throw new InvalidOperationException($"Could not load additional receipt data for id: {receipt.Id}, mandant: {receipt.Mandant}");

            if (!_globalSettings.IgnoreUserFields)
                LoadUserFields(parcel, receipt, additionalData);

            if (additionalData != null && additionalData.ShippingPrintDone)
                parcel.Error = "Achtung, für diesen Beleg wurde bereits ein Paketaufkleber gedruckt!";

            var paymentCondition = _receiptRepository.GetPaymentCondition(receipt.Id, receipt.Mandant);
            if (paymentCondition != null)
                parcel.PaymentCondition = paymentCondition.Condition;

            if (parcel.DeliveryDate < DateTime.Today && !isImport)
                parcel.DeliveryDate = DateTime.Today;

            if (receipt.Weight.HasValue && receipt.Weight.Value > 0 && parcel.Packages[0].Weight == 0)
                parcel.Packages[0].Weight = receipt.Weight.Value;

            LoadEmail(parcel, receipt, additionalData);

            // IP 5.1.19 alle Kontaktdaten aus Ansprechpartner (falls vorhanden) ... zunächst für GAZ
            if (_globalSettings.UseContactDataFromAsp && receipt.CustomerContactPersonId.HasValue && receipt.CustomerContactPersonId.Value > 0)
                LoadContactData(parcel, receipt);

            if (receipt.GrossAmount > 0)
                parcel.DebtAmount = receipt.GrossAmount.Value;

            LoadPackages(parcel, additionalData);
            LoadAllUserFields(parcel, additionalData);
        }

        private void LoadAllUserFields(Parcel parcel, AdditionalReceiptData additionalData)
        {
            foreach (var columnName in additionalData.GetColumnNames())
            {
                if (columnName.StartsWith("USER_"))
                    parcel.UserFields.Add($"{columnName}|{additionalData.Get<object>(columnName)}", columnName);
            }
        }

        private static void LoadPackages(Parcel parcel, AdditionalData additionalData)
        {
            var amountOfPackages = 0;
            if (additionalData.HasColumn("USER_Anzahlpakete"))
            {
                amountOfPackages = additionalData.Get<int>("USER_Anzahlpakete");
                if (amountOfPackages > 9 || amountOfPackages == 0)
                    amountOfPackages = 1;
            }

            if (additionalData.HasColumn("USER_Paketanzahl"))
            {
                amountOfPackages = additionalData.Get<int>("USER_Paketanzahl");
                if (amountOfPackages > 9 || amountOfPackages == 0)
                    amountOfPackages = 1;
            }

            for (var i = 1; i < amountOfPackages; i++)
            {
                parcel.Packages.Add(new Package { });
            }

            parcel.CalculateDebtPerPackage();
        }

        private void LoadEmail(Parcel parcel, Receipt receipt, AdditionalReceiptData? additionalData)
        {
            if (_globalSettings.IsVersion62)
                parcel.Email = additionalData?.Get<string>("WSEMail");
            else
                parcel.Email = additionalData?.Get<string>("WebEMail");

            var customerAddressData = _addressRepository.GetAdditionalAddressData(receipt.CustomerAddressId ?? 0, receipt.Mandant);

            // 2. Mailadresse aus Kundenstamm
            if (string.IsNullOrWhiteSpace(parcel.Email) && customerAddressData != null)
                parcel.Email = customerAddressData.Email;

            var shippingStyle = customerAddressData?.ShippingStyle;
            if (shippingStyle > 0)
            {
                var email = "";

                // 1: Fixe Versand-Emailadresse, alternativ Standardadresse
                if (shippingStyle == 1)
                    email = customerAddressData?.ShippingEmail;
                //2: Fixe Versand-Emailadresse (feld: USER_EMailVersandnachricht aus Lieferadresse), alternativ Standardadresse
                else if (shippingStyle == 2)
                {
                    if (receipt.DestinationAddressId.HasValue)
                    {
                        var destinationAddressData = _addressRepository.GetAdditionalAddressData(receipt.DestinationAddressId.Value, receipt.Mandant);
                        email = destinationAddressData?.Email;
                    }
                }
                // 3: Wie 2, alternativ 1
                else if (shippingStyle == 3)
                {
                    if (receipt.DestinationAddressId.HasValue)
                    {
                        var destinationAddressData = _addressRepository.GetAdditionalAddressData(receipt.DestinationAddressId.Value, receipt.Mandant);
                        email = destinationAddressData?.Email;
                    }

                    if (string.IsNullOrWhiteSpace(email))
                        email = customerAddressData?.ShippingEmail;
                }
                // Keine Versandnachricht
                else if (shippingStyle == 9)
                {
                    parcel.Email = string.Empty;
                }

                if (!string.IsNullOrEmpty(email))
                    parcel.Email = email;
            }
        }

        private void LoadPhone(Parcel parcel, Receipt receipt)
        {
            // 1. Suche: Lieferadresse (lAdresse, d.h. A1Adressnr)
            if (receipt.DestinationAddressId > 0 && receipt.DestinationAddressId.HasValue)
            {
                var address = _addressRepository.GetAddress(receipt.DestinationAddressId.Value, receipt.Mandant);
                if (address != null)
                {
                    parcel.Phone = address.Phone;
                    parcel.PhoneSource = "Adr. Lieferanschrift";
                }
            }

            // 2. Suche, wenn Telefon leer ist, in der Auftraggeberadresse  (A0Adressnr)
            if (string.IsNullOrWhiteSpace(parcel.Phone) && receipt.CustomerAddressId.HasValue)
            {
                var address = _addressRepository.GetAddress(receipt.CustomerAddressId.Value, receipt.Mandant);
                if (address != null)
                {
                    parcel.Phone = address.Phone;
                    parcel.PhoneSource = "Adr. Auftraggeber";
                }
            }

            // 3. Wenn Telefon leer, dann Telefonnummer aus Mandantenadresse
            if (string.IsNullOrWhiteSpace(parcel.Phone))
            {
                var address = _addressRepository.GetAddressByMandant(receipt.Mandant);
                if (address != null)
                {
                    parcel.Phone = address.Phone;
                    parcel.PhoneSource = "Mandant";
                }
            }

            // 4. wenn gar nichts hilft, dann Dummy-Nummer
            if (string.IsNullOrWhiteSpace(parcel.Phone))
            {
                parcel.Phone = "00";
                parcel.PhoneSource = "Dummy";
            }
        }

        private void LoadUserFields(Parcel parcel, Receipt receipt, AdditionalReceiptData? additionalData)
        {
            if (additionalData != null && additionalData.HasColumn("USER_A1Name1"))
            {
                parcel.Additional = additionalData.Name1;
                parcel.Name1 = additionalData.Name2;
                parcel.Name2 = additionalData.Name4;
                parcel.Name3 = additionalData.Name3;
                parcel.Name4 = additionalData.Additional;
                parcel.Saluation = additionalData.Saluation;

                if (!string.IsNullOrWhiteSpace(additionalData.ContactPerson))
                    parcel.ContactPerson = additionalData.ContactPerson;

                parcel.ContactPersonId = additionalData.ContactPersonId;

                if (!string.IsNullOrWhiteSpace(parcel.ContactPerson) && receipt.DestinationAddressId.HasValue)
                {
                    var contactPerson = _contactPersonRepository.GetByName(receipt.DestinationAddressId.Value, _currentMandantProvider.GetMandantId(), parcel.ContactPerson);
                    if (contactPerson != null)
                    {
                        parcel.Phone = contactPerson.Phone;
                        parcel.PhoneSource = "Ansprechpartner Lieferadr.";
                    }
                }

                parcel.Street = additionalData.Street;
                parcel.City = additionalData.City;
                parcel.Country = additionalData.Country;
                parcel.PostCode = additionalData.PostCode;

                parcel.UserAddressFields = true;
            }

        }

        private void LoadContactData(Parcel parcel, Receipt receipt)
        {
            if (!receipt.CustomerContactPersonId.HasValue)
                return;

            var contactPerson = _contactPersonRepository.GetContactPerson(receipt.CustomerContactPersonId.Value, receipt.Mandant);

            // wenn abweichende Lieferadresse mit A1Zusatz, dann diesen als Zusatz belassen  ...spezieller Wunsch GAZ
            if (receipt.CustomerAddressId == receipt.DestinationAddressId || string.IsNullOrWhiteSpace(receipt.Additional))
            {
                if (!string.IsNullOrWhiteSpace(contactPerson?.Name))
                    parcel.Additional = contactPerson.Name;
            }

            if (!string.IsNullOrWhiteSpace(contactPerson?.Phone))
            {
                parcel.Phone = contactPerson.Phone;
                parcel.PhoneSource = "Ansprechpartner der Kundenadr.";
            }

            if (!string.IsNullOrWhiteSpace(contactPerson?.Email))
                parcel.Email = contactPerson.Email;
        }

        private void LoadParcelDataForPurchase(Parcel parcel, PurchaseReceipt receipt)
        {
            parcel.Name1 = receipt.Name1;
            parcel.Name2 = receipt.Name2;
            parcel.Name3 = string.Empty;
            parcel.Name4 = string.Empty;
            parcel.Additional = receipt.Additional;
            parcel.Street = receipt.Street;
            parcel.City = receipt.City;
            parcel.PostCode = receipt.PostCode;
            parcel.Country = receipt.Country;
            parcel.Currency = receipt.Currency;
            parcel.Saluation = receipt.Saluation;
            parcel.ReceiptNumber = $"{receipt.Year}-{receipt.Number}";
            parcel.CustomerNumber = receipt.CustomerNumber;

            var additionalData = _receiptRepository.GetAdditionalPurchaseReceiptData(receipt.Id, receipt.Mandant);

            if (additionalData == null)
                throw new InvalidOperationException($"Could not load additional purchase receipt data for id: {receipt.Id}, mandant: {receipt.Mandant}");

            LoadCustomerData(parcel, receipt);
            LoadPackages(parcel, additionalData);
        }

        private static void LoadCustomerData(Parcel parcel, PurchaseReceipt receipt)
        {
            if (string.IsNullOrWhiteSpace(parcel.Street))
                parcel.Street = receipt.CustomerStreet;
            if (string.IsNullOrWhiteSpace(parcel.City))
                parcel.City = receipt.CustomerCity;
            if (string.IsNullOrWhiteSpace(parcel.PostCode))
                parcel.PostCode = receipt.CustomerPostCode;
            if (string.IsNullOrWhiteSpace(parcel.Country))
                parcel.Country = receipt.CustomerCountry;

            if (string.IsNullOrWhiteSpace(parcel.Saluation) && string.IsNullOrWhiteSpace(parcel.Name1))
                parcel.Saluation = receipt.CustomerSaluation;

            if (string.IsNullOrWhiteSpace(parcel.Name1))
                parcel.Name1 = receipt.CustomerName1;

            if (string.IsNullOrWhiteSpace(parcel.Name2))
                parcel.Name2 = receipt.CustomerName2;

            if (string.IsNullOrWhiteSpace(parcel.Additional))
                parcel.Additional = receipt.CustomerAdditional;
        }

        private void LoadEmailMessages(Parcel parcel)
        {
            parcel.EmailAddressForFailedShipping = _globalSettings.StatusEmailAddress;

            // EmailAddressForDelivery ist eine zusätzliche Adresse zur Kundenadresse, an die eine Zustellnachricht gesenet wird (nur UPS)
            // Momentan: Wir verwenden die hinterlegte Zustellnachrichtadresse
            parcel.EmailAddressForDelivery = _globalSettings.DeliveryEmailAddress;


            // Standard: Versandnachricht an die E-Mail Adresse des Kunden
            parcel.EmailAddressForShipping = parcel.Email;

            // d.h. Kunde hat keine E-mail Adresse
            if (string.IsNullOrWhiteSpace(parcel.EmailAddressForShipping))
                // ersatzweise an eine hausinterne Adresse
                parcel.EmailAddressForShipping = _globalSettings.DeliveryEmailAddress?.Split(';').FirstOrDefault();  // das ist neuerdings die Zustell-Mailadresse. Die kann mehrere Mailadressen enthalten


            //falls auch keine eigene Adresse hinterlegt, Fehleradresse nehmen
            if (string.IsNullOrWhiteSpace(parcel.EmailAddressForShipping))
                parcel.EmailAddressForShipping = parcel.EmailAddressForFailedShipping?.Split(';').FirstOrDefault();
        }
    }
}
