﻿namespace intelliconShipping.Core.Services
{
    public interface ILicenseService
    {
        LicenseInfo? ReadLicense();
        void WriteLicense(LicenseInfo license);

        Task<CheckResult> ValidateLicense(LicenseInfo license);

        Task<CheckResult> ValidateLicense(LicenseInfo license, string moduleName);
    }
}
