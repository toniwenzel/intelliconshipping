﻿namespace intelliconShipping.Core.Services
{
    public interface IGlobalSettings
    {
        bool IsVersion62 { get; }
        bool IgnoreUserFields { get; }
        bool UseContactDataFromAsp { get; }
        decimal FixedWeightDomestic { get; }

        /// <summary>
        /// Email address for new states such as failed shipping
        /// </summary>
        string? StatusEmailAddress { get; }

        /// <summary>
        /// Email address for delivery (Zustellung)
        /// </summary>
        string? DeliveryEmailAddress { get; }

        void Reload();
    }
}
