﻿using Microsoft.Win32;

namespace intelliconShipping.Core.Services
{
    public class OldRegistryReader : IDisposable, IRegistryReader
    {
        private RegistryKey? _registryKey;
        private bool _searchFinished;
        private bool _disposedValue;

        public object? ReadValue(string key)
        {
            OpenRegistryKey();

            return _registryKey?.GetValue(key);
        }

        public object? ReadUserValue(string key)
        {
            return ReadValue("USER_" + key);
        }

        private void OpenRegistryKey()
        {
            if (_registryKey == null && !_searchFinished)
            {
                _registryKey = Registry.LocalMachine.OpenSubKey(@"Software\intellicon\icPaketversand");

                if (_registryKey == null)
                    _registryKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\intellicon\icPaketversand");
                if (_registryKey == null)
                    _registryKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Classes\VirtualStore\MACHINE\SOFTWARE\WOW6432Node\intellicon\icPaketversand");

            }

            _searchFinished = true;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    if (_registryKey != null)
                    {
                        _registryKey.Close();
                        _registryKey.Dispose();
                    }
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
