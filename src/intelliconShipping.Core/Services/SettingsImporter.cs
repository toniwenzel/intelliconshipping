﻿namespace intelliconShipping.Core.Services
{
    /// <summary>
    /// Class to import settings from registry into database
    /// </summary>
    public class SettingsImporter
    {
        private readonly ILicenseService _licenseService;
        private readonly IRegistryReader _registryReader;
        private readonly IConfigurationService _configurationService;

        public SettingsImporter(ILicenseService licenseService, IRegistryReader registryReader, IConfigurationService configurationService)
        {
            _licenseService = licenseService ?? throw new ArgumentNullException(nameof(licenseService));
            _registryReader = registryReader ?? throw new ArgumentNullException(nameof(registryReader));
            _configurationService = configurationService ?? throw new ArgumentNullException(nameof(configurationService));
        }

        public void Import()
        {
            ImportLicense();
            ImportWeights();
            ImportBool(ConfigurationKeys.IgnoreUserFields, "UserfelderInaktiv");
            ImportBool(ConfigurationKeys.UseContactDataFromAsp, "KontaktAusASP");
            ImportString(ConfigurationKeys.StatusEmailAddress, "StatusEmailadresse");
            ImportString(ConfigurationKeys.DeliveryEmailAddress, "EMailadresseNachricht");
        }

        private void ImportLicense()
        {
            var license = _licenseService.ReadLicense();

            if (license == null)
            {
                var customerNumber = _registryReader.ReadValue("SageKdNr")?.ToString();
                var customerName = _registryReader.ReadValue("SageKdName")?.ToString();

                if (!string.IsNullOrWhiteSpace(customerNumber))
                {
                    license = new LicenseInfo { CustomerName = customerName, CustomerNumber = customerNumber };

                    _licenseService.WriteLicense(license);
                }
            }
        }

        private void ImportWeights()
        {
            var value = _configurationService.ReadValue(ConfigurationKeys.FixedWeightDomestic);
            if (string.IsNullOrEmpty(value))
            {
                var weight = _registryReader.ReadUserValue("GewichtFixDE")?.ToString();
                if (!string.IsNullOrWhiteSpace(weight) && decimal.TryParse(weight, out var parsedValue))
                {
                    _configurationService.WriteValue(ConfigurationKeys.FixedWeightDomestic, parsedValue);
                }
            }
        }

        private void ImportBool(string configurationKeyName, string registryKey)
        {
            var value = _configurationService.ReadValue(configurationKeyName);
            if (string.IsNullOrEmpty(value))
            {
                var inactive = _registryReader.ReadValue(registryKey)?.ToString();
                if (!string.IsNullOrWhiteSpace(inactive))
                {
                    _configurationService.WriteValue(configurationKeyName, inactive == "1");
                }
            }
        }

        private void ImportString(string configurationKeyName, string registryKey)
        {
            var value = _configurationService.ReadValue(configurationKeyName);
            if (string.IsNullOrEmpty(value))
            {
                var registryValue = _registryReader.ReadValue(registryKey)?.ToString();
                if (!string.IsNullOrWhiteSpace(registryValue))
                {
                    _configurationService.WriteValue(configurationKeyName, registryValue);
                }
            }
        }
    }
}
