﻿using System.Text.RegularExpressions;

namespace intelliconShipping.Core.Services
{
    public class ReceiptNumberScanner : IReceiptNumberScanner
    {
        private static Regex s_ReceiptNumber = new Regex(@"([eE]?)([0-9]{4})\-([0-9]+)");

        public ReceiptScanResult Scan(string number)
        {
            var result = new ReceiptScanResult();

            if (number.Contains("$M") && number.IndexOf("$", 1) > 1)
            {
                var mandant = number.Substring(2, number.IndexOf("$", 1) - 2);
                if (int.TryParse(mandant, out var mandantId))
                    result.Mandant = mandantId;
            }

            if (number.Contains("$I"))
            {
                var receiptId = number.Substring(number.IndexOf("$I") + 2);
                if (receiptId.Contains("$"))
                    receiptId = receiptId.Substring(0, receiptId.IndexOf("$"));

                if (int.TryParse(receiptId, out var receiptIdNumber))
                {
                    result.ReceiptId = receiptIdNumber;
                    result.IsValid = true;
                }
            }

            if (number.Contains("$E"))
            {
                result.IsPurchase = true;
            }

            var match = s_ReceiptNumber.Match(number);
            if (match.Success)
            {
                result.ReceiptNumber = int.Parse(match.Groups[3].Value);
                result.ReceiptYear = short.Parse(match.Groups[2].Value);
                result.IsPurchase = match.Groups[1].Length > 0;
                result.IsValid = true;
            }


            return result;
        }
    }
}
