﻿using System.Data.Common;
using intellicon.Utilities.Services;
using intelliconShipping.Core.DataAccess;
using intelliconShipping.Core.Entities;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;

namespace intelliconShipping.Core.Services
{
    public class GlobalSettings : IGlobalSettings
    {
        private readonly ShippingDbContext _shippingDbContext;
        private readonly IConfigurationService _configurationService;
        private readonly IDbConnectionInfoService _dbConnectionInfoService;
        private readonly ILogger<GlobalSettings> _logger;

        public bool IsVersion62 { get; private set; }

        public bool IgnoreUserFields { get; private set; }
        public bool UseContactDataFromAsp { get; private set; }
        public decimal FixedWeightDomestic { get; private set; }
        public string? StatusEmailAddress { get; private set; }
        public string? DeliveryEmailAddress { get; private set; }

        public GlobalSettings(ShippingDbContext shippingDbContext, IConfigurationService configurationService, IDbConnectionInfoService dbConnectionInfoService, ILogger<GlobalSettings> logger)
        {
            _shippingDbContext = shippingDbContext ?? throw new ArgumentNullException(nameof(shippingDbContext));
            _configurationService = configurationService ?? throw new ArgumentNullException(nameof(configurationService));
            _dbConnectionInfoService = dbConnectionInfoService ?? throw new ArgumentNullException(nameof(dbConnectionInfoService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            Reload();
        }

        public void Reload()
        {
            IsVersion62 = ExecuteQuery<int>("SELECT Count(Property) FROM USysSetup WHERE (Tree = 'Admin') AND (Token = 'Update')  AND (Property = 'System62_1')") > 0;
            ReloadSettings();
        }

        private void ReloadSettings()
        {
            var entries = _configurationService.GetAll();

            IgnoreUserFields = ReadBool(entries, ConfigurationKeys.IgnoreUserFields);
            UseContactDataFromAsp = ReadBool(entries, ConfigurationKeys.UseContactDataFromAsp);
            FixedWeightDomestic = ReadDecimal(entries, ConfigurationKeys.FixedWeightDomestic);
            StatusEmailAddress = ReadString(entries, ConfigurationKeys.StatusEmailAddress);
            DeliveryEmailAddress = ReadString(entries, ConfigurationKeys.DeliveryEmailAddress);
        }

        private static bool ReadBool(IEnumerable<ConfigurationValue> entries, string keyName, bool defaultValue = false)
        {
            var entry = entries.FirstOrDefault(e => e.KeyName == keyName);

            if (entry == null)
                return defaultValue;
            else
                return ConfigurationServiceExtensions.ReadBool(entry.Value, defaultValue);
        }

        private static decimal ReadDecimal(IEnumerable<ConfigurationValue> entries, string keyName, decimal defaultValue = 0m)
        {
            var entry = entries.FirstOrDefault(e => e.KeyName == keyName);

            if (entry == null)
                return defaultValue;
            else
                return ConfigurationServiceExtensions.ReadDecimal(entry.Value, defaultValue);
        }

        private static string ReadString(IEnumerable<ConfigurationValue> entries, string keyName, string defaultValue = null)
        {
            var entry = entries.FirstOrDefault(e => e.KeyName == keyName);

            if (entry == null)
                return defaultValue;
            else
                return entry.Value ?? defaultValue;
        }

        private T? ExecuteQuery<T>(string commandText)
        {
            try
            {
                using var connection = new SqlConnection(_dbConnectionInfoService.GetConnectionString());
                connection.Open();
                using var command = connection.CreateCommand();
                command.CommandText = commandText;
                var reader = command.ExecuteReader();

                if (reader.Read())
                {
                    return (T)reader.GetValue(0);
                }
            }
            catch (DbException e)
            {
                _logger.LogError(e, "Could not execute query '{query}'", commandText);
            }

            return default;
        }

        //private bool TableColumnExists(string columnName, string tableName)
        //{
        //    using (var connection = new SqlConnection(_dbConnectionInfoService.GetConnectionString()))
        //    {
        //        connection.Open();
        //        using (var command = connection.CreateCommand())
        //        {
        //            command.CommandText = "SELECT Column_Name FROM Information_Schema.Columns WHERE Table_Name=@table AND Column_Name=@column";
        //            command.Parameters.Add(new SqlParameter("@table", tableName));
        //            command.Parameters.Add(new SqlParameter("@column", columnName));
        //            var reader = command.ExecuteReader();

        //            if (reader.Read())
        //            {
        //                return string.Equals(reader.GetString(0), columnName, StringComparison.OrdinalIgnoreCase);
        //            }
        //        }
        //    }

        //    return false;
        //}
    }
}
