﻿namespace intelliconShipping.Core.Services
{
    public static class ConfigurationKeys
    {
        public const string FixedWeightDomestic = "FixedWeightDomestic";
        public const string IgnoreUserFields = "IgnoreUserFields";
        public const string UseContactDataFromAsp = "UseContactDataFromAsp";
        public const string StatusEmailAddress = "StatusEmailAddress";
        public const string DeliveryEmailAddress = "DeliveryEmailAddress";

    }
}
