﻿using System.Globalization;

namespace intelliconShipping.Core.Services
{
    public static class ConfigurationServiceExtensions
    {
        public static void WriteValue(this IConfigurationService service, string key, decimal value)
        {
            service.WriteValue(key, value.ToString(CultureInfo.InvariantCulture));
        }

        public static void WriteValue(this IConfigurationService service, string key, bool value)
        {
            service.WriteValue(key, value.ToString(CultureInfo.InvariantCulture));
        }

        public static decimal ReadDecimal(this IConfigurationService service, string key, decimal defaultValue = default)
        {
            var value = service.ReadValue(key);
            return ReadDecimal(value, defaultValue);
        }

        public static decimal ReadDecimal(string? value, decimal defaultValue = default)
        {
            if (string.IsNullOrEmpty(value))
                return defaultValue;

            return decimal.Parse(value, CultureInfo.InvariantCulture);
        }

        public static bool ReadBool(this IConfigurationService service, string key, bool defaultValue = default)
        {
            var value = service.ReadValue(key);
            return ReadBool(value);
        }

        public static bool ReadBool(string? value, bool defaultValue = default)
        {
            if (string.IsNullOrEmpty(value))
                return defaultValue;

            return bool.Parse(value);
        }
    }
}
