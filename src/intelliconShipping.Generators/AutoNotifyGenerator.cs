﻿using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;

namespace intelliconShipping.Generators
{
    [Generator]
    public class AutoNotifyGenerator : ISourceGenerator
    {
        private const string attributeText = @"
using System;
namespace intelliconShipping
{
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    [System.Diagnostics.Conditional(""AutoNotifyGenerator_DEBUG"")]
    sealed class AutoNotifyAttribute : Attribute
    {
        public AutoNotifyAttribute()
        {
        }
        public string PropertyName { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = true)]
    [System.Diagnostics.Conditional(""AutoNotifyGenerator_DEBUG"")]
    sealed class DependsOnAttribute : Attribute
    {
        public DependsOnAttribute(string propertyName)
        {
            PropertyName = propertyName;
        }

        public string PropertyName { get; set; }
    }
}
";


        public void Initialize(GeneratorInitializationContext context)
        {
            // Register the attribute source
            context.RegisterForPostInitialization((i) => i.AddSource("AutoNotifyAttribute", attributeText));

            // Register a syntax receiver that will be created for each generation pass
            context.RegisterForSyntaxNotifications(() => new SyntaxReceiver());
        }

        public void Execute(GeneratorExecutionContext context)
        {
            // retrieve the populated receiver
            if (context.SyntaxContextReceiver is not SyntaxReceiver receiver)
                return;

            // get the added attribute, and INotifyPropertyChanged
            INamedTypeSymbol autoNotifyAttributeSymbol = context.Compilation.GetTypeByMetadataName("intelliconShipping.AutoNotifyAttribute");
            INamedTypeSymbol dependsOnAttributeSymbol = context.Compilation.GetTypeByMetadataName("intelliconShipping.DependsOnAttribute");
            var notifySymbol = context.Compilation.GetTypeByMetadataName("System.ComponentModel.INotifyPropertyChanged");

            // group the fields by class, and generate the source
            foreach (var group in receiver.Fields.GroupBy(f => f.ContainingType))
            {
                string classSource = ProcessClass(group.Key, group.ToList(), autoNotifyAttributeSymbol, receiver.DependentProperties, dependsOnAttributeSymbol, notifySymbol, context);
                context.AddSource($"{group.Key.Name}_autoNotify.cs", SourceText.From(classSource, Encoding.UTF8));
            }
        }

        private string ProcessClass(INamedTypeSymbol classSymbol, List<IFieldSymbol> fields, ISymbol autoNotifyAttributeSymbol, Dictionary<string, List<IPropertySymbol>> dependentProperties, ISymbol dependsOnAttributeSymbol, ISymbol notifySymbol, GeneratorExecutionContext context)
        {
            if (!classSymbol.ContainingSymbol.Equals(classSymbol.ContainingNamespace, SymbolEqualityComparer.Default))
            {
                return null; //TODO: issue a diagnostic that it must be top level
            }

            string namespaceName = classSymbol.ContainingNamespace.ToDisplayString();
            var classAlreadyImplementyPropertyChanged = classSymbol.Interfaces.Contains(notifySymbol) || classSymbol.BaseType.Interfaces.Contains(notifySymbol);

            // begin building the generated source
            StringBuilder source = new StringBuilder($@"
namespace {namespaceName}
{{
    public partial class {classSymbol.Name} {(classAlreadyImplementyPropertyChanged ? "" : $": {notifySymbol.ToDisplayString()}")}
    {{
");

            // if the class doesn't implement INotifyPropertyChanged already, add it
            if (!classAlreadyImplementyPropertyChanged)
                source.Append("public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;");

            // create properties for each field
            foreach (var fieldSymbol in fields)
            {
                ProcessField(source, fieldSymbol, autoNotifyAttributeSymbol, dependentProperties, dependsOnAttributeSymbol, classSymbol);
            }

            source.Append("} }");
            return source.ToString();
        }

        private void ProcessField(StringBuilder source, IFieldSymbol fieldSymbol, ISymbol autoNotifyAttributeSymbol, Dictionary<string, List<IPropertySymbol>> dependentProperties, ISymbol dependsOnAttributeSymbol, INamedTypeSymbol classSymbol)
        {
            // get the name and type of the field
            var fieldName = fieldSymbol.Name;
            var fieldType = fieldSymbol.Type;

            // get the AutoNotify attribute from the field, and any associated data
            var attributeData = fieldSymbol.GetAttributes().Single(ad => ad.AttributeClass.Equals(autoNotifyAttributeSymbol, SymbolEqualityComparer.Default));
            var overridenNameOpt = attributeData.NamedArguments.SingleOrDefault(kvp => kvp.Key == "PropertyName").Value;

            var propertyName = ChooseName(fieldName, overridenNameOpt);
            if (propertyName.Length == 0 || propertyName == fieldName)
            {
                //TODO: issue a diagnostic that we can't process this field
                return;
            }

            source.AppendLine($@"
public {fieldType} {propertyName} 
{{
    get 
    {{
        return this.{fieldName};
    }}
    set
    {{
        this.{fieldName} = value;
        this.PropertyChanged?.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs(nameof({propertyName})));");


            if (classSymbol.MemberNames.Contains($"On{propertyName}Changed"))
            {
                source.AppendLine($"        this.On{propertyName}Changed();");
            }

            if (dependentProperties.TryGetValue(propertyName, out var propertyList))
            {
                foreach (var dependendProperty in propertyList.Where(p => p.ContainingType == classSymbol))
                    source.AppendLine($"        this.PropertyChanged?.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs(nameof({dependendProperty.Name})));");
            }

            source.Append($@"    }}
}}
");

        }

        private string ChooseName(string fieldName, TypedConstant overridenNameOpt)
        {
            if (!overridenNameOpt.IsNull)
            {
                return overridenNameOpt.Value.ToString();
            }

            fieldName = fieldName.TrimStart('_');
            if (fieldName.Length == 0)
                return string.Empty;

            if (fieldName.Length == 1)
                return fieldName.ToUpper();

            return fieldName.Substring(0, 1).ToUpper() + fieldName.Substring(1);
        }



        /// <summary>
        /// Created on demand before each generation pass
        /// </summary>
        class SyntaxReceiver : ISyntaxContextReceiver
        {
            public List<IFieldSymbol> Fields { get; } = new List<IFieldSymbol>();
            public Dictionary<string, List<IPropertySymbol>> DependentProperties { get; } = new Dictionary<string, List<IPropertySymbol>>();

            /// <summary>
            /// Called for every syntax node in the compilation, we can inspect the nodes and save any information useful for generation
            /// </summary>
            public void OnVisitSyntaxNode(GeneratorSyntaxContext context)
            {
                // any field with at least one attribute is a candidate for property generation
                if (context.Node is FieldDeclarationSyntax fieldDeclarationSyntax && fieldDeclarationSyntax.AttributeLists.Count > 0)
                {
                    foreach (var variable in fieldDeclarationSyntax.Declaration.Variables)
                    {
                        // Get the symbol being declared by the field, and keep it if its annotated
                        var fieldSymbol = context.SemanticModel.GetDeclaredSymbol(variable) as IFieldSymbol;
                        if (fieldSymbol.GetAttributes().Any(ad => ad.AttributeClass.ToDisplayString() == "intelliconShipping.AutoNotifyAttribute"))
                        {
                            Fields.Add(fieldSymbol);
                        }
                    }
                }

                // any field with at least one attribute is a candidate for property generation
                if (context.Node is PropertyDeclarationSyntax propertyDeclarationSyntax && propertyDeclarationSyntax.AttributeLists.Count > 0)
                {
                    var propertySymbol = context.SemanticModel.GetDeclaredSymbol(propertyDeclarationSyntax) as IPropertySymbol;
                    foreach (var attribute in propertySymbol.GetAttributes())
                    {
                        if (attribute.AttributeClass.ToDisplayString() == "intelliconShipping.DependsOnAttribute")
                        {
                            var fieldName = attribute.ConstructorArguments[0].Value.ToString();

                            if (DependentProperties.TryGetValue(fieldName, out var dependentProperties))
                                dependentProperties.Add(propertySymbol);
                            else
                                DependentProperties.Add(fieldName, new List<IPropertySymbol>() { propertySymbol });
                        }
                    }
                }
            }
        }
    }
}
