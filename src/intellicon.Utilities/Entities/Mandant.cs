﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace intellicon.Utilities.Entities
{
    [Table("KHKMandanten")]
    [DebuggerDisplay("{Id} - {Name}")]
    public class Mandant
    {
        [Column("Wert")]
        public string Name { get; set; }

        [Column("Mandant")]
        public short Id { get; set; }


        [Column("Eigenschaft")]
        public int Property { get; set; }
    }
}
