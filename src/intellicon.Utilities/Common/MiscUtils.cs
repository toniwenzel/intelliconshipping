﻿using System.Globalization;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace intellicon.Utilities.Common
{
    /// <summary>
    /// Utility methods
    /// </summary>
    public static class MiscUtils
    {
        /// <summary>
        /// Deseriaizes an XML file
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static T? DeserializeFile<T>(string filename) where T : new()
        {
            if (File.Exists(filename))
            {
                using (var reader = new StreamReader(filename))
                {
                    var serializer = new XmlSerializer(typeof(T));
                    var result = (T?)serializer.Deserialize(reader);

                    return result;
                }
            }

            return new T();

        }

        /// <summary>
        /// Serializes an object to xml
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="file"></param>
        /// <param name="value"></param>
        public static void SerializeFile<T>(string file, T value)
        {
            var dir = Directory.GetParent(file);
            if (dir != null && !dir.Exists)
                dir.Create();

            using (var writer = new StreamWriter(file))
            {
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(writer, value);
                writer.Close();
            }
        }

        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                static string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    string domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException e)
            {
                return false;
            }
            catch (ArgumentException e)
            {
                return false;
            }

            try
            {
                return Regex.IsMatch(email, @"^[^@\s]+@[^@\s]+\.[^@\s]{2,}$", RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }
    }
}
