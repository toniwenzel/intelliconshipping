﻿namespace intellicon.Utilities.DataAccess
{
    /// <summary>
    /// Database login information
    /// </summary>
    public class Login
    {
        /// <summary>
        /// Name of the database server
        /// </summary>
        public string? DatabaseServer { get; set; } = "localhost";

        /// <summary>
        /// Name of the database
        /// </summary>
        public string? DatabaseName { get; set; }

        /// <summary>
        /// Database login name
        /// </summary>
        public string? Username { get; set; }

        /// <summary>
        /// Login password
        /// </summary>
        public string? Password { get; set; }

        /// <summary>
        /// Authentication type
        /// </summary>
        public AuthenticationType AuthenticationType { get; set; } = AuthenticationType.Windows;
    }
}
