﻿using intellicon.Utilities.Entities;
using Microsoft.EntityFrameworkCore;

namespace intellicon.Utilities.DataAccess
{
    public interface ISageDbContext
    {
        DbSet<Mandant> Mandants { get; set; }
    }
}
