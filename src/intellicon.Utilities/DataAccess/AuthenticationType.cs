﻿using System.ComponentModel;

namespace intellicon.Utilities.DataAccess
{
    public enum AuthenticationType
    {
        [Description("Windows authentication")]
        Windows,
        [Description("Sql Server authentication")]
        SqlServer
    }
}
