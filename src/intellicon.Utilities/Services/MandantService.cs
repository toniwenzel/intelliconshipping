﻿using intellicon.Utilities.DataAccess;
using intellicon.Utilities.Entities;
using Microsoft.EntityFrameworkCore;

namespace intellicon.Utilities.Services
{
    public class MandantService : IMandantService
    {
        private ISageDbContext _context;

        public MandantService(ISageDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public List<Mandant> GetAll()
        {
            return _context.Mandants.Where(m => m.Property == 1).AsNoTracking().ToList();
        }
    }
}
