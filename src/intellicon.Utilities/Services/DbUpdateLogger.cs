﻿using DbUp.Engine.Output;
using System.Text;

namespace intellicon.Utilities.Services
{
    public class DbUpdateLogger : IUpgradeLog
    {
        private readonly string _fileName;
        private readonly IProgramVersion _programVersion;
        private readonly StringBuilder _buffer;
        private int _bufferEntryCount;

        private const int PREBUFFERED_ENTRY_COUNT = 3; // Buffer the first three entries. If there are more entries, than more action than a usual application startup is going on. This is to prevent cluttering and over filling the log file.

        public DbUpdateLogger(IFilePathProvider filePathProvider, IProgramVersion programVersion, IDbConnectionInfoService dbConnectionInfoService)
        {
            if (filePathProvider is null)
                throw new ArgumentNullException(nameof(filePathProvider));

            _fileName = Path.Combine(filePathProvider.GetLoggingPath(), "DbUpdate.log");
            _programVersion = programVersion ?? throw new ArgumentNullException(nameof(programVersion));
            _buffer = new StringBuilder();
            _buffer.AppendLine($"{DateTime.Now:G} Database upgrade to version {_programVersion.VersionString} ({_programVersion.InformationalVersion})");
            _buffer.AppendLine($"Database: {dbConnectionInfoService.GetDisplayConnectionString()} - {dbConnectionInfoService.GetSqlServerVersion()}");
        }

        public void WriteError(string format, params object[] args)
        {
            Write("Error:\t", format, args);
        }

        public void WriteInformation(string format, params object[] args)
        {
            Write("Information:\t", format, args);
        }

        public void WriteWarning(string format, params object[] args)
        {
            Write("Warning:\t", format, args);
        }

        private void Write(string prefix, string format, object[] args)
        {
            Write($"{prefix}{string.Format(format, args)}\r\n");
        }

        private void Write(string content)
        {
            if (_bufferEntryCount <= PREBUFFERED_ENTRY_COUNT)
            {
                _bufferEntryCount++;
                _buffer.Append(content);

                // Write all buffered text to file
                if (_bufferEntryCount == PREBUFFERED_ENTRY_COUNT + 1)
                    File.AppendAllText(_fileName, _buffer.ToString());
            }
            else
            {
                File.AppendAllText(_fileName, content);
            }
        }
    }
}
