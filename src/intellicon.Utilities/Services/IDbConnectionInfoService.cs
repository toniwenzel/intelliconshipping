﻿using System.Reflection;
using intellicon.Utilities.DataAccess;

namespace intellicon.Utilities.Services
{
    public interface IDbConnectionInfoService
    {
        /// <summary>
        /// Gets the db login data
        /// </summary>
        /// <returns></returns>
        Login? GetLogin();

        /// <summary>
        /// Rememer db login data
        /// </summary>
        /// <param name="login"></param>
        void UseLogin(Login login);

        /// <summary>
        /// Validate the login data
        /// </summary>
        /// <param name="login"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<ValueTuple<bool, string>> IsValidLoginAsync(Login login, CancellationToken cancellationToken);

        /// <summary>
        /// Validate the login data
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        bool IsValidLogin(Login login);

        /// <summary>
        /// Get the connection string for the current set login
        /// </summary>
        /// <returns></returns>
        string GetConnectionString();

        /// <summary>
        /// Gets the connection string without sensitive data
        /// </summary>
        /// <returns></returns>
        string GetDisplayConnectionString();

        /// <summary>
        /// Gets the sql server version
        /// </summary>
        /// <returns></returns>
        string GetSqlServerVersion();

        /// <summary>
        /// Gets the schema of the database
        /// </summary>
        /// <returns></returns>
        SchemaInfo GetSchema();

        /// <summary>
        /// Gets the table name for the db up journal
        /// </summary>
        /// <returns></returns>
        string GetJournalTableName();

        /// <summary>
        /// Gets the assembly containing the Sql scripts
        /// </summary>
        Assembly GetScriptAssembly();
    }
}
