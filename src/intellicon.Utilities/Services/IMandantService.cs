﻿using intellicon.Utilities.Entities;

namespace intellicon.Utilities.Services
{
    public interface IMandantService
    {
        /// <summary>
        /// Returns all mandants
        /// </summary>
        /// <returns></returns>
        List<Mandant> GetAll();
    }
}
