﻿namespace intellicon.Utilities.Services
{
    /// <summary>
    /// Service to retrieve the current selected/valid mandant
    /// </summary>
    public interface ICurrentMandantProvider
    {
        /// <summary>
        /// Will be raised when the current mandant has been changed
        /// </summary>
        event EventHandler OnCurrentMandantChanged;

        /// <summary>
        /// Gets the currently selected mandant id
        /// </summary>
        /// <returns></returns>
        short GetMandantId();

        /// <summary>
        /// Sets the current mandant id
        /// </summary>
        /// <param name="id"></param>
        void SetMandantId(short id);
    }
}
