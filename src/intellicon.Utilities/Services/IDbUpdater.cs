﻿namespace intellicon.Utilities.Services
{
    /// <summary>
    /// Interface to update the database (schema)
    /// </summary>
    public interface IDbUpdater
    {
        /// <summary>
        /// Performs a database update
        /// </summary>
        /// <returns></returns>
        bool UpgradeDatabase();

        /// <summary>
        /// Determines whether a db update is required
        /// </summary>
        /// <returns></returns>
        bool IsUpgradeRequired();
    }
}
