﻿namespace intellicon.Utilities.Services
{
    public interface IErrorHelper
    {
        /// <summary>
        /// Handles the exception
        /// </summary>
        /// <param name="exception">The exception to handle</param>
        void HandleException(Exception exception);
    }
}
