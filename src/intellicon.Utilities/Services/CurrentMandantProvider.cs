﻿namespace intellicon.Utilities.Services
{
    public class CurrentMandantProvider : ICurrentMandantProvider
    {
        private short _mandantId;

        /// <summary>
        /// Will be raised when the current mandant has been changed
        /// </summary>
        public event EventHandler OnCurrentMandantChanged;

        public short GetMandantId() => _mandantId;

        public void SetMandantId(short id)
        {
            if (id != _mandantId)
            {
                _mandantId = id;

                OnCurrentMandantChanged?.Invoke(this, EventArgs.Empty);
            }
        }
    }
}
