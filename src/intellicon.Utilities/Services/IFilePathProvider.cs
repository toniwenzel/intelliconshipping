﻿namespace intellicon.Utilities.Services
{
    public interface IFilePathProvider
    {
        string GetLoggingPath();
    }
}
