﻿using DbUp;
using DbUp.Engine;
using DbUp.Engine.Output;

namespace intellicon.Utilities.Services
{
    public class DbUpdater : IDbUpdater
    {
        private readonly IDbConnectionInfoService _dbConnectionInfoService;
        private readonly IEnumerable<IUpgradeLog> _loggers;
        private UpgradeEngine? _engine;

        public DbUpdater(IDbConnectionInfoService dbConnectionInfoService, IEnumerable<IUpgradeLog> loggers)
        {
            _dbConnectionInfoService = dbConnectionInfoService ?? throw new ArgumentNullException(nameof(dbConnectionInfoService));
            _loggers = loggers ?? throw new ArgumentNullException(nameof(loggers));
        }

        public bool UpgradeDatabase()
        {
            EnsureEngineWasBuilt();

            if (_engine == null)
                return false;

            var result = _engine.PerformUpgrade();

            return result.Successful;
        }

        public bool IsUpgradeRequired()
        {
            EnsureEngineWasBuilt();

            if (_engine == null)
                return false;

            return _engine.IsUpgradeRequired();
        }

        private void EnsureEngineWasBuilt()
        {
            if (_engine == null)
            {
                var builder = DeployChanges.To
                    .SqlDatabase(_dbConnectionInfoService.GetConnectionString())
                    .WithScriptsEmbeddedInAssembly(_dbConnectionInfoService.GetScriptAssembly())
                    .WithTransaction()
                    .JournalToSqlTable("", _dbConnectionInfoService.GetJournalTableName())
                    .LogToConsole()
                    .LogScriptOutput();

                foreach (var logger in _loggers)
                    builder.LogTo(logger);

                _engine = builder.Build();
            }
        }
    }
}
