﻿using System.Data;
using System.Data.Common;
using System.Reflection;
using intellicon.Utilities.DataAccess;
using Microsoft.Data.SqlClient;

namespace intellicon.Utilities.Services
{
    public class DbConnectionInfoService : IDbConnectionInfoService
    {
        private Login? _login;
        private readonly string _journalTableName;
        private readonly Assembly _scriptAssembly;

        public DbConnectionInfoService(string journalTableName, Assembly scriptAssembly)
        {
            if (string.IsNullOrEmpty(journalTableName))
            {
                throw new ArgumentException($"'{nameof(journalTableName)}' cannot be null or empty.", nameof(journalTableName));
            }

            _journalTableName = journalTableName;
            _scriptAssembly = scriptAssembly ?? throw new ArgumentNullException(nameof(scriptAssembly));
        }

        /// <summary>
        /// Gets the current login data
        /// </summary>
        /// <returns></returns>
        public Login? GetLogin() => _login;

        /// <summary>
        /// Sets the current login data
        /// </summary>
        /// <param name="login"></param>
        public void UseLogin(Login login) => _login = login;

        /// <summary>
        /// Determines whether the login data is valid. Aka it tries to connect to a database using the given credentials
        /// </summary>
        /// <param name="login">The login information</param>
        /// <param name="cancellationToken">Token to cancel the operation</param>
        /// <returns></returns>
        public async Task<ValueTuple<bool, string>> IsValidLoginAsync(Login login, CancellationToken cancellationToken)
        {
            try
            {
                var connectionString = BuildConnectionString(login, true);

                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync(cancellationToken);
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = "SELECT * from KHKMandanten WHERE 0=1";
                        await command.ExecuteNonQueryAsync();
                    }

                    return (connection.State == System.Data.ConnectionState.Open, "");
                }
            }
            catch (DbException e)
            {
                return (false, e.Message);
            }
        }

        /// <summary>
        /// Determines whether the login data is valid. Aka it tries to connect to a database using the given credentials
        /// </summary>
        /// <param name="login">The login information</param>
        /// <returns></returns>
        public bool IsValidLogin(Login login)
        {
            try
            {
                var connectionString = BuildConnectionString(login, true);

                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = "SELECT * from KHKMandanten WHERE 0=1";
                        command.ExecuteNonQuery();
                    }

                    return connection.State == System.Data.ConnectionState.Open;
                }
            }
            catch (DbException)
            {
                return false;
            }
        }

        /// <summary>
        /// Get the connection string for the current set login
        /// </summary>
        /// <returns></returns>
        public string GetConnectionString()
        {
            var login = GetLogin();

            if (login == null)
                throw new InvalidOperationException("No login data set yet. Please login first.");

            return BuildConnectionString(login, true);
        }

        /// <summary>
        /// Gets the connection string without sensitive data
        /// </summary>
        /// <returns></returns>
        public string GetDisplayConnectionString()
        {
            var login = GetLogin();

            if (login == null)
                throw new InvalidOperationException("No login data set yet. Please login first.");

            return BuildConnectionString(login, false);
        }

        /// <summary>
        /// Gets the sql server version
        /// </summary>
        /// <returns></returns>
        public string GetSqlServerVersion()
        {
            try
            {
                using (var connection = new SqlConnection(GetConnectionString()))
                {
                    connection.Open();
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = "SELECT @@Version as 'SqlServerVersion'";
                        var reader = command.ExecuteReader();

                        if (reader.Read())
                        {
                            var result = reader.GetString(0);

                            var indexOfEOL = result.IndexOf("\n");

                            if (indexOfEOL > 0)
                                result = result.Substring(0, indexOfEOL).Trim();

                            return result;
                        }
                    }
                }
            }
            catch (DbException e)
            {
                return e.Message;
            }

            return "Unknown";
        }

        /// <summary>
        /// Gets the schema of the database
        /// </summary>
        /// <returns></returns>
        public SchemaInfo GetSchema()
        {
            using (var connection = new SqlConnection(GetConnectionString()))
            {
                connection.Open();
                var schema = connection.GetSchema("Columns");

                var info = new SchemaInfo();
                SchemaTableInfo? tableInfo = null;

                foreach (DataRow row in schema.Rows)
                {
                    var tableName = row["TABLE_NAME"].ToString();

                    if (tableInfo == null || tableName != tableInfo.Name)
                    {
                        if (string.IsNullOrEmpty(tableName))
                            tableName = "Unknown";

                        tableInfo = new SchemaTableInfo(tableName);
                        info.Tables.Add(tableInfo);
                    }

                    if (tableInfo != null)
                    {
                        var columnName = row["COLUMN_NAME"].ToString();
                        if (!string.IsNullOrWhiteSpace(columnName))
                            tableInfo.Columns.Add(columnName);
                    }
                }

                return info;
            }
        }


        private static string BuildConnectionString(Login login, bool includeSensitiveData)
        {
            var builder = new SqlConnectionStringBuilder
            {
                DataSource = login.DatabaseServer
            };

            if (login.AuthenticationType == AuthenticationType.Windows)
                builder.IntegratedSecurity = true;

            if (!string.IsNullOrEmpty(login.DatabaseName))
                builder.InitialCatalog = login.DatabaseName;

            if (!string.IsNullOrEmpty(login.Username))
                builder.UserID = login.Username;

            if (!string.IsNullOrEmpty(login.Password) && includeSensitiveData)
                builder.Password = login.Password;

            return builder.ConnectionString;
        }

        public string GetJournalTableName() => _journalTableName;

        public Assembly GetScriptAssembly() => _scriptAssembly;
    }
}
