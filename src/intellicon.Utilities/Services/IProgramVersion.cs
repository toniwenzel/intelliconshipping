﻿namespace intellicon.Utilities.Services
{
    public interface IProgramVersion
    {
        /// <summary>
        /// Get the display version string
        /// </summary>
        string VersionString { get; }

        /// <summary>
        /// Gets the internal information version text
        /// </summary>
        string InformationalVersion { get; }
    }
}
