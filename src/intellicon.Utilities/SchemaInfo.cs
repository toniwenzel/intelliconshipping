﻿using System.Diagnostics;

namespace intellicon.Utilities
{
    public class SchemaInfo
    {
        public List<SchemaTableInfo> Tables { get; } = new List<SchemaTableInfo>();
    }

    [DebuggerDisplay("{Name}")]
    public class SchemaTableInfo
    {
        public string Name { get; }
        public List<string> Columns { get; } = new List<string>();

        public SchemaTableInfo(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException($"'{nameof(name)}' cannot be null or whitespace.", nameof(name));

            Name = name;
        }
    }
}
