﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using intellicon.Utilities.Entities;
using intellicon.Utilities.Services;
using intelliconShipping.Core.Services;
using intelliconShipping.Services;
using intelliconShipping.Views;
using intelliconShipping.Views.Settings;

namespace intelliconShipping
{
    public partial class MainPresenter : Presenter
    {
        private readonly IDbConnectionInfoService _dbConnectionInfoService;
        private readonly IPresenterFactory _presenterFactory;
        private readonly ILicenseService _licenseService;
        private readonly IMessageBoxService _messageBoxService;
        private readonly IMandantService _mandantService;
        private readonly ICurrentMandantProvider _currentMandantProvider;
        private ShippingPresenter _shippingPresenter;
        private const string TITLE_PREFIX = "intellicon Shipping";


        [intelliconShipping.AutoNotify]
        public bool _isValid;

        [intelliconShipping.AutoNotify]
        public Mandant _currentMandant;

        public string Title { get; set; }

        public string ConnectionInfo { get; set; }

        public List<Mandant> Mandants { get; set; }

        public MainPresenter(IDbConnectionInfoService dbConnectionInfoService,
                            IErrorHelper errorHelper, IPresenterFactory presenterFactory, ILicenseService licenseService, IMessageBoxService messageBoxService, IMandantService mandantService, ICurrentMandantProvider currentMandantProvider)
           : base(errorHelper)
        {
            Title = TITLE_PREFIX;
            _dbConnectionInfoService = dbConnectionInfoService ?? throw new System.ArgumentNullException(nameof(dbConnectionInfoService));
            _presenterFactory = presenterFactory ?? throw new ArgumentNullException(nameof(presenterFactory));
            _licenseService = licenseService ?? throw new ArgumentNullException(nameof(licenseService));
            _messageBoxService = messageBoxService ?? throw new ArgumentNullException(nameof(messageBoxService));
            _mandantService = mandantService ?? throw new ArgumentNullException(nameof(mandantService));
            _currentMandantProvider = currentMandantProvider ?? throw new ArgumentNullException(nameof(currentMandantProvider));
        }

        protected override Control CreateForm() => new MainView();

        private void UpdateConnectionInfo()
        {
            var login = _dbConnectionInfoService.GetLogin();
            ConnectionInfo = $"{login.DatabaseServer}\\{login.DatabaseName}";
        }

        public void Exit()
        {
            Environment.Exit(0);
        }

        public void ShowSettings()
        {
            _presenterFactory.Show<SettingsPresenter>(this);
        }

        public void ShowAbout()
        {
            _presenterFactory.Show<AboutPresenter>(this);
        }

        protected override void OnInit()
        {
            UpdateConnectionInfo();

            Mandants = _mandantService.GetAll();
            if (UserSettings.Default.MandantId > 0)
                CurrentMandant = Mandants.FirstOrDefault(m => m.Id == UserSettings.Default.MandantId);

            if (UserSettings.Default.MandantId == 0 || CurrentMandant == null)
                CurrentMandant = Mandants.FirstOrDefault();

        }

        public async Task CheckLicense()
        {
            var license = _licenseService.ReadLicense();
            if (license == null)
            {
                _messageBoxService.Show("Es sind keine Lizenzdaten hinterlegt. Bitte öffnen Sie die Einstellungen und vervollständigen die Lizenzierungseinstellungen.", "Lizenzprüfung");
                ShowSettings();
                await CheckLicense();
            }
            else
            {
                var licenseResult = await _licenseService.ValidateLicense(license);
                if (!licenseResult.IsValid)
                {
                    _messageBoxService.Show("Es sind ungültige Lizenzdaten hinterlegt. Bitte öffnen Sie die Einstellungen und korrigieren die Lizenzierungseinstellungen.", "Lizenzprüfung");
                    ShowSettings();
                    await CheckLicense();
                }
                else
                {
                    _shippingPresenter = _presenterFactory.Show<ShippingPresenter>(this);
                    IsValid = true;
                }

            }
        }

        public void ShowHelp()
        {
            System.Diagnostics.Process.Start("explorer", "https://support.intellicon.de/produkte/versandmodul-versandschnittstelle/");
        }

        public void TestReceipt()
        {
            _shippingPresenter.TestReceipt();
        }

        protected void OnCurrentMandantChanged()
        {
            _currentMandantProvider.SetMandantId(CurrentMandant.Id);
            if (UserSettings.Default.MandantId != CurrentMandant.Id)
            {
                UserSettings.Default.MandantId = CurrentMandant.Id;
                UserSettings.Default.Save();
            }
        }
    }
}
