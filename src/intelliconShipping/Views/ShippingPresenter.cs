﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using intellicon.Utilities.Services;
using intelliconShipping.Core.DataAccess;
using intelliconShipping.Core.Entities;
using intelliconShipping.Core.Services;

namespace intelliconShipping.Views
{
    public partial class ShippingPresenter : Presenter
    {
        private readonly IShippingTypesRepository _shippingTypesRepository;
        private readonly ICurrentMandantProvider _currentMandantProvider;
        private readonly IReceiptRepository _receiptRepository;
        private readonly IReceiptNumberScanner _numberScanner;
        private readonly IShippingService _shippingService;

        public IEnumerable<ShippingType> ShippingTypes { get; set; }

        [AutoNotify]
        private Parcel _parcel;

        [AutoNotify]
        private string _error;

        [AutoNotify]
        private string _deliveryAddress;

        [AutoNotify]
        private bool _isValid;

        [DependsOn(nameof(IsValid))]
        [DependsOn(nameof(Parcel))]
        public bool IsDebtDelivery => IsValid && Parcel?.IsDebtDelivery == true;


        public ShippingPresenter(IErrorHelper errorHelper,
                                 IShippingTypesRepository shippingTypesRepository,
                                 ICurrentMandantProvider currentMandantProvider,
                                 IReceiptRepository receiptRepository,
                                 IReceiptNumberScanner numberScanner,
                                 IShippingService shippingService)
            : base(errorHelper)
        {
            _shippingTypesRepository = shippingTypesRepository ?? throw new System.ArgumentNullException(nameof(shippingTypesRepository));
            _currentMandantProvider = currentMandantProvider ?? throw new ArgumentNullException(nameof(currentMandantProvider));
            _receiptRepository = receiptRepository ?? throw new ArgumentNullException(nameof(receiptRepository));
            _numberScanner = numberScanner ?? throw new ArgumentNullException(nameof(numberScanner));
            _shippingService = shippingService ?? throw new ArgumentNullException(nameof(shippingService));
        }

        protected override void OnInit()
        {
            ShippingTypes = _shippingTypesRepository.GetShippingTypes();
            ResetState();
        }

        public async Task Ship()
        {
            if (Parcel != null)
            {
                await _shippingService.Ship(Parcel);
                ResetState();
            }
        }

        /// <summary>
        /// Searches the receipt
        /// </summary>
        /// <param name="text"></param>
        /// <remarks>Zeile 4773 frmMain</remarks>
        public bool ScanReceipt(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return false;

            ResetState();

            var scanResult = _numberScanner.Scan(text);

            if (!scanResult.IsValid)
                return false;

            var receipt = _receiptRepository.GetReceiptByScan(scanResult);

            if (receipt == null)
            {
                Error = "Beleg existiert nicht";
                return true;
            }
            else
            {
                Parcel = _shippingService.CreateParcel(receipt);
                Error = Parcel.Error;
                DeliveryAddress = Parcel.GetDeliveryAddress();
                IsValid = true;
                return true;
            }
        }

        protected override Control CreateForm() => new ShippingControl();

        /// <summary>
        /// Enters the test/demo receipt
        /// </summary>
        public void TestReceipt()
        {
            var lastReceiptId = _receiptRepository.GetLastReceiptId();

            if (lastReceiptId > 0)
            {
                ScanReceipt($"$M{_currentMandantProvider.GetMandantId()}$I{lastReceiptId}"); //Zeile 4796
            }
        }

        private void ResetState()
        {
            Error = string.Empty;
            IsValid = false;
            DeliveryAddress = string.Empty;
            Parcel = new Parcel();
        }
    }
}
