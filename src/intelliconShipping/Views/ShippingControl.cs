﻿using System.Windows.Forms;
using DevExpress.XtraEditors;
using intelliconShipping.Common;
using intelliconShipping.Core.Entities;

namespace intelliconShipping.Views
{
    public partial class ShippingControl : XtraUserControl, IForm
    {
        private ShippingPresenter _presenter;

        public ShippingControl()
        {
            InitializeComponent();
        }

        public void Init(IPresenter presenter)
        {
            _presenter = presenter as ShippingPresenter;

            cboShippingType.Properties.DataSource = _presenter.ShippingTypes;
            cboShippingType.Properties.DisplayMember = nameof(ShippingType.Description);
            cboShippingType.Properties.ValueMember = nameof(ShippingType.Type);
            cboShippingType.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo { FieldName = nameof(ShippingType.Description) });

            _presenter.SetBinding(txtReceiptNumber, x => x.Text, p => p.Parcel.ReceiptNumber);
            _presenter.SetBinding(txtDeliveryAddress, x => x.Text, p => p.DeliveryAddress);
            _presenter.SetBinding(txtDeliveryConditions, x => x.Text, p => p.Parcel.DeliveryCondition);
            _presenter.SetBinding(txtDebtAmount, x => x.EditValue, p => p.Parcel.DebtAmount);
            _presenter.SetBinding(txtReference, x => x.Text, p => p.Parcel.Reference);
            _presenter.SetBinding(gridPackages, x => x.DataSource, p => p.Parcel.Packages);
            _presenter.SetBinding(lblError, x => x.Text, p => p.Error);
            _presenter.SetBinding(cboShippingType, x => x.EditValue, p => p.Parcel.ShippingType);

            _presenter.SetBinding(lblDebtAmount, x => x.Visible, p => p.IsDebtDelivery);
            _presenter.SetBinding(txtDebtAmount, x => x.Visible, p => p.IsDebtDelivery);
            _presenter.SetBinding(pnlDetails, x => x.Visible, p => p.IsValid);
            _presenter.SetTrigger(p => p.IsDebtDelivery, b => colPrice.Visible = b);
        }

        private void txtReceipt_Properties_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _presenter.ScanReceipt(txtReceipt.Text);
        }

        private void btnPackages_Click(object sender, System.EventArgs e)
        {
            _presenter.Parcel.SetPackagesAmount(int.Parse(((Control)sender).Name.Substring(11)));
            gridPackages.RefreshDataSource();
        }

        private async void btnOutput_Click(object sender, System.EventArgs e)
        {
            await _presenter.Ship();
        }
    }
}
