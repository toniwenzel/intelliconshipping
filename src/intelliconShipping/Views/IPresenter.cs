﻿using System.Windows.Forms;

namespace intelliconShipping.Views
{
    public interface IPresenter
    {
        Control GetForm();
        void Init();
        void Closing();
    }
}
