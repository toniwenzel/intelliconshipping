﻿namespace intelliconShipping.Views
{
    public interface IForm
    {
        void Init(IPresenter presenter);
    }
}
