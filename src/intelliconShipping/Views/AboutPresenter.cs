﻿using System.Windows.Forms;
using intellicon.Utilities.Services;
using intelliconShipping.Services;

namespace intelliconShipping.Views
{
    public class AboutPresenter : Presenter
    {
        public AboutPresenter(IErrorHelper errorHelper) : base(errorHelper)
        {
        }

        public string Version { get; set; }
        public string ErrorLogPath { get; set; }

        protected override Control CreateForm() => new AboutForm();

        protected override void OnInit()
        {
            Version = $"Version: {ProgramVersion.Instance.VersionString}";
            ErrorLogPath = ExceptionManager.Instance.TraceFilePath;
        }
    }
}
