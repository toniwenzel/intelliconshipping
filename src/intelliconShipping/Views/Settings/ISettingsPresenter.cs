﻿namespace intelliconShipping.Views.Settings
{
    public interface ISettingsPresenter
    {
        void SaveSettings();
    }
}
