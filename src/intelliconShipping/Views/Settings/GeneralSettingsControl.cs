﻿using System;
using DevExpress.XtraEditors;
using intelliconShipping.Common;

namespace intelliconShipping.Views.Settings
{
    public partial class GeneralSettingsControl : XtraUserControl, IForm
    {
        private GeneralSettingsPresenter _presenter;

        public GeneralSettingsControl()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            InitSkinMenu();
            DevExpress.LookAndFeel.UserLookAndFeel.Default.StyleChanged += OnSkinChanged;
        }

        private void OnSkinChanged(object sender, EventArgs e)
        {
            _presenter.SkinName = SkinHelper.ActiveSkinName;
            _presenter.SkinPaletteName = SkinHelper.ActiveSvgPaletteName;
        }

        private void InitSkinMenu()
        {
            DevExpress.XtraBars.Helpers.SkinHelper.InitSkinGallery(popupSkin);
            DevExpress.XtraBars.Helpers.SkinHelper.InitSkinPaletteGallery(popupPalette);

            popupSkin.EditValue = SkinHelper.ActiveSkinName;
            popupPalette.EditValue = SkinHelper.ActiveSvgPaletteName;
        }

        public void Init(IPresenter presenter)
        {
            _presenter = (GeneralSettingsPresenter)presenter;
        }
    }
}
