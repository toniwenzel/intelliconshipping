﻿namespace intelliconShipping.Views.Settings
{
    partial class GeneralSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.popupSkin = new DevExpress.XtraEditors.PopupGalleryEdit();
            this.popupPalette = new DevExpress.XtraEditors.PopupGalleryEdit();
            ((System.ComponentModel.ISupportInitialize)(this.popupSkin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupPalette.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(3, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(102, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Anwendungs-Design:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(3, 48);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(62, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Farbschema:";
            // 
            // popupSkin
            // 
            this.popupSkin.EditValue = "";
            this.popupSkin.Location = new System.Drawing.Point(3, 22);
            this.popupSkin.Name = "popupSkin";
            this.popupSkin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupSkin.Size = new System.Drawing.Size(204, 20);
            this.popupSkin.TabIndex = 2;
            // 
            // popupPalette
            // 
            this.popupPalette.EditValue = "";
            this.popupPalette.Location = new System.Drawing.Point(3, 67);
            this.popupPalette.Name = "popupPalette";
            this.popupPalette.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupPalette.Size = new System.Drawing.Size(204, 20);
            this.popupPalette.TabIndex = 3;
            // 
            // GeneralSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.popupPalette);
            this.Controls.Add(this.popupSkin);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Name = "GeneralSettingsControl";
            this.Size = new System.Drawing.Size(421, 212);            
            ((System.ComponentModel.ISupportInitialize)(this.popupSkin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupPalette.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.PopupGalleryEdit popupSkin;
        private DevExpress.XtraEditors.PopupGalleryEdit popupPalette;
    }
}
