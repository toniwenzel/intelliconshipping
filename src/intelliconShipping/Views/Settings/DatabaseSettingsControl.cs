﻿using DevExpress.XtraEditors;
using intelliconShipping.Common;

namespace intelliconShipping.Views.Settings
{
    public partial class DatabaseSettingsControl : XtraUserControl, IForm
    {
        public DatabaseSettingsControl()
        {
            InitializeComponent();
        }

        public void Init(IPresenter presenter)
        {
            var dbSettingsPresenter = presenter as DatabaseSettingsPresenter;
            dbSettingsPresenter.SetBinding(txtServer, x => x.Text, p => p.DatabaseServer);
            dbSettingsPresenter.SetBinding(txtDatabaseName, x => x.Text, p => p.DatabaseName);
            dbSettingsPresenter.SetBinding(txtUsername, x => x.Text, p => p.Username);
            dbSettingsPresenter.SetBinding(txtPassword, x => x.Text, p => p.Password);
            dbSettingsPresenter.SetBinding(cboAuthentication, x => x.EditValue, p => p.AuthenticationType, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged);
            dbSettingsPresenter.SetBinding(lblError, x => x.Text, p => p.LoginError);
            dbSettingsPresenter.SetBinding(imgBoxCheck, x => x.Visible, p => p.IsLoginValid);

            cboAuthentication.Properties.Items.AddRange(dbSettingsPresenter.GetAuthenticationTypes());

            dbSettingsPresenter.BindCommand(btnLogin, p => p.CheckLogin());
            dbSettingsPresenter.SetTrigger(p => p.AuthenticationType, v => UpdateReadOnlyControls(dbSettingsPresenter.CanEditUserName));

            UpdateReadOnlyControls(dbSettingsPresenter.CanEditUserName);
        }

        private void UpdateReadOnlyControls(bool canEditUserName)
        {
            txtPassword.Enabled = txtUsername.Enabled = canEditUserName;
            lblPassword.Enabled = lblUsername.Enabled = canEditUserName;
        }
    }
}
