﻿using intelliconShipping.Common;

namespace intelliconShipping.Views.Settings
{
    public partial class LicenseControl : DevExpress.XtraEditors.XtraUserControl, IForm
    {
        public LicenseControl()
        {
            InitializeComponent();
        }

        public void Init(IPresenter presenter)
        {
            var licPresenter = presenter as LicensePresenter;
            licPresenter.SetBinding(txtCustomerNumber, x => x.Text, p => p.CustomerNumber);
            licPresenter.SetBinding(txtName, x => x.Text, p => p.CustomerName);
            licPresenter.SetBinding(lblError, x => x.Text, p => p.LicenseError);
            licPresenter.SetBinding(imgBoxCheck, x => x.Visible, p => p.IsLicenseValid);

            licPresenter.BindCommand(btnCheckLicense, p => p.CheckLicense());
        }
    }
}
