﻿namespace intelliconShipping.Views.Settings
{
    partial class GeneralShippingControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtFixedWeightDomestic = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.chkUserFieldsInactive = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.chkContactFromAsp = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtStatusEmailAddress = new DevExpress.XtraEditors.TextEdit();
            this.txtDeliveryEmailAddress = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFixedWeightDomestic.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUserFieldsInactive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkContactFromAsp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusEmailAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryEmailAddress.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(5, 6);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(103, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Fixes Gewicht Inland:";
            // 
            // txtFixedWeightDomestic
            // 
            this.txtFixedWeightDomestic.Location = new System.Drawing.Point(145, 3);
            this.txtFixedWeightDomestic.Name = "txtFixedWeightDomestic";
            this.txtFixedWeightDomestic.Properties.MaskSettings.Set("MaskManagerType", typeof(DevExpress.Data.Mask.NumericMaskManager));
            this.txtFixedWeightDomestic.Properties.MaskSettings.Set("mask", "f");
            this.txtFixedWeightDomestic.Size = new System.Drawing.Size(100, 20);
            this.txtFixedWeightDomestic.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(5, 32);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(110, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "User-Felder (Adresse):";
            // 
            // chkUserFieldsInactive
            // 
            this.chkUserFieldsInactive.Location = new System.Drawing.Point(145, 29);
            this.chkUserFieldsInactive.Name = "chkUserFieldsInactive";
            this.chkUserFieldsInactive.Properties.Caption = "ignorieren";
            this.chkUserFieldsInactive.Size = new System.Drawing.Size(100, 20);
            this.chkUserFieldsInactive.TabIndex = 3;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(5, 55);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(111, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Kontaktdaten aus ASP:";
            // 
            // chkContactFromAsp
            // 
            this.chkContactFromAsp.Location = new System.Drawing.Point(145, 55);
            this.chkContactFromAsp.Name = "chkContactFromAsp";
            this.chkContactFromAsp.Properties.Caption = "verwenden";
            this.chkContactFromAsp.Size = new System.Drawing.Size(78, 20);
            this.chkContactFromAsp.TabIndex = 5;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(5, 86);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(62, 13);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Fehler-Email:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(5, 112);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(63, 13);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "Zustell-Email:";
            // 
            // txtStatusEmailAddress
            // 
            this.txtStatusEmailAddress.Location = new System.Drawing.Point(145, 81);
            this.txtStatusEmailAddress.Name = "txtStatusEmailAddress";
            this.txtStatusEmailAddress.Size = new System.Drawing.Size(224, 20);
            this.txtStatusEmailAddress.TabIndex = 8;
            this.txtStatusEmailAddress.Validating += new System.ComponentModel.CancelEventHandler(this.txtStatusEmailAddress_Validating);
            // 
            // txtDeliveryEmailAddress
            // 
            this.txtDeliveryEmailAddress.Location = new System.Drawing.Point(145, 107);
            this.txtDeliveryEmailAddress.Name = "txtDeliveryEmailAddress";
            this.txtDeliveryEmailAddress.Size = new System.Drawing.Size(224, 20);
            this.txtDeliveryEmailAddress.TabIndex = 9;
            this.txtDeliveryEmailAddress.Validating += new System.ComponentModel.CancelEventHandler(this.txtDeliveryEmailAddress_Validating);
            // 
            // GeneralShippingControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtDeliveryEmailAddress);
            this.Controls.Add(this.txtStatusEmailAddress);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.chkContactFromAsp);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.chkUserFieldsInactive);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.txtFixedWeightDomestic);
            this.Controls.Add(this.labelControl1);
            this.Name = "GeneralShippingControl";
            this.Size = new System.Drawing.Size(528, 236);
            ((System.ComponentModel.ISupportInitialize)(this.txtFixedWeightDomestic.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUserFieldsInactive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkContactFromAsp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusEmailAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryEmailAddress.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtFixedWeightDomestic;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.CheckEdit chkUserFieldsInactive;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.CheckEdit chkContactFromAsp;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtStatusEmailAddress;
        private DevExpress.XtraEditors.TextEdit txtDeliveryEmailAddress;
    }
}
