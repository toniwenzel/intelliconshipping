﻿using System.Windows.Forms;
using intellicon.Utilities.Services;
using intelliconShipping.Core.Services;

namespace intelliconShipping.Views.Settings
{
    public partial class GeneralShippingPresenter : Presenter, ISettingsPresenter
    {
        private readonly IConfigurationService _configurationService;

        [intelliconShipping.AutoNotify]
        private decimal _fixedWeightDomestic;
        [intelliconShipping.AutoNotify]
        private bool _userFieldsInactive;
        [intelliconShipping.AutoNotify]
        private bool _useContactDataFromAsp;
        [intelliconShipping.AutoNotify]
        private string _statusEmailAddress;
        [intelliconShipping.AutoNotify]
        private string _deliveryEmailAddress;

        public GeneralShippingPresenter(IErrorHelper errorHelper, IConfigurationService configurationService)
            : base(errorHelper)
        {
            _configurationService = configurationService ?? throw new System.ArgumentNullException(nameof(configurationService));
        }

        protected override Control CreateForm() => new GeneralShippingControl();

        protected override void OnInit()
        {
            FixedWeightDomestic = _configurationService.ReadDecimal(ConfigurationKeys.FixedWeightDomestic);
            UserFieldsInactive = _configurationService.ReadBool(ConfigurationKeys.IgnoreUserFields);
            UseContactDataFromAsp = _configurationService.ReadBool(ConfigurationKeys.UseContactDataFromAsp);
            StatusEmailAddress = _configurationService.ReadValue(ConfigurationKeys.StatusEmailAddress);
            DeliveryEmailAddress = _configurationService.ReadValue(ConfigurationKeys.DeliveryEmailAddress);
        }

        public void SaveSettings()
        {
            _configurationService.WriteValue(ConfigurationKeys.FixedWeightDomestic, FixedWeightDomestic);
            _configurationService.WriteValue(ConfigurationKeys.IgnoreUserFields, UserFieldsInactive);
            _configurationService.WriteValue(ConfigurationKeys.UseContactDataFromAsp, UseContactDataFromAsp);
            _configurationService.WriteValue(ConfigurationKeys.StatusEmailAddress, StatusEmailAddress);
            _configurationService.WriteValue(ConfigurationKeys.DeliveryEmailAddress, DeliveryEmailAddress);
        }

    }
}
