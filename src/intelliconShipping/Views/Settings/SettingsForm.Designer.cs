﻿namespace intelliconShipping.Views
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.navBarControl1 = new DevExpress.XtraNavBar.NavBarControl();
            this.navBarGroupShipping = new DevExpress.XtraNavBar.NavBarGroup();
            this.biGeneralShipping = new DevExpress.XtraNavBar.NavBarItem();
            this.biShippingTypes = new DevExpress.XtraNavBar.NavBarItem();
            this.biShipper = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroupGeneral = new DevExpress.XtraNavBar.NavBarGroup();
            this.biGeneral = new DevExpress.XtraNavBar.NavBarItem();
            this.biLicense = new DevExpress.XtraNavBar.NavBarItem();
            this.biDatabase = new DevExpress.XtraNavBar.NavBarItem();
            this.pnlSettings = new DevExpress.XtraEditors.PanelControl();
            this.pnlHeader = new DevExpress.XtraEditors.PanelControl();
            this.lblHeader = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHeader)).BeginInit();
            this.pnlHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // navBarControl1
            // 
            this.navBarControl1.ActiveGroup = this.navBarGroupShipping;
            this.navBarControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.navBarControl1.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroupGeneral,
            this.navBarGroupShipping});
            this.navBarControl1.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.biGeneral,
            this.biShippingTypes,
            this.biShipper,
            this.biLicense,
            this.biDatabase,
            this.biGeneralShipping});
            this.navBarControl1.Location = new System.Drawing.Point(0, 0);
            this.navBarControl1.Name = "navBarControl1";
            this.navBarControl1.OptionsNavPane.ExpandedWidth = 141;
            this.navBarControl1.OptionsNavPane.ShowExpandButton = false;
            this.navBarControl1.OptionsNavPane.ShowOverflowButton = false;
            this.navBarControl1.OptionsNavPane.ShowOverflowPanel = false;
            this.navBarControl1.OptionsNavPane.ShowSplitter = false;
            this.navBarControl1.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.NavigationPane;
            this.navBarControl1.ShowIcons = DevExpress.Utils.DefaultBoolean.True;
            this.navBarControl1.Size = new System.Drawing.Size(141, 419);
            this.navBarControl1.TabIndex = 0;
            this.navBarControl1.Text = "navBarControl1";
            this.navBarControl1.ActiveGroupChanged += new DevExpress.XtraNavBar.NavBarGroupEventHandler(this.navBarControl1_ActiveGroupChanged);
            this.navBarControl1.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarControl1_LinkClicked);
            // 
            // navBarGroupShipping
            // 
            this.navBarGroupShipping.Caption = "Versand";
            this.navBarGroupShipping.Expanded = true;
            this.navBarGroupShipping.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("navBarGroupShipping.ImageOptions.SvgImage")));
            this.navBarGroupShipping.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.biGeneralShipping),
            new DevExpress.XtraNavBar.NavBarItemLink(this.biShippingTypes),
            new DevExpress.XtraNavBar.NavBarItemLink(this.biShipper)});
            this.navBarGroupShipping.Name = "navBarGroupShipping";
            // 
            // biGeneralShipping
            // 
            this.biGeneralShipping.Caption = "Allgemein";
            this.biGeneralShipping.Name = "biGeneralShipping";
            // 
            // biShippingTypes
            // 
            this.biShippingTypes.Caption = "Versandarten";
            this.biShippingTypes.Name = "biShippingTypes";
            // 
            // biShipper
            // 
            this.biShipper.Caption = "Paketdienste";
            this.biShipper.Name = "biShipper";
            // 
            // navBarGroupGeneral
            // 
            this.navBarGroupGeneral.Caption = "Allgemein";
            this.navBarGroupGeneral.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("navBarGroupGeneral.ImageOptions.SvgImage")));
            this.navBarGroupGeneral.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.biGeneral),
            new DevExpress.XtraNavBar.NavBarItemLink(this.biLicense),
            new DevExpress.XtraNavBar.NavBarItemLink(this.biDatabase)});
            this.navBarGroupGeneral.Name = "navBarGroupGeneral";
            // 
            // biGeneral
            // 
            this.biGeneral.Caption = "Allgemein";
            this.biGeneral.Name = "biGeneral";
            // 
            // biLicense
            // 
            this.biLicense.Caption = "Lizenz";
            this.biLicense.Name = "biLicense";
            // 
            // biDatabase
            // 
            this.biDatabase.Caption = "Datenbank";
            this.biDatabase.Name = "biDatabase";
            // 
            // pnlSettings
            // 
            this.pnlSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSettings.Location = new System.Drawing.Point(141, 32);
            this.pnlSettings.Name = "pnlSettings";
            this.pnlSettings.Padding = new System.Windows.Forms.Padding(10);
            this.pnlSettings.Size = new System.Drawing.Size(600, 387);
            this.pnlSettings.TabIndex = 1;
            // 
            // pnlHeader
            // 
            this.pnlHeader.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlHeader.Controls.Add(this.lblHeader);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(141, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(600, 32);
            this.pnlHeader.TabIndex = 0;
            // 
            // lblHeader
            // 
            this.lblHeader.Location = new System.Drawing.Point(4, 9);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(63, 13);
            this.lblHeader.TabIndex = 0;
            this.lblHeader.Text = "labelControl1";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(741, 419);
            this.Controls.Add(this.pnlSettings);
            this.Controls.Add(this.pnlHeader);
            this.Controls.Add(this.navBarControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Einstellungen";
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHeader)).EndInit();
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraNavBar.NavBarControl navBarControl1;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroupGeneral;
        private DevExpress.XtraNavBar.NavBarItem biGeneral;
        private DevExpress.XtraNavBar.NavBarItem biLicense;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroupShipping;
        private DevExpress.XtraNavBar.NavBarItem biShippingTypes;
        private DevExpress.XtraNavBar.NavBarItem biShipper;
        private DevExpress.XtraNavBar.NavBarItem biDatabase;
        private DevExpress.XtraEditors.PanelControl pnlSettings;
        private DevExpress.XtraEditors.PanelControl pnlHeader;
        private DevExpress.XtraEditors.LabelControl lblHeader;
        private DevExpress.XtraNavBar.NavBarItem biGeneralShipping;
    }
}
