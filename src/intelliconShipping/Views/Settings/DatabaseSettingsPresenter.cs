﻿using System;
using System.Collections;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using intellicon.Utilities.DataAccess;
using intellicon.Utilities.Services;
using intelliconShipping.Services;

namespace intelliconShipping.Views.Settings
{
    public partial class DatabaseSettingsPresenter : Presenter, ISettingsPresenter
    {
        protected readonly IDbConnectionInfoService _dbConnectionInfoService;

        [intelliconShipping.AutoNotify]
        public string _loginError;

        [intelliconShipping.AutoNotify]
        public bool _isLoginValid;

        [intelliconShipping.AutoNotify]
        public AuthenticationType _authenticationType;

        public bool CanEditUserName { get => AuthenticationType == AuthenticationType.SqlServer; }
        public string DatabaseName { get; set; }
        public string DatabaseServer { get; set; }
        public string DatabasePassword { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }


        public DatabaseSettingsPresenter(IErrorHelper errorHelper, IDbConnectionInfoService dbConnectionInfoService)
            : base(errorHelper)
        {
            _dbConnectionInfoService = dbConnectionInfoService ?? throw new System.ArgumentNullException(nameof(dbConnectionInfoService));
        }

        public void SaveSettings()
        {
            UserSettings.Default.AuthenticationType = AuthenticationType;
            UserSettings.Default.DatabaseName = DatabaseName;
            UserSettings.Default.DatabaseServer = DatabaseServer;
            UserSettings.Default.Username = Username;
            UserSettings.Default.Password = Password;
            UserSettings.Default.Save();
        }

        protected override Control CreateForm() => new DatabaseSettingsControl();

        protected override void OnInit()
        {
            AuthenticationType = UserSettings.Default.AuthenticationType;
            DatabaseName = UserSettings.Default.DatabaseName;
            DatabaseServer = UserSettings.Default.DatabaseServer;
            Username = UserSettings.Default.Username;
            Password = UserSettings.Default.Password;
        }

        public virtual async Task CheckLogin()
        {
            try
            {
                (IsLoginValid, LoginError) = await _dbConnectionInfoService.IsValidLoginAsync(GetLogin(), CancellationToken.None);
            }
            catch (OperationCanceledException)
            {
                LoginError = "";
            }
            catch (Exception e)
            {
                LoginError = $"Vorgang fehlgeschlagen: {e.Message}";
                ExceptionManager.TraceException(e);
            }
        }

        protected intellicon.Utilities.DataAccess.Login GetLogin()
        {
            return new intellicon.Utilities.DataAccess.Login
            {
                AuthenticationType = AuthenticationType,
                DatabaseName = DatabaseName,
                DatabaseServer = DatabaseServer,
                Password = Password,
                Username = Username,
            };
        }

        internal ICollection GetAuthenticationTypes()
        {
            return typeof(AuthenticationType).GetEnumValues();
        }
    }
}
