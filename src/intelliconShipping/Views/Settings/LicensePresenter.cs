﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using intellicon.Utilities.Services;
using intelliconShipping.Core.Services;
using intelliconShipping.Services;

namespace intelliconShipping.Views.Settings
{
    public partial class LicensePresenter : Presenter, ISettingsPresenter
    {
        private readonly ILicenseService _licenseService;
        [intelliconShipping.AutoNotify]
        private string _licenseError;

        [intelliconShipping.AutoNotify]
        private bool _isLicenseValid;

        public string CustomerName { get; set; }
        public string CustomerNumber { get; set; }

        public LicensePresenter(IErrorHelper errorHelper, ILicenseService licenseService)
            : base(errorHelper)
        {
            _licenseService = licenseService ?? throw new System.ArgumentNullException(nameof(licenseService));
        }

        public async Task CheckLicense()
        {
            try
            {
                var result = await _licenseService.ValidateLicense(GetLicenseInfo());
                IsLicenseValid = result.IsValid;
                LicenseError = result.Error;
            }
            catch (OperationCanceledException)
            {
                LicenseError = "";
            }
            catch (Exception e)
            {
                LicenseError = $"Vorgang fehlgeschlagen: {e.Message}";
                ExceptionManager.TraceException(e);
            }
        }

        protected override Control CreateForm() => new LicenseControl();

        protected override void OnInit()
        {
            var license = _licenseService.ReadLicense();

            if (license != null)
            {
                CustomerName = license.CustomerName;
                CustomerNumber = license.CustomerNumber;
            }
        }

        public void SaveSettings()
        {
            _licenseService.WriteLicense(GetLicenseInfo());
        }

        private Core.LicenseInfo GetLicenseInfo()
        {
            return new Core.LicenseInfo
            {
                CustomerName = CustomerName,
                CustomerNumber = CustomerNumber
            };
        }
    }
}
