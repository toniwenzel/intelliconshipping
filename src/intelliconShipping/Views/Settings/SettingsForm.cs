﻿using System.Drawing;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.Utils.Colors;
using DevExpress.XtraEditors;
using intelliconShipping.Common;
using intelliconShipping.Views.Settings;

namespace intelliconShipping.Views
{
    public partial class SettingsForm : XtraForm, IForm, IFormContainer
    {
        private SettingsPresenter _presenter;

        public SettingsForm()
        {
            InitializeComponent();
        }

        public void AddForm(Control form)
        {
            form.Dock = DockStyle.Fill;
            pnlSettings.Controls.Add(form);
            form.BringToFront();
        }

        public void Show(IPresenter presenter)
        {
            presenter.GetForm().BringToFront();
        }

        public void Init(IPresenter presenter)
        {
            _presenter = (SettingsPresenter)presenter;

            _presenter.BindCommand(biGeneral, p => p.ShowSettings<GeneralSettingsPresenter>());
            _presenter.BindCommand(biLicense, p => p.ShowSettings<LicensePresenter>());
            _presenter.BindCommand(biDatabase, p => p.ShowSettings<DatabaseSettingsPresenter>());
            _presenter.BindCommand(biGeneralShipping, p => p.ShowSettings<GeneralShippingPresenter>());

            var controlBackgroundColor = SkinHelper.GetControlColor(LookAndFeel);

            _presenter.ShowSettings<GeneralSettingsPresenter>();
            navBarControl1.ActiveGroup = navBarGroupGeneral;
            lblHeader.Text = biGeneral.Caption;

            // ApplyLabelStyle(lblHeader, LookAndFeel);
        }

        public static void ApplyLabelStyle(LabelControl label, UserLookAndFeel lookAndFeel)
        {
            var controlBackgroundColor = SkinHelper.GetControlColor(lookAndFeel);
            var col = DXSkinColorHelper.GetDXSkinColor(SystemColors.ActiveBorder, lookAndFeel);
            label.BackColor = controlBackgroundColor;
            label.Appearance.BackColor = controlBackgroundColor;
            label.Appearance.BorderColor = Color.Transparent;
            label.Appearance.Options.UseBackColor = true;
            label.Appearance.Options.UseBorderColor = true;

            label.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
        }

        private void navBarControl1_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            lblHeader.Text = e.Link.Caption;
        }

        private void navBarControl1_ActiveGroupChanged(object sender, DevExpress.XtraNavBar.NavBarGroupEventArgs e)
        {
            if (e.Group == navBarGroupGeneral)
            {
                _presenter.ShowSettings<GeneralSettingsPresenter>();
                lblHeader.Text = biGeneral.Caption;
            }
            else if (e.Group == navBarGroupShipping)
            {
                _presenter.ShowSettings<GeneralShippingPresenter>();
                lblHeader.Text = biGeneralShipping.Caption;
            }
        }
    }
}
