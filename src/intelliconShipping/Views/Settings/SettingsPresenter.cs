﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using intellicon.Utilities.Services;
using intelliconShipping.Core.Services;
using intelliconShipping.Services;

namespace intelliconShipping.Views.Settings
{
    public class SettingsPresenter : Presenter
    {
        private readonly IPresenterFactory _presenterFactory;
        private readonly IGlobalSettings _globalSettings;
        private Dictionary<Type, ISettingsPresenter> _presenters = new Dictionary<Type, ISettingsPresenter>();
        private ISettingsPresenter _currentPresenter;

        public SettingsPresenter(IErrorHelper errorHelper, IPresenterFactory presenterFactory, IGlobalSettings globalSettings)
            : base(errorHelper)
        {
            _presenterFactory = presenterFactory ?? throw new ArgumentNullException(nameof(presenterFactory));
            _globalSettings = globalSettings ?? throw new ArgumentNullException(nameof(globalSettings));
        }

        public string ModuleTitle { get; set; }

        protected override void OnInit()
        {

        }

        protected override Control CreateForm() => new SettingsForm();

        public void ShowSettings<TPresenter>() where TPresenter : Presenter
        {
            if (_currentPresenter != null)
                _currentPresenter.SaveSettings();

            if (_presenters.TryGetValue(typeof(TPresenter), out var presenter))
            {
                _currentPresenter = (ISettingsPresenter)presenter;
                ((IFormContainer)GetForm()).Show((IPresenter)presenter);
            }
            else
            {
                _currentPresenter = (ISettingsPresenter)_presenterFactory.Show<TPresenter>(this);
                _presenters.Add(typeof(TPresenter), _currentPresenter);
            }
        }


        public override void Closing()
        {
            if (_currentPresenter != null)
                _currentPresenter.SaveSettings();

            _globalSettings.Reload();
        }
    }
}
