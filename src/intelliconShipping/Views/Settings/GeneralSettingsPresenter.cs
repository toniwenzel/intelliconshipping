﻿using System.Windows.Forms;
using intellicon.Utilities.Services;

namespace intelliconShipping.Views.Settings
{
    public class GeneralSettingsPresenter : Presenter, ISettingsPresenter
    {
        public GeneralSettingsPresenter(IErrorHelper errorHelper) : base(errorHelper)
        {
        }

        public string SkinName { get; internal set; }
        public string SkinPaletteName { get; internal set; }

        protected override Control CreateForm() => new GeneralSettingsControl();

        protected override void OnInit()
        {
            SkinName = UserSettings.Default.SkinName;
            SkinPaletteName = UserSettings.Default.SkinPaletteName;
        }

        public void SaveSettings()
        {
            UserSettings.Default.SkinName = SkinName;
            UserSettings.Default.SkinPaletteName = SkinPaletteName;
            UserSettings.Default.Save();
        }
    }
}
