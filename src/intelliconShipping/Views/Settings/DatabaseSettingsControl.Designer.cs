﻿namespace intelliconShipping.Views.Settings
{
    partial class DatabaseSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DatabaseSettingsControl));
            this.btnLogin = new DevExpress.XtraEditors.SimpleButton();
            this.imgBoxCheck = new DevExpress.XtraEditors.SvgImageBox();
            this.lblPassword = new DevExpress.XtraEditors.LabelControl();
            this.lblUsername = new DevExpress.XtraEditors.LabelControl();
            this.txtPassword = new DevExpress.XtraEditors.TextEdit();
            this.txtUsername = new DevExpress.XtraEditors.TextEdit();
            this.cboAuthentication = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblAuthentication = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lblServer = new DevExpress.XtraEditors.LabelControl();
            this.txtDatabaseName = new DevExpress.XtraEditors.TextEdit();
            this.txtServer = new DevExpress.XtraEditors.TextEdit();
            this.lblError = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.imgBoxCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsername.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboAuthentication.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatabaseName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServer.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(5, 146);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(75, 23);
            this.btnLogin.TabIndex = 0;
            this.btnLogin.Text = "Anmelden";
            // 
            // imgBoxCheck
            // 
            this.imgBoxCheck.Location = new System.Drawing.Point(310, 3);
            this.imgBoxCheck.Name = "imgBoxCheck";
            this.imgBoxCheck.Size = new System.Drawing.Size(40, 40);
            this.imgBoxCheck.SizeMode = DevExpress.XtraEditors.SvgImageSizeMode.Zoom;
            this.imgBoxCheck.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("imgBoxCheck.SvgImage")));
            this.imgBoxCheck.TabIndex = 23;
            this.imgBoxCheck.Text = "svgImageBox1";
            // 
            // lblPassword
            // 
            this.lblPassword.Enabled = false;
            this.lblPassword.Location = new System.Drawing.Point(21, 110);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(48, 13);
            this.lblPassword.TabIndex = 22;
            this.lblPassword.Text = "Passwort:";
            // 
            // lblUsername
            // 
            this.lblUsername.Enabled = false;
            this.lblUsername.Location = new System.Drawing.Point(21, 84);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(73, 13);
            this.lblUsername.TabIndex = 21;
            this.lblUsername.Text = "Benutzername:";
            // 
            // txtPassword
            // 
            this.txtPassword.Enabled = false;
            this.txtPassword.Location = new System.Drawing.Point(117, 107);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Properties.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(173, 20);
            this.txtPassword.TabIndex = 20;
            // 
            // txtUsername
            // 
            this.txtUsername.Enabled = false;
            this.txtUsername.Location = new System.Drawing.Point(117, 81);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(173, 20);
            this.txtUsername.TabIndex = 19;
            // 
            // cboAuthentication
            // 
            this.cboAuthentication.Location = new System.Drawing.Point(101, 55);
            this.cboAuthentication.Name = "cboAuthentication";
            this.cboAuthentication.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboAuthentication.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cboAuthentication.Size = new System.Drawing.Size(189, 20);
            this.cboAuthentication.TabIndex = 18;
            // 
            // lblAuthentication
            // 
            this.lblAuthentication.Location = new System.Drawing.Point(5, 58);
            this.lblAuthentication.Name = "lblAuthentication";
            this.lblAuthentication.Size = new System.Drawing.Size(57, 13);
            this.lblAuthentication.TabIndex = 17;
            this.lblAuthentication.Text = "Anmeldung:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(5, 32);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(82, 13);
            this.labelControl1.TabIndex = 16;
            this.labelControl1.Text = "Datenbankname:";
            // 
            // lblServer
            // 
            this.lblServer.Location = new System.Drawing.Point(5, 6);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(87, 13);
            this.lblServer.TabIndex = 15;
            this.lblServer.Text = "Datenbankserver:";
            // 
            // txtDatabaseName
            // 
            this.txtDatabaseName.Location = new System.Drawing.Point(101, 29);
            this.txtDatabaseName.Name = "txtDatabaseName";
            this.txtDatabaseName.Size = new System.Drawing.Size(189, 20);
            this.txtDatabaseName.TabIndex = 14;
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(101, 3);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(189, 20);
            this.txtServer.TabIndex = 13;
            // 
            // lblError
            // 
            this.lblError.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblError.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblError.Appearance.Options.UseForeColor = true;
            this.lblError.Appearance.Options.UseTextOptions = true;
            this.lblError.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.lblError.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lblError.AutoEllipsis = true;
            this.lblError.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblError.Location = new System.Drawing.Point(5, 175);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(404, 26);
            this.lblError.TabIndex = 24;
            this.lblError.Text = "Fehlersdf\r\nsdf\r\n";
            // 
            // DatabaseSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.imgBoxCheck);
            this.Controls.Add(this.txtServer);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.txtDatabaseName);
            this.Controls.Add(this.lblServer);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.lblAuthentication);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.cboAuthentication);
            this.Controls.Add(this.txtUsername);
            this.Name = "DatabaseSettingsControl";
            this.Size = new System.Drawing.Size(430, 220);
            ((System.ComponentModel.ISupportInitialize)(this.imgBoxCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsername.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboAuthentication.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatabaseName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServer.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnLogin;
        private DevExpress.XtraEditors.SvgImageBox imgBoxCheck;
        private DevExpress.XtraEditors.LabelControl lblPassword;
        private DevExpress.XtraEditors.LabelControl lblUsername;
        private DevExpress.XtraEditors.TextEdit txtPassword;
        private DevExpress.XtraEditors.TextEdit txtUsername;
        private DevExpress.XtraEditors.ComboBoxEdit cboAuthentication;
        private DevExpress.XtraEditors.LabelControl lblAuthentication;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lblServer;
        private DevExpress.XtraEditors.TextEdit txtDatabaseName;
        private DevExpress.XtraEditors.TextEdit txtServer;
        private DevExpress.XtraEditors.LabelControl lblError;
    }
}
