﻿using DevExpress.XtraEditors;
using intellicon.Utilities.Common;
using intelliconShipping.Common;

namespace intelliconShipping.Views.Settings
{
    public partial class GeneralShippingControl : XtraUserControl, IForm
    {
        public GeneralShippingControl()
        {
            InitializeComponent();
        }

        public void Init(IPresenter presenter)
        {
            var shippingPresenter = presenter as GeneralShippingPresenter;

            shippingPresenter.SetBinding(txtFixedWeightDomestic, x => x.EditValue, p => p.FixedWeightDomestic);
            shippingPresenter.SetBinding(chkUserFieldsInactive, x => x.Checked, p => p.UserFieldsInactive);
            shippingPresenter.SetBinding(chkContactFromAsp, x => x.Checked, p => p.UseContactDataFromAsp);
            shippingPresenter.SetBinding(txtDeliveryEmailAddress, x => x.Text, p => p.DeliveryEmailAddress);
            shippingPresenter.SetBinding(txtStatusEmailAddress, x => x.Text, p => p.StatusEmailAddress);
        }

        private void txtStatusEmailAddress_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!MiscUtils.IsValidEmail(txtStatusEmailAddress.Text))
            {
                txtStatusEmailAddress.ErrorText = "Ungültige Email";
                e.Cancel = true;
            }
        }

        private void txtDeliveryEmailAddress_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!MiscUtils.IsValidEmail(txtDeliveryEmailAddress.Text))
            {
                txtDeliveryEmailAddress.ErrorText = "Ungültige Email";
                e.Cancel = true;
            }
        }
    }
}
