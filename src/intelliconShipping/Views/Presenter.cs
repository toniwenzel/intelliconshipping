﻿using System;
using System.Windows.Forms;
using intellicon.Utilities.Services;

namespace intelliconShipping.Views
{
    public abstract class Presenter : IPresenter
    {
        private Control _form;
        private readonly IErrorHelper _errorHelper;

        public virtual bool IsBusy { get; set; }
        public bool WasInitialized { get; private set; }

        protected Presenter(IErrorHelper errorHelper)
        {
            _errorHelper = errorHelper ?? throw new System.ArgumentNullException(nameof(errorHelper));
        }

        public Control GetForm()
        {
            return _form ??= CreateForm();
        }

        protected abstract Control CreateForm();

        public void Init()
        {
            IsBusy = true;
            try
            {
                OnInit();
                WasInitialized = true;
            }
            catch (Exception e)
            {
                HandleException(e);
            }
            finally
            {
                IsBusy = false;
            }
        }

        protected abstract void OnInit();

        protected void HandleException(Exception exception)
        {
            _errorHelper.HandleException(exception);
        }

        public virtual void Closing()
        {
        }
    }
}
