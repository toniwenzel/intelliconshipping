﻿namespace intelliconShipping.Views
{
    partial class ShippingControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblReceiptNumber = new DevExpress.XtraEditors.LabelControl();
            this.lblShippingType = new DevExpress.XtraEditors.LabelControl();
            this.cboShippingType = new DevExpress.XtraEditors.LookUpEdit();
            this.txtReceipt = new DevExpress.XtraEditors.TextEdit();
            this.txtReceiptNumber = new DevExpress.XtraEditors.TextEdit();
            this.lblDeliveryConditions = new DevExpress.XtraEditors.LabelControl();
            this.lblReference = new DevExpress.XtraEditors.LabelControl();
            this.txtDeliveryAddress = new DevExpress.XtraEditors.MemoEdit();
            this.txtDeliveryConditions = new DevExpress.XtraEditors.TextEdit();
            this.txtReference = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lblWeight = new DevExpress.XtraEditors.LabelControl();
            this.gridPackages = new DevExpress.XtraGrid.GridControl();
            this.viewPackages = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDecimalText = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPrice = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.lblError = new DevExpress.XtraEditors.LabelControl();
            this.lblDebtAmount = new DevExpress.XtraEditors.LabelControl();
            this.txtDebtAmount = new DevExpress.XtraEditors.TextEdit();
            this.pnlDetails = new DevExpress.XtraEditors.PanelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnPackages1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPackages2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPackages3 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPackages4 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPackages5 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPackages6 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPackages7 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPackages8 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPackages9 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPackages10 = new DevExpress.XtraEditors.SimpleButton();
            this.btnOutput = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.cboShippingType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceipt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceiptNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryConditions.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPackages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewPackages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDecimalText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDebtAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDetails)).BeginInit();
            this.pnlDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblReceiptNumber
            // 
            this.lblReceiptNumber.Location = new System.Drawing.Point(3, 3);
            this.lblReceiptNumber.Name = "lblReceiptNumber";
            this.lblReceiptNumber.Size = new System.Drawing.Size(48, 13);
            this.lblReceiptNumber.TabIndex = 1;
            this.lblReceiptNumber.Text = "Beleg Nr.:";
            // 
            // lblShippingType
            // 
            this.lblShippingType.Location = new System.Drawing.Point(3, 131);
            this.lblShippingType.Name = "lblShippingType";
            this.lblShippingType.Size = new System.Drawing.Size(57, 13);
            this.lblShippingType.TabIndex = 2;
            this.lblShippingType.Text = "Versandart:";
            // 
            // cboShippingType
            // 
            this.cboShippingType.Location = new System.Drawing.Point(102, 131);
            this.cboShippingType.Name = "cboShippingType";
            this.cboShippingType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboShippingType.Properties.ShowFooter = false;
            this.cboShippingType.Properties.ShowHeader = false;
            this.cboShippingType.Size = new System.Drawing.Size(246, 20);
            this.cboShippingType.TabIndex = 4;
            // 
            // txtReceipt
            // 
            this.txtReceipt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReceipt.Location = new System.Drawing.Point(318, 17);
            this.txtReceipt.Name = "txtReceipt";
            this.txtReceipt.Properties.AdvancedModeOptions.AutoCompleteMode = DevExpress.XtraEditors.TextEditAutoCompleteMode.Suggest;
            this.txtReceipt.Properties.AdvancedModeOptions.Label = "Belegnummer eingeben oder scannen";
            this.txtReceipt.Properties.UseAdvancedMode = DevExpress.Utils.DefaultBoolean.True;
            this.txtReceipt.Properties.ValidateOnEnterKey = true;
            this.txtReceipt.Properties.Validating += new System.ComponentModel.CancelEventHandler(this.txtReceipt_Properties_Validating);
            this.txtReceipt.Size = new System.Drawing.Size(261, 34);
            this.txtReceipt.TabIndex = 5;
            // 
            // txtReceiptNumber
            // 
            this.txtReceiptNumber.Location = new System.Drawing.Point(90, 3);
            this.txtReceiptNumber.Name = "txtReceiptNumber";
            this.txtReceiptNumber.Properties.ReadOnly = true;
            this.txtReceiptNumber.Size = new System.Drawing.Size(258, 20);
            this.txtReceiptNumber.TabIndex = 6;
            // 
            // lblDeliveryConditions
            // 
            this.lblDeliveryConditions.Location = new System.Drawing.Point(3, 157);
            this.lblDeliveryConditions.Name = "lblDeliveryConditions";
            this.lblDeliveryConditions.Size = new System.Drawing.Size(81, 13);
            this.lblDeliveryConditions.TabIndex = 7;
            this.lblDeliveryConditions.Text = "Lieferbedingung:";
            // 
            // lblReference
            // 
            this.lblReference.Location = new System.Drawing.Point(3, 187);
            this.lblReference.Name = "lblReference";
            this.lblReference.Size = new System.Drawing.Size(48, 13);
            this.lblReference.TabIndex = 8;
            this.lblReference.Text = "Referenz:";
            // 
            // txtDeliveryAddress
            // 
            this.txtDeliveryAddress.Location = new System.Drawing.Point(3, 29);
            this.txtDeliveryAddress.Name = "txtDeliveryAddress";
            this.txtDeliveryAddress.Size = new System.Drawing.Size(345, 96);
            this.txtDeliveryAddress.TabIndex = 9;
            // 
            // txtDeliveryConditions
            // 
            this.txtDeliveryConditions.Location = new System.Drawing.Point(102, 157);
            this.txtDeliveryConditions.Name = "txtDeliveryConditions";
            this.txtDeliveryConditions.Properties.ReadOnly = true;
            this.txtDeliveryConditions.Size = new System.Drawing.Size(246, 20);
            this.txtDeliveryConditions.TabIndex = 10;
            // 
            // txtReference
            // 
            this.txtReference.Location = new System.Drawing.Point(102, 183);
            this.txtReference.Name = "txtReference";
            this.txtReference.Properties.ReadOnly = true;
            this.txtReference.Size = new System.Drawing.Size(246, 20);
            this.txtReference.TabIndex = 11;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(378, 3);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(100, 13);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "Gewicht laut Waage:";
            // 
            // lblWeight
            // 
            this.lblWeight.Location = new System.Drawing.Point(484, 3);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(63, 13);
            this.lblWeight.TabIndex = 13;
            this.lblWeight.Text = "labelControl3";
            // 
            // gridPackages
            // 
            this.gridPackages.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gridPackages.Location = new System.Drawing.Point(5, 61);
            this.gridPackages.MainView = this.viewPackages;
            this.gridPackages.Name = "gridPackages";
            this.gridPackages.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDecimalText,
            this.repositoryItemPrice});
            this.gridPackages.Size = new System.Drawing.Size(375, 261);
            this.gridPackages.TabIndex = 14;
            this.gridPackages.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewPackages});
            // 
            // viewPackages
            // 
            this.viewPackages.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWeight,
            this.colLength,
            this.colWidth,
            this.colHeight,
            this.colPrice});
            this.viewPackages.GridControl = this.gridPackages;
            this.viewPackages.Name = "viewPackages";
            this.viewPackages.OptionsView.ShowGroupPanel = false;
            this.viewPackages.OptionsView.ShowIndicator = false;
            // 
            // colWeight
            // 
            this.colWeight.Caption = "Gewicht";
            this.colWeight.ColumnEdit = this.repositoryItemDecimalText;
            this.colWeight.FieldName = "Weight";
            this.colWeight.Name = "colWeight";
            this.colWeight.Visible = true;
            this.colWeight.VisibleIndex = 0;
            this.colWeight.Width = 53;
            // 
            // repositoryItemDecimalText
            // 
            this.repositoryItemDecimalText.AutoHeight = false;
            this.repositoryItemDecimalText.MaskSettings.Set("MaskManagerType", typeof(DevExpress.Data.Mask.NumericMaskManager));
            this.repositoryItemDecimalText.MaskSettings.Set("mask", "f3");
            this.repositoryItemDecimalText.Name = "repositoryItemDecimalText";
            // 
            // colLength
            // 
            this.colLength.Caption = "Länge";
            this.colLength.ColumnEdit = this.repositoryItemDecimalText;
            this.colLength.FieldName = "Length";
            this.colLength.Name = "colLength";
            this.colLength.Visible = true;
            this.colLength.VisibleIndex = 1;
            this.colLength.Width = 53;
            // 
            // colWidth
            // 
            this.colWidth.Caption = "Breite";
            this.colWidth.ColumnEdit = this.repositoryItemDecimalText;
            this.colWidth.FieldName = "Width";
            this.colWidth.Name = "colWidth";
            this.colWidth.Visible = true;
            this.colWidth.VisibleIndex = 2;
            this.colWidth.Width = 53;
            // 
            // colHeight
            // 
            this.colHeight.Caption = "Höhe";
            this.colHeight.ColumnEdit = this.repositoryItemDecimalText;
            this.colHeight.FieldName = "Height";
            this.colHeight.Name = "colHeight";
            this.colHeight.Visible = true;
            this.colHeight.VisibleIndex = 3;
            this.colHeight.Width = 53;
            // 
            // colPrice
            // 
            this.colPrice.Caption = "NN-Betrag";
            this.colPrice.ColumnEdit = this.repositoryItemPrice;
            this.colPrice.FieldName = "Price";
            this.colPrice.Name = "colPrice";
            this.colPrice.Visible = true;
            this.colPrice.VisibleIndex = 4;
            this.colPrice.Width = 56;
            // 
            // repositoryItemPrice
            // 
            this.repositoryItemPrice.AutoHeight = false;
            this.repositoryItemPrice.MaskSettings.Set("MaskManagerType", typeof(DevExpress.Data.Mask.NumericMaskManager));
            this.repositoryItemPrice.MaskSettings.Set("mask", "c");
            this.repositoryItemPrice.Name = "repositoryItemPrice";
            // 
            // lblError
            // 
            this.lblError.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblError.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblError.Appearance.Options.UseFont = true;
            this.lblError.Appearance.Options.UseTextOptions = true;
            this.lblError.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblError.AutoEllipsis = true;
            this.lblError.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblError.Location = new System.Drawing.Point(3, 55);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(893, 27);
            this.lblError.TabIndex = 15;
            this.lblError.Text = "Fehler";
            // 
            // lblDebtAmount
            // 
            this.lblDebtAmount.Location = new System.Drawing.Point(3, 212);
            this.lblDebtAmount.Name = "lblDebtAmount";
            this.lblDebtAmount.Size = new System.Drawing.Size(93, 13);
            this.lblDebtAmount.TabIndex = 16;
            this.lblDebtAmount.Text = "Nachname-Gebühr:";
            // 
            // txtDebtAmount
            // 
            this.txtDebtAmount.Location = new System.Drawing.Point(102, 209);
            this.txtDebtAmount.Name = "txtDebtAmount";
            this.txtDebtAmount.Properties.MaskSettings.Set("MaskManagerType", typeof(DevExpress.Data.Mask.NumericMaskManager));
            this.txtDebtAmount.Properties.MaskSettings.Set("mask", "c");
            this.txtDebtAmount.Size = new System.Drawing.Size(246, 20);
            this.txtDebtAmount.TabIndex = 17;
            // 
            // pnlDetails
            // 
            this.pnlDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlDetails.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlDetails.Controls.Add(this.btnOutput);
            this.pnlDetails.Controls.Add(this.groupControl1);
            this.pnlDetails.Controls.Add(this.lblReceiptNumber);
            this.pnlDetails.Controls.Add(this.txtDebtAmount);
            this.pnlDetails.Controls.Add(this.lblShippingType);
            this.pnlDetails.Controls.Add(this.lblDebtAmount);
            this.pnlDetails.Controls.Add(this.cboShippingType);
            this.pnlDetails.Controls.Add(this.txtReceiptNumber);
            this.pnlDetails.Controls.Add(this.lblDeliveryConditions);
            this.pnlDetails.Controls.Add(this.lblWeight);
            this.pnlDetails.Controls.Add(this.lblReference);
            this.pnlDetails.Controls.Add(this.labelControl2);
            this.pnlDetails.Controls.Add(this.txtDeliveryAddress);
            this.pnlDetails.Controls.Add(this.txtReference);
            this.pnlDetails.Controls.Add(this.txtDeliveryConditions);
            this.pnlDetails.Location = new System.Drawing.Point(3, 88);
            this.pnlDetails.Name = "pnlDetails";
            this.pnlDetails.Size = new System.Drawing.Size(893, 441);
            this.pnlDetails.TabIndex = 18;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gridPackages);
            this.groupControl1.Controls.Add(this.btnPackages1);
            this.groupControl1.Controls.Add(this.btnPackages2);
            this.groupControl1.Controls.Add(this.btnPackages3);
            this.groupControl1.Controls.Add(this.btnPackages4);
            this.groupControl1.Controls.Add(this.btnPackages5);
            this.groupControl1.Controls.Add(this.btnPackages6);
            this.groupControl1.Controls.Add(this.btnPackages7);
            this.groupControl1.Controls.Add(this.btnPackages8);
            this.groupControl1.Controls.Add(this.btnPackages9);
            this.groupControl1.Controls.Add(this.btnPackages10);
            this.groupControl1.Location = new System.Drawing.Point(378, 29);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(431, 327);
            this.groupControl1.TabIndex = 18;
            this.groupControl1.Text = "Pakete";
            // 
            // btnPackages1
            // 
            this.btnPackages1.Location = new System.Drawing.Point(5, 32);
            this.btnPackages1.Name = "btnPackages1";
            this.btnPackages1.Size = new System.Drawing.Size(25, 23);
            this.btnPackages1.TabIndex = 0;
            this.btnPackages1.Text = "1";
            this.btnPackages1.Click += new System.EventHandler(this.btnPackages_Click);
            // 
            // btnPackages2
            // 
            this.btnPackages2.Location = new System.Drawing.Point(36, 32);
            this.btnPackages2.Name = "btnPackages2";
            this.btnPackages2.Size = new System.Drawing.Size(25, 23);
            this.btnPackages2.TabIndex = 0;
            this.btnPackages2.Text = "2";
            this.btnPackages2.Click += new System.EventHandler(this.btnPackages_Click);
            // 
            // btnPackages3
            // 
            this.btnPackages3.Location = new System.Drawing.Point(67, 32);
            this.btnPackages3.Name = "btnPackages3";
            this.btnPackages3.Size = new System.Drawing.Size(25, 23);
            this.btnPackages3.TabIndex = 0;
            this.btnPackages3.Text = "3";
            this.btnPackages3.Click += new System.EventHandler(this.btnPackages_Click);
            // 
            // btnPackages4
            // 
            this.btnPackages4.Location = new System.Drawing.Point(98, 32);
            this.btnPackages4.Name = "btnPackages4";
            this.btnPackages4.Size = new System.Drawing.Size(25, 23);
            this.btnPackages4.TabIndex = 0;
            this.btnPackages4.Text = "4";
            this.btnPackages4.Click += new System.EventHandler(this.btnPackages_Click);
            // 
            // btnPackages5
            // 
            this.btnPackages5.Location = new System.Drawing.Point(129, 32);
            this.btnPackages5.Name = "btnPackages5";
            this.btnPackages5.Size = new System.Drawing.Size(25, 23);
            this.btnPackages5.TabIndex = 0;
            this.btnPackages5.Text = "5";
            this.btnPackages5.Click += new System.EventHandler(this.btnPackages_Click);
            // 
            // btnPackages6
            // 
            this.btnPackages6.Location = new System.Drawing.Point(160, 32);
            this.btnPackages6.Name = "btnPackages6";
            this.btnPackages6.Size = new System.Drawing.Size(25, 23);
            this.btnPackages6.TabIndex = 0;
            this.btnPackages6.Text = "6";
            this.btnPackages6.Click += new System.EventHandler(this.btnPackages_Click);
            // 
            // btnPackages7
            // 
            this.btnPackages7.Location = new System.Drawing.Point(191, 32);
            this.btnPackages7.Name = "btnPackages7";
            this.btnPackages7.Size = new System.Drawing.Size(25, 23);
            this.btnPackages7.TabIndex = 0;
            this.btnPackages7.Text = "7";
            this.btnPackages7.Click += new System.EventHandler(this.btnPackages_Click);
            // 
            // btnPackages8
            // 
            this.btnPackages8.Location = new System.Drawing.Point(222, 32);
            this.btnPackages8.Name = "btnPackages8";
            this.btnPackages8.Size = new System.Drawing.Size(25, 23);
            this.btnPackages8.TabIndex = 0;
            this.btnPackages8.Text = "8";
            this.btnPackages8.Click += new System.EventHandler(this.btnPackages_Click);
            // 
            // btnPackages9
            // 
            this.btnPackages9.Location = new System.Drawing.Point(253, 32);
            this.btnPackages9.Name = "btnPackages9";
            this.btnPackages9.Size = new System.Drawing.Size(25, 23);
            this.btnPackages9.TabIndex = 0;
            this.btnPackages9.Text = "9";
            this.btnPackages9.Click += new System.EventHandler(this.btnPackages_Click);
            // 
            // btnPackages10
            // 
            this.btnPackages10.Location = new System.Drawing.Point(284, 32);
            this.btnPackages10.Name = "btnPackages10";
            this.btnPackages10.Size = new System.Drawing.Size(25, 23);
            this.btnPackages10.TabIndex = 0;
            this.btnPackages10.Text = "10";
            this.btnPackages10.Click += new System.EventHandler(this.btnPackages_Click);
            // 
            // btnOutput
            // 
            this.btnOutput.Location = new System.Drawing.Point(364, 373);
            this.btnOutput.Name = "btnOutput";
            this.btnOutput.Size = new System.Drawing.Size(75, 23);
            this.btnOutput.TabIndex = 19;
            this.btnOutput.Text = "Versenden";
            this.btnOutput.Click += new System.EventHandler(this.btnOutput_Click);
            // 
            // ShippingControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlDetails);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.txtReceipt);
            this.Name = "ShippingControl";
            this.Size = new System.Drawing.Size(899, 532);
            ((System.ComponentModel.ISupportInitialize)(this.cboShippingType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceipt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceiptNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryConditions.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPackages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewPackages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDecimalText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDebtAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDetails)).EndInit();
            this.pnlDetails.ResumeLayout(false);
            this.pnlDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblReceiptNumber;
        private DevExpress.XtraEditors.LabelControl lblShippingType;
        private DevExpress.XtraEditors.LookUpEdit cboShippingType;
        private DevExpress.XtraEditors.TextEdit txtReceipt;
        private DevExpress.XtraEditors.TextEdit txtReceiptNumber;
        private DevExpress.XtraEditors.LabelControl lblDeliveryConditions;
        private DevExpress.XtraEditors.LabelControl lblReference;
        private DevExpress.XtraEditors.MemoEdit txtDeliveryAddress;
        private DevExpress.XtraEditors.TextEdit txtDeliveryConditions;
        private DevExpress.XtraEditors.TextEdit txtReference;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl lblWeight;
        private DevExpress.XtraGrid.GridControl gridPackages;
        private DevExpress.XtraGrid.Views.Grid.GridView viewPackages;
        private DevExpress.XtraGrid.Columns.GridColumn colWeight;
        private DevExpress.XtraGrid.Columns.GridColumn colLength;
        private DevExpress.XtraGrid.Columns.GridColumn colWidth;
        private DevExpress.XtraGrid.Columns.GridColumn colHeight;
        private DevExpress.XtraGrid.Columns.GridColumn colPrice;
        private DevExpress.XtraEditors.LabelControl lblError;
        private DevExpress.XtraEditors.LabelControl lblDebtAmount;
        private DevExpress.XtraEditors.TextEdit txtDebtAmount;
        private DevExpress.XtraEditors.PanelControl pnlDetails;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemDecimalText;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemPrice;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnPackages1;
        private DevExpress.XtraEditors.SimpleButton btnPackages2;
        private DevExpress.XtraEditors.SimpleButton btnPackages3;
        private DevExpress.XtraEditors.SimpleButton btnPackages4;
        private DevExpress.XtraEditors.SimpleButton btnPackages5;
        private DevExpress.XtraEditors.SimpleButton btnPackages6;
        private DevExpress.XtraEditors.SimpleButton btnPackages7;
        private DevExpress.XtraEditors.SimpleButton btnPackages8;
        private DevExpress.XtraEditors.SimpleButton btnPackages9;
        private DevExpress.XtraEditors.SimpleButton btnPackages10;
        private DevExpress.XtraEditors.SimpleButton btnOutput;
    }
}
