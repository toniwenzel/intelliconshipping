﻿using DevExpress.XtraEditors;
using intelliconShipping.Common;

namespace intelliconShipping.Views
{
    public partial class AboutForm : XtraForm, IForm
    {
        public AboutForm()
        {
            InitializeComponent();
        }

        public void Init(IPresenter presenter)
        {
            var mainPresenter = presenter as AboutPresenter;

            mainPresenter.SetBinding(lblVersion, x => x.Text, x => x.Version);
            mainPresenter.SetBinding(txtErrorLog, x => x.Text, x => x.ErrorLogPath);
        }
    }
}
