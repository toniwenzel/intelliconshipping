﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using intellicon.Utilities.Services;
using intelliconShipping.Services;
using intelliconShipping.Views.Settings;

namespace intelliconShipping.Views.Login
{
    public partial class LoginPresenter : DatabaseSettingsPresenter
    {
        private CancellationTokenSource _cancellationTokenSource;

        [intelliconShipping.AutoNotify]
        public bool _hasError;

        [intelliconShipping.AutoNotify]
        public DialogResult _dialogResult = DialogResult.None;


        public LoginPresenter(IErrorHelper errorHelper, IDbConnectionInfoService dbConnectionInfoService)
            : base(errorHelper, dbConnectionInfoService)
        {


        }

        protected override Control CreateForm() => new LoginForm();


        public override async Task CheckLogin()
        {
            IsBusy = true;
            _cancellationTokenSource = new CancellationTokenSource();
            _cancellationTokenSource.CancelAfter(TimeSpan.FromMinutes(1));

            try
            {
                (IsLoginValid, LoginError) = await _dbConnectionInfoService.IsValidLoginAsync(GetLogin(), _cancellationTokenSource.Token);
            }
            catch (OperationCanceledException)
            {
                LoginError = "";
            }
            catch (Exception e)
            {
                LoginError = $"Vorgang fehlgeschlagen: {e.Message}";
                ExceptionManager.TraceException(e);
            }

            _cancellationTokenSource.Dispose();
            _cancellationTokenSource = null;

            HasError = !string.IsNullOrEmpty(LoginError);
            if (IsLoginValid)
            {
                DialogResult = DialogResult.OK;
                var login = GetLogin();
                SaveLastUsedSettings(login);

                _dbConnectionInfoService.UseLogin(login);
            }
            IsBusy = false;
        }

        public void CancelLogin()
        {
            if (_cancellationTokenSource != null)
                _cancellationTokenSource.Cancel();
            else
                DialogResult = DialogResult.Cancel;
        }

        private void SaveLastUsedSettings(intellicon.Utilities.DataAccess.Login login)
        {
            UserSettings.Default.Username = login.Username;
            UserSettings.Default.AuthenticationType = login.AuthenticationType;
            UserSettings.Default.DatabaseName = login.DatabaseName;
            UserSettings.Default.DatabaseServer = login.DatabaseServer;
            UserSettings.Default.Password = login.Password;

            UserSettings.Default.Save();
        }
    }
}
