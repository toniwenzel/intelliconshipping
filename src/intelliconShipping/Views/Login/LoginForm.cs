﻿using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraSplashScreen;
using intelliconShipping.Common;
using intelliconShipping.Views;
using intelliconShipping.Views.Login;

namespace intelliconShipping
{
    public partial class LoginForm : XtraForm, IForm
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        public void Init(IPresenter presenter)
        {
            var loginPresenter = presenter as LoginPresenter;
            loginPresenter.SetBinding(txtServer, x => x.Text, m => m.DatabaseServer);
            loginPresenter.SetBinding(txtDatabaseName, x => x.Text, m => m.DatabaseName);
            loginPresenter.SetBinding(cboAuthentication, x => x.EditValue, m => m.AuthenticationType, DataSourceUpdateMode.OnPropertyChanged);
            loginPresenter.SetBinding(txtPassword, x => x.Text, m => m.Password);
            loginPresenter.SetBinding(txtUsername, x => x.Text, m => m.Username);
            loginPresenter.SetBinding(lblError, x => x.Text, p => p.LoginError);

            loginPresenter.BindCommand(btnCancel, m => m.CancelLogin());
            loginPresenter.BindCommand(btnLogin, m => m.CheckLogin());

            loginPresenter.SetTrigger(x => x.HasError, UpdateHeight);
            loginPresenter.SetTrigger(x => x.DialogResult, result => this.DialogResult = result);

            cboAuthentication.Properties.Items.AddRange(loginPresenter.GetAuthenticationTypes());

            UpdateReadOnlyControls(loginPresenter.CanEditUserName);
            UpdateHeight(loginPresenter.HasError);

            IOverlaySplashScreenHandle handle = null;
            loginPresenter.SetTrigger(x => x.IsBusy, isBusy =>
            {
                if (isBusy)
                    handle = SplashScreenManager.ShowOverlayForm(pnlControls);
                else if (handle != null)
                {
                    SplashScreenManager.CloseOverlayForm(handle);
                    handle = null;
                }
            });
        }

        private void UpdateReadOnlyControls(bool canEditUserName)
        {
            txtPassword.Enabled = txtUsername.Enabled = canEditUserName;
            lblPassword.Enabled = lblUsername.Enabled = canEditUserName;
        }

        private void UpdateHeight(bool hasError)
        {
            if (hasError)
                Height = 252;
            else
                Height = 232;
        }
    }
}
