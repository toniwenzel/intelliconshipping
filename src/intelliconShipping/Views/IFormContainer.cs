﻿using System.Windows.Forms;

namespace intelliconShipping.Views
{
    public interface IFormContainer
    {
        void AddForm(Control form);
        void Show(IPresenter presenter);
    }
}
