﻿using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using intellicon.Utilities.Entities;
using intelliconShipping.Common;
using intelliconShipping.Views;

namespace intelliconShipping
{
    public partial class MainView : DevExpress.XtraEditors.XtraForm, IForm
    {
        protected override FormShowMode ShowMode => FormShowMode.AfterInitialization;
        private MainPresenter _presenter;

        public MainView()
        {
            InitializeComponent();
        }

        public void Init(IPresenter presenter)
        {
            _presenter = presenter as MainPresenter;

            _presenter.BindCommand(biClose, p => p.Exit());
            _presenter.BindCommand(biSettings, p => p.ShowSettings());
            _presenter.BindCommand(biAbout, p => p.ShowAbout());
            _presenter.BindCommand(biOnlineHelp, p => p.ShowHelp());
            _presenter.BindCommand(biTestReceipt, p => p.TestReceipt());

            _presenter.SetBinding(biConnectionInfo, bi => bi.Caption, x => x.ConnectionInfo);
            _presenter.SetBinding(this, view => view.Text, x => x.Title);
            _presenter.SetBinding(biTestReceipt, bi => bi.Enabled, x => x.IsValid);
            _presenter.SetBinding(biCompany, item => item.Caption, x => x.CurrentMandant.Name);

            foreach (var mandant in _presenter.Mandants)
            {
                var barItem = new BarCheckItem() { Tag = mandant };
                barItem.CheckedChanged += BarItem_CheckedChanged;
                barItem.Caption = mandant.Name;
                barItem.Checked = _presenter.CurrentMandant == mandant;

                biCompany.ItemLinks.Add(barItem);
            }
        }

        private async void MainView_Load(object sender, System.EventArgs e)
        {
            await _presenter.CheckLicense();
        }

        private void BarItem_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if ((e.Item as BarCheckItem).Checked)
            {
                // uncheck all others
                foreach (BarItemLink link in biCompany.ItemLinks)
                {
                    if (link.Item != e.Item)
                        (link.Item as BarCheckItem).Checked = false;
                }

                _presenter.CurrentMandant = e.Item.Tag as Mandant;
            }
        }
    }
}
