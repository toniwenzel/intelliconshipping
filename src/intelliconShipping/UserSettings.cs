﻿using System;
using System.IO;
using intellicon.Utilities.Common;
using intellicon.Utilities.DataAccess;
using intelliconShipping.Common;
using intelliconShipping.Core.Services;

namespace intelliconShipping
{
    public class UserSettings
    {
        private static UserSettings _instance = null;

        public static UserSettings Default => GetInstance();

        public int MandantId { get; set; }
        public string SkinName { get; set; } = SkinHelper.SkinNames.Office2019Colorful;
        public string SkinPaletteName { get; set; }
        public string DatabaseServer { get; set; }
        public string DatabaseName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public AuthenticationType AuthenticationType { get; set; } = AuthenticationType.Windows;


        public static UserSettings GetInstance()
        {
            if (_instance == null)
            {
                _instance = MiscUtils.DeserializeFile<UserSettings>(GetConfigFile());
            }

            return _instance;
        }

        public void Save()
        {
            MiscUtils.SerializeFile<UserSettings>(GetConfigFile(), this);
        }

        private static string GetConfigFile()
        {
            return Path.Combine(GetDefaultAppDataPath(), "UserSettings.xml");
        }

        public static string GetDefaultAppDataPath()
        {
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "intellicon", "Shipping");
        }

        internal void ReadFromOldRegistry()
        {
            using var reader = new OldRegistryReader();
            DatabaseName = reader.ReadValue("Database")?.ToString();
            DatabaseServer = reader.ReadValue("Server")?.ToString();
            Username = reader.ReadValue("User")?.ToString();
            Password = reader.ReadValue("Password")?.ToString();
            AuthenticationType = AuthenticationType.SqlServer;
            Save();
        }
    }
}
