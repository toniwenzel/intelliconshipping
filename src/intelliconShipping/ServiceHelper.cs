﻿using System;
using System.Linq;
using DbUp.Engine.Output;
using intellicon.Utilities.DataAccess;
using intellicon.Utilities.Services;
using intelliconShipping.Core.DataAccess;
using intelliconShipping.Core.Services;
using intelliconShipping.Core.ShippingProvider;
using intelliconShipping.Services;
using intelliconShipping.Views;
using intelliconShipping.Views.Login;
using intelliconShipping.Views.Settings;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace intelliconShipping
{
    /// <summary>
    /// Helper to register all required services
    /// </summary>
    public static class ServiceHelper
    {
        /// <summary>
        /// Registers all services
        /// </summary>
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddSingleton<IDbConnectionInfoService>(new DbConnectionInfoService("icPaketversandSchemaVersions", typeof(intelliconShipping.Core.Services.FilePathProvider).Assembly));
            services.AddSingleton<IErrorHelper, ErrorHelper>();
            services.AddSingleton<IDbUpdater, DbUpdater>();
            services.AddSingleton<IFilePathProvider, FilePathProvider>();
            services.AddSingleton<IProgramVersion>(ProgramVersion.Instance);
            services.AddSingleton<IUpgradeLog, DbUpdateLogger>();
            services.AddSingleton<IPresenterFactory, PresenterFactory>();
            services.AddTransient<IRegistryReader, OldRegistryReader>();
            services.AddTransient<SettingsImporter>();
            services.AddTransient<ILicenseService, LicenseService>();
            services.AddSingleton<IMessageBoxService, MessageBoxService>();
            services.AddSingleton<ICurrentMandantProvider, CurrentMandantProvider>();
            services.AddTransient<IMandantService, MandantService>();
            services.AddTransient<IReceiptNumberScanner, ReceiptNumberScanner>();
            services.AddTransient<IConfigurationService, ConfigurationService>();
            services.AddSingleton<IGlobalSettings, GlobalSettings>();
            services.AddTransient<IShippingService, ShippingService>();

            // add view Models
            services.AddTransient<LoginPresenter>();
            services.AddTransient<MainPresenter>();
            services.AddTransient<SettingsPresenter>();
            services.AddTransient<GeneralSettingsPresenter>();
            services.AddTransient<LicensePresenter>();
            services.AddTransient<DatabaseSettingsPresenter>();
            services.AddTransient<AboutPresenter>();
            services.AddTransient<ShippingPresenter>();
            services.AddTransient<GeneralShippingPresenter>();

            // repositories
            services.AddTransient<IShippingTypesRepository, ShippingTypesRepository>();
            services.AddScoped<ISageDbContext>(sp => sp.GetRequiredService<ShippingDbContext>());
            services.AddTransient<IReceiptRepository, ReceiptRepository>();
            services.AddTransient<IAddressRepository, AddressRepository>();
            services.AddTransient<IContactPersonRepository, ContactPersonRepository>();

            RegisterShippingProvider(services);

            services.AddHttpClient();
            services.AddLogging(options => options.AddDebug());

            services.AddDbContext<ShippingDbContext>((IServiceProvider sp, DbContextOptionsBuilder optionsBuilder) =>
            {
                var infoService = sp.GetRequiredService<IDbConnectionInfoService>();
                optionsBuilder.UseSqlServer(infoService.GetConnectionString());
                optionsBuilder.UseLoggerFactory(sp.GetRequiredService<ILoggerFactory>());
            });
        }

        private static void RegisterShippingProvider(IServiceCollection services)
        {
            // register factory
            services.AddSingleton<IShippingProviderFactory, ShippingProviderFactory>();

            var interfaceType = typeof(IShippingProvider);
            var shippingProviderTypes = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes()).Where(t => interfaceType.IsAssignableFrom(t) && !t.IsInterface && !t.IsAbstract);

            foreach (var type in shippingProviderTypes)
                services.AddTransient(interfaceType, type);
        }
    }
}
