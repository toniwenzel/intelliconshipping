﻿using System;
using System.Linq.Expressions;
using System.Windows.Forms;
using DevExpress.Utils.MVVM;
using DevExpress.XtraEditors;

namespace intelliconShipping.Common
{
    /// <summary>
    /// Helper class for easier binding syntax
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    public class ModelBindingSource<TModel> : BindingSource
    {
        public ModelBindingSource()
        {
            DataSource = typeof(TModel);
        }

        /// <summary>
        /// Adds the binding to the control
        /// </summary>
        /// <typeparam name="TControl">Type of the control. Can be ommited because it will be infered.</typeparam>
        /// <typeparam name="TControlValue">Type of the control's property. Can be ommited because it will be infered.</typeparam>
        /// <typeparam name="TValue">Type of the model's property. Can be ommited because it will be infered.</typeparam>
        /// <param name="control">The control to bind to.</param>
        /// <param name="controlPropertyExpression">The expression to define the control's property.</param>
        /// <param name="modelPropertyExpression">>The expression to define the model's property.</param>
        public void SetBinding<TControl, TControlValue, TValue>(TControl control, Expression<Func<TControl, TControlValue>> controlPropertyExpression, Expression<Func<TModel, TValue>> modelPropertyExpression) where TControl : Control
        {
            SetBinding(control, controlPropertyExpression, modelPropertyExpression, DataSourceUpdateMode.OnValidation);
        }

        /// <summary>
        /// Adds the binding to the control
        /// </summary>
        /// <typeparam name="TControl">Type of the control. Can be ommited because it will be infered.</typeparam>
        /// <typeparam name="TControlValue">Type of the control's property. Can be ommited because it will be infered.</typeparam>
        /// <typeparam name="TValue">Type of the model's property. Can be ommited because it will be infered.</typeparam>
        /// <param name="control">The control to bind to.</param>
        /// <param name="controlPropertyExpression">The expression to define the control's property.</param>
        /// <param name="modelPropertyExpression">>The expression to define the model's property.</param>
        /// <param name="dataSourceUpdateMode">One of the System.Windows.Forms.DataSourceUpdateMode values.</param>
        public void SetBinding<TControl, TControlValue, TValue>(TControl control, Expression<Func<TControl, TControlValue>> controlPropertyExpression, Expression<Func<TModel, TValue>> modelPropertyExpression, DataSourceUpdateMode dataSourceUpdateMode) where TControl : Control
        {
            var propertyName = ExpressionHelper.GetPath(controlPropertyExpression);
            var modelPropertyName = ExpressionHelper.GetPath(modelPropertyExpression);
            control.DataBindings.Add(new Binding(propertyName, this, modelPropertyName, true, dataSourceUpdateMode));
        }

        /// <summary>
        /// Adds the binding to the control's EditValue property
        /// </summary>
        /// <typeparam name="TControl">Type of the control. Can be ommited because it will be infered.</typeparam>
        /// <typeparam name="TValue">Type of the model's property. Can be ommited because it will be infered.</typeparam>
        /// <param name="control">The control to bind to.</param>
        /// <param name="modelPropertyExpression">>The expression to define the model's property.</param>
        public void SetBinding<TControl, TValue>(TControl control, Expression<Func<TModel, TValue>> modelPropertyExpression) where TControl : BaseEdit
        {
            SetBinding(control, modelPropertyExpression, DataSourceUpdateMode.OnValidation);
        }

        /// <summary>
        /// Adds the binding to the control's EditValue property
        /// </summary>
        /// <typeparam name="TControl">Type of the control. Can be ommited because it will be infered.</typeparam>
        /// <typeparam name="TValue">Type of the model's property. Can be ommited because it will be infered.</typeparam>
        /// <param name="control">The control to bind to.</param>
        /// <param name="modelPropertyExpression">>The expression to define the model's property.</param>
        /// <param name="dataSourceUpdateMode">One of the System.Windows.Forms.DataSourceUpdateMode values.</param>
        public void SetBinding<TControl, TValue>(TControl control, Expression<Func<TModel, TValue>> modelPropertyExpression, DataSourceUpdateMode dataSourceUpdateMode) where TControl : BaseEdit
        {
            var modelPropertyName = ExpressionHelper.GetPath(modelPropertyExpression);
            control.DataBindings.Add(new Binding(nameof(BaseEdit.EditValue), this, modelPropertyName, true, dataSourceUpdateMode));
        }
    }
}
