﻿using DevExpress.LookAndFeel;
using DevExpress.Skins;
using System.Drawing;

namespace intelliconShipping.Common
{
    public static class SkinHelper
    {
        public static class SkinNames
        {
            public const string Basic = "Basic";
            public const string TheBezier = "The Bezier";
            public const string DevExpressStyle = "DevExpress Style";
            public const string DevExpressDarkStyle = "DevExpress Dark Style";

            public const string Office2019Colorful = "Office 2019 Colorful";
            public const string Office2019Black = "Office 2019 Black";
            public const string Office2019White = "Office 2019 White";
            public const string Office2019DarkGray = "Office 2019 Dark Gray";
        }

        public static string ActiveSkinName => UserLookAndFeel.Default.ActiveSkinName;
        public static string ActiveSvgPaletteName => UserLookAndFeel.Default.ActiveSvgPaletteName;

        /// <summary>
        /// Sets the skin of the whole application and applies the applications accent color if set
        /// </summary>
        /// <param name="skinName">The name of the skin to set</param>
        /// <param name="vectorSkinPaletteName">The palette name for DevExpress vector skins. Can be null to use the DevExpress default.</param>
        /// <remarks></remarks>
        public static void SetSkin(string skinName, string vectorSkinPaletteName)
        {
            if (IsVectorSkin(skinName) && !string.IsNullOrEmpty(vectorSkinPaletteName))
                UserLookAndFeel.Default.SetSkinStyle(skinName, vectorSkinPaletteName);

            else
                UserLookAndFeel.Default.SetSkinStyle(skinName);

        }

        /// <summary>
        /// Determines whether a skin is a vector skin
        /// which is a new kind of skin in contrast to the older, image-based skins
        /// </summary>
        /// <returns></returns>
        private static bool IsVectorSkin(string skinName)
        {
            return skinName == SkinNames.TheBezier || skinName == SkinNames.Basic ||
                   skinName.Contains("Office 2019");
        }

        public static Color GetControlColor(DevExpress.LookAndFeel.UserLookAndFeel provider)
        {
            return DevExpress.LookAndFeel.LookAndFeelHelper.GetSystemColor(provider, SystemColors.Control);
        }

        public static Color TextColor
        {
            get { return CommonSkins.GetSkin(DevExpress.LookAndFeel.UserLookAndFeel.Default).Colors.GetColor(CommonColors.ControlText); }
        }
    }
}
