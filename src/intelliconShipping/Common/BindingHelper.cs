﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraNavBar;
using intelliconShipping.Views;

namespace intelliconShipping.Common
{
    public static class BindingHelper
    {
        public static void BindCommand<TPresenter>(this TPresenter presenter, object control, Action<TPresenter> action) where TPresenter : Presenter
        {
            // find click or item click
            var events = control.GetType().GetEvents();

            var itemClick = events.FirstOrDefault(e => e.Name == "ItemClick");

            if (itemClick != null)
            {
                ItemClickEventHandler handler = new ItemClickEventHandler((s, e) =>
                {
                    action(presenter);
                });

                itemClick.AddEventHandler(control, handler);
            }

            var linkClicked = events.FirstOrDefault(e => e.Name == "LinkClicked");

            if (linkClicked != null)
            {
                NavBarLinkEventHandler handler = new NavBarLinkEventHandler((s, e) =>
                {
                    action(presenter);
                });

                linkClicked.AddEventHandler(control, handler);
            }


            var click = events.FirstOrDefault(e => e.Name == "Click");

            if (click != null)
            {
                EventHandler handler = new EventHandler((s, e) =>
                {
                    action(presenter);
                });

                click.AddEventHandler(control, handler);
            }

            //var returnType = GetDelegateReturnType(tDelegate);
            //if (returnType != typeof(void))
            //    throw new ArgumentException("Delegate has a return type.", nameof(tDelegate));

            //var handler = new DynamicMethod("", null, GetDelegateParameterTypes(tDelegate), typeof(TPresenter));

            //ILGenerator ilgen = handler.GetILGenerator();

            //Delegate d = action.Compile();

            //ilgen.EmitCall(OpCodes.Callvirt, targetMethod, null);
            //ilgen.Emit(OpCodes.Pop);
            //ilgen.Emit(OpCodes.Ret);

            //Delegate dEmitted = handler.CreateDelegate(tDelegate);
            //addHandler.Invoke(presenter, new Object[] { dEmitted });

            //Delegate d = Delegate.CreateDelegate(tDelegate, itemClick, targetMethod);
            //itemClick.AddEventHandler(presenter, dEmitted);
        }

        public static void BindCommand<TPresenter>(this TPresenter presenter, object control, Func<TPresenter, Task> action) where TPresenter : Presenter
        {
            // find click or item click
            var events = control.GetType().GetEvents();

            var itemClick = events.FirstOrDefault(e => e.Name == "ItemClick");

            if (itemClick != null)
            {
                ItemClickEventHandler handler = new ItemClickEventHandler(async (s, e) =>
                {
                    await action(presenter);
                });

                itemClick.AddEventHandler(control, handler);
            }

            var linkClicked = events.FirstOrDefault(e => e.Name == "LinkClicked");

            if (linkClicked != null)
            {
                NavBarLinkEventHandler handler = new NavBarLinkEventHandler(async (s, e) =>
                {
                    await action(presenter);
                });

                linkClicked.AddEventHandler(control, handler);
            }

            var click = events.FirstOrDefault(e => e.Name == "Click");

            if (click != null)
            {
                EventHandler handler = new EventHandler(async (s, e) =>
                {
                    await action(presenter);
                });

                click.AddEventHandler(control, handler);
            }
        }

        public static void SetBinding<TPresenter, TControl>(this TPresenter presenter, TControl control, Expression<Func<TControl, object>> controlProperty, Expression<Func<TPresenter, object>> presenterProperty, DataSourceUpdateMode dataSourceUpdateMode = DataSourceUpdateMode.OnValidation) where TControl : IBindableComponent
        {
            Expression controlExpression = controlProperty.Body;
            if (controlProperty.Body is UnaryExpression unaryExpression)
                controlExpression = unaryExpression.Operand;

            Expression presenterExpression = presenterProperty.Body;
            if (presenterProperty.Body is UnaryExpression unaryPresenterExpression)
                presenterExpression = unaryPresenterExpression.Operand;

            var presenterPropertyExpression = (MemberExpression)presenterExpression;

            var propertyName = ((MemberExpression)controlExpression).Member.Name;
            var presenterPropertyName = presenterPropertyExpression.Member.Name;
            object dataSource = presenter;

            if (presenterPropertyExpression.Expression is MemberExpression subPropertyExpression)
            {
                presenterPropertyName = $"{subPropertyExpression.Member.Name}.{presenterPropertyName}";
            }

            control.DataBindings.Add(propertyName, dataSource, presenterPropertyName, false, dataSourceUpdateMode);
        }

        public static void SetTrigger<TPresenter, TPropertyType>(this TPresenter presenter, Expression<Func<TPresenter, TPropertyType>> presenterProperty, Action<TPropertyType> onTriggerAction) where TPresenter : INotifyPropertyChanged
        {
            Expression presenterExpression = presenterProperty.Body;
            if (presenterProperty.Body is UnaryExpression unaryPresenterExpression)
                presenterExpression = unaryPresenterExpression.Operand;
            var propertyName = ((MemberExpression)presenterExpression).Member.Name;
            var propertyGetter = presenterProperty.Compile();

            presenter.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == propertyName)
                {
                    TPropertyType value = propertyGetter(presenter);
                    onTriggerAction(value);
                }
            };
        }

        private static Delegate CreateDelegate(Type d, Expression action)
        {
            if (d.BaseType != typeof(MulticastDelegate))
                throw new ArgumentException("Not a delegate.", nameof(d));

            MethodInfo invoke = d.GetMethod("Invoke");
            if (invoke == null)
                throw new ArgumentException("Not a delegate.", nameof(d));

            ParameterInfo[] parameters = invoke.GetParameters();
            List<ParameterExpression> parameterExpressions = new List<ParameterExpression>();

            for (int i = 0; i < parameters.Length; i++)
            {
                parameterExpressions.Add(Expression.Parameter(parameters[i].ParameterType, parameters[i].Name));
            }

            var lambda = Expression.Lambda(typeof(ItemClickEventHandler), action, parameterExpressions);

            var func = lambda.Compile();

            return func;
        }

        private static Type GetDelegateReturnType(Type d)
        {
            if (d.BaseType != typeof(MulticastDelegate))
                throw new ArgumentException("Not a delegate.", nameof(d));

            MethodInfo invoke = d.GetMethod("Invoke");
            if (invoke == null)
                throw new ArgumentException("Not a delegate.", nameof(d));

            return invoke.ReturnType;
        }
    }
}
