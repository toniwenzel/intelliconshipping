﻿using System.Windows.Forms;

namespace intelliconShipping.Services
{
    public static class MessageBoxServiceExtensions
    {
        public static DialogResult Show(this IMessageBoxService service, string messageBoxText, string caption)
        {
            return service.Show(messageBoxText, caption, MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        }

        public static DialogResult ShowWarning(this IMessageBoxService service, string messageBoxText, string caption)
        {
            return service.Show(messageBoxText, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
        }
    }
}
