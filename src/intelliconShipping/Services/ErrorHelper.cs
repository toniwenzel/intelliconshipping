﻿using System;
using intellicon.Utilities.Services;

namespace intelliconShipping.Services
{
    public class ErrorHelper : IErrorHelper
    {
        public void HandleException(Exception exception)
        {
            ExceptionManager.TraceException(exception);

            var form = new FormError(exception);
            form.ShowDialog();
        }
    }
}
