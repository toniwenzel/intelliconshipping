﻿using System.Windows.Forms;

namespace intelliconShipping.Services
{
    public interface IMessageBoxService
    {
        DialogResult Show(string messageBoxText, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton);
    }
}
