﻿using System;
using System.Windows.Forms;
using intelliconShipping.Views;
using Microsoft.Extensions.DependencyInjection;

namespace intelliconShipping.Services
{
    public class PresenterFactory : IPresenterFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public PresenterFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
        }

        public T Show<T>(IPresenter parent) where T : IPresenter
        {
            var presenter = _serviceProvider.GetRequiredService<T>();
            var form = presenter.GetForm();
            //IOverlaySplashScreenHandle handle = null;

            //if (parent != null)
            //{
            //    //
            //    handle = SplashScreenManager.ShowOverlayForm(form);
            //}

            presenter.Init();

            if (form is IForm presenterForm)
                presenterForm.Init(presenter);

            if (parent != null)
            {
                var parentForm = parent.GetForm();

                if (form is Form window)
                {
                    AddFormClosingHandler(window, presenter);
                    window.ShowDialog(parentForm);
                }
                else if (parentForm is IFormContainer formContainer)
                {
                    formContainer.AddForm(form);
                }
                else
                {
                    form.Dock = DockStyle.Fill;
                    parentForm.Controls.Add(form);
                }
            }

            //if (parent != null)
            //    SplashScreenManager.CloseOverlayForm(handle);

            return presenter;
        }

        private void AddFormClosingHandler(Form window, IPresenter presenter)
        {
            FormClosingEventHandler handler = null;

            handler = new FormClosingEventHandler((s, e) =>
            {
                window.FormClosing -= handler;
                presenter.Closing();
            });

            window.FormClosing += handler;
        }
    }
}
