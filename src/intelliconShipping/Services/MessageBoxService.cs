﻿using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace intelliconShipping.Services
{
    public class MessageBoxService : IMessageBoxService
    {
        public DialogResult Show(string messageBoxText, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton)
        {
            return XtraMessageBox.Show(messageBoxText, caption, buttons, icon, defaultButton);
        }
    }
}
