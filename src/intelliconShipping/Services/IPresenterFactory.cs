﻿using intelliconShipping.Views;

namespace intelliconShipping.Services
{
    public interface IPresenterFactory
    {
        T Show<T>(IPresenter parent) where T : IPresenter;
    }
}
