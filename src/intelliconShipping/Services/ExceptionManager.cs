﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace intelliconShipping.Services
{
    public class ExceptionManager
    {
        public static ExceptionManager Instance { get; } = new ExceptionManager();

        public string TraceFilePath { get; private set; }

        private ExceptionManager()
        {
        }

        /// <summary>
        /// Initializes the ExceptionManager
        /// </summary>
        /// <param name="sourceName">Name of EventLog source</param>
        public void Init(string sourceName, bool addListeners, string fileListenerPath)
        {
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            CreateEventLog(sourceName);

            if (addListeners)
            {
                AddEventLogTraceListener(sourceName);
                AddFileTraceListener(fileListenerPath);
            }
        }

        public static void TraceException(Exception ex)
        {
            if (ex.Data.Contains("traced") && ((bool)ex.Data["traced"]))
                return;

            var error = new StringBuilder();

            error.AppendLine($"Date:                {DateTime.Now:dd/MM/yyyy HH:mm:ss}");
            error.AppendLine($"Version:             {ProgramVersion.Instance.VersionString} ({ProgramVersion.Instance.InformationalVersion})");
            error.AppendLine($"Exception class:     {ex.GetType().Name}");
            error.AppendLine($"Exception message:   {GetExceptionStack(ex)}");
            error.AppendLine($"Source:              {ex.Source}");

            error.Append(Environment.NewLine);
            error.AppendLine("Stack Trace:");
            error.Append(ex.StackTrace);

            error.Append(Environment.NewLine);

            var errorMessage = error.ToString();

            // Eventlog entries are limited
            if (errorMessage.Length > 32765)
                errorMessage = errorMessage.Substring(0, 32765);

            Trace.TraceError(errorMessage);

            MarkAsTraced(ex);
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            HandleException(e.ExceptionObject as Exception);
        }

        void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            HandleException(e.Exception);
        }

        private void HandleException(Exception exception)
        {
            TraceException(exception);

            var form = new FormError(exception);
            form.ShowDialog();
        }

        private static void CreateEventLog(string sourceName)
        {
            try
            {
                if (!EventSourceExists(sourceName))
                {
                    EventLog.CreateEventSource(sourceName, "intellicon");
                    EventLog log = new EventLog("intellicon");

                    // important, if too many entries may occur
                    log.ModifyOverflowPolicy(OverflowAction.OverwriteAsNeeded, 1);
                    log.Close();
                }
            }
            catch { }
        }

        private void AddFileTraceListener(string filename)
        {
            if (string.IsNullOrEmpty(filename))
            {
                filename = Path.GetDirectoryName(Application.ExecutablePath);
                filename = Path.Combine(filename, "Exceptions.txt");
            }

            System.Diagnostics.TextWriterTraceListener traceListener = new TextWriterTraceListener(filename);
            traceListener.TraceOutputOptions = TraceOptions.Callstack | TraceOptions.DateTime | TraceOptions.LogicalOperationStack | TraceOptions.ThreadId;
            traceListener.Name = "FileListener";

            Trace.Listeners.Add(traceListener);

            TraceFilePath = filename;
        }

        private static void AddEventLogTraceListener(string sourceName)
        {
            if (EventSourceExists(sourceName))
            {
                EventLogTraceListener eventLogTraceField = new EventLogTraceListener(sourceName);
                eventLogTraceField.TraceOutputOptions = TraceOptions.Callstack | TraceOptions.DateTime | TraceOptions.LogicalOperationStack | TraceOptions.ThreadId;
                eventLogTraceField.Name = "EventLogListener";
                Trace.Listeners.Add(eventLogTraceField);
            }
        }

        private static bool EventSourceExists(string sourceName)
        {
            // do not use "EventLog.SourceExists(sourceName)" because of missing permissions
            var logs = EventLog.GetEventLogs();

            var log = logs.SingleOrDefault(l => l.Source.ToLower() == sourceName.ToLower());

            return log != null;
        }

        private static string GetExceptionStack(Exception e)
        {
            var message = new StringBuilder();
            message.Append(e.Message);
            while (e.InnerException != null)
            {
                e = e.InnerException;
                message.Append(Environment.NewLine);
                message.Append(e.Message);
            }

            return message.ToString();
        }

        private static void MarkAsTraced(Exception ex)
        {
            ex.Data.Add("traced", true);

            if (ex.InnerException != null)
                MarkAsTraced(ex.InnerException);
        }
    }
}
