﻿using System;
using System.IO;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraSplashScreen;
using intellicon.Utilities.Services;
using intelliconShipping.Common;
using intelliconShipping.Core.Services;
using intelliconShipping.Services;
using intelliconShipping.Views.Login;
using Microsoft.Extensions.DependencyInjection;

namespace intelliconShipping
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ShowSplashScreen();

            ExceptionManager.Instance.Init("intellicon Shipping", true, Path.Combine(UserSettings.GetDefaultAppDataPath(), "Exceptions.log"));

            WindowsFormsSettings.SetDPIAware();
            WindowsFormsSettings.EnableFormSkins();
            WindowsFormsSettings.ForceDirectXPaint();
            SkinHelper.SetSkin(UserSettings.Default.SkinName, UserSettings.Default.SkinPaletteName);
            WindowsFormsSettings.DefaultRibbonStyle = DefaultRibbonControlStyle.Office2019;
            WindowsFormsSettings.FindPanelBehavior = FindPanelBehavior.Search;
            WindowsFormsSettings.FilterCriteriaDisplayStyle = FilterCriteriaDisplayStyle.Visual;
            WindowsFormsSettings.AllowPixelScrolling = DefaultBoolean.True;

            WindowsFormsSettings.CustomizationFormSnapMode = DevExpress.Utils.Controls.SnapMode.OwnerControl;
            WindowsFormsSettings.ColumnFilterPopupMode = ColumnFilterPopupMode.Excel;
            WindowsFormsSettings.AllowSkinEditorAttach = DefaultBoolean.True;


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var services = new ServiceCollection();
            ConfigureServices(services);

            using (var serviceProvider = services.BuildServiceProvider())
            {
                if (Login(serviceProvider) && CheckDbUpgrade(serviceProvider))
                {
                    // try to read old registry value into the DB
                    var settingsImporter = serviceProvider.GetRequiredService<SettingsImporter>();
                    settingsImporter.Import();

                    var presenterFactory = serviceProvider.GetRequiredService<IPresenterFactory>();
                    var mainPresenter = presenterFactory.Show<MainPresenter>(null);

                    HideSplashScreen();

                    Application.Run(mainPresenter.GetForm() as Form);
                }
            }
        }

        private static void ShowSplashScreen()
        {
            SplashScreenManager.ShowSkinSplashScreen(
                            title: "intellicon Shipping",
                            subtitle: "Easy shipping",
                            footer: $"Version {ProgramVersion.Instance.VersionString}. Copyright © 2021 intellicon GmbH." + Environment.NewLine + "All Rights reserved.",
                            loading: "Lade..."
                        );
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            ServiceHelper.RegisterServices(services);
        }

        private static bool Login(IServiceProvider serviceProvider)
        {
            // Shortcut -> check whether a valid login information is already present
            if (IsLoginDataValid(serviceProvider))
                return true;

            HideSplashScreen();

            var presenterFactory = serviceProvider.GetRequiredService<IPresenterFactory>();
            var loginPresenter = presenterFactory.Show<LoginPresenter>(null);

            var result = (loginPresenter.GetForm() as Form).ShowDialog() == DialogResult.OK;

            // Show splash screen after successful login
            if (result)
                ShowSplashScreen();

            return result;
        }

        private static void HideSplashScreen()
        {
            SplashScreenManager.CloseForm();
        }

        private static bool CheckDbUpgrade(ServiceProvider serviceProvider)
        {
            var dbUpdater = serviceProvider.GetRequiredService<IDbUpdater>();

            if (dbUpdater.IsUpgradeRequired())
            {
                if (XtraMessageBox.Show("Die Datenbank muss auf die aktuelle Version aktualisiert werden. Wollen Sie fortfahren?", "Datenbank aktualisieren", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    if (dbUpdater.UpgradeDatabase())
                    {
                        return true;
                    }
                    else
                    {
                        var filePathProvider = serviceProvider.GetRequiredService<IFilePathProvider>();
                        XtraMessageBox.Show($"Datenbank konnte leider nicht aktualisiert werden.\r\nBitte prüfen Sie die Log-Datei in {filePathProvider.GetLoggingPath()}.", "Fehler bei Datenbank-Aktualisierung", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                return false;
            }
            else
            {
                return true;
            }
        }

        private static bool IsLoginDataValid(IServiceProvider serviceProvider)
        {
            // If usersettings are empty then read initial values from old registry
            if (string.IsNullOrWhiteSpace(UserSettings.Default.DatabaseName) &&
                string.IsNullOrWhiteSpace(UserSettings.Default.DatabaseServer))
                UserSettings.Default.ReadFromOldRegistry();

            var login = new intellicon.Utilities.DataAccess.Login();
            login.AuthenticationType = UserSettings.Default.AuthenticationType;
            login.DatabaseName = UserSettings.Default.DatabaseName;
            login.DatabaseServer = UserSettings.Default.DatabaseServer;
            login.Username = UserSettings.Default.Username;
            login.Password = UserSettings.Default.Password;

            if (!string.IsNullOrWhiteSpace(login.DatabaseServer)
                && !string.IsNullOrWhiteSpace(login.DatabaseName)
                && !string.IsNullOrWhiteSpace(login.Password)
                && !string.IsNullOrWhiteSpace(login.Username)
                )
            {

                try
                {
                    var dbConnectionInfoService = serviceProvider.GetRequiredService<IDbConnectionInfoService>();

                    var isLoginValid = dbConnectionInfoService.IsValidLogin(login);

                    if (isLoginValid)
                    {
                        dbConnectionInfoService.UseLogin(login);
                        return true;
                    }
                }
                catch (Exception)
                {
                }
            }

            return false;
        }
    }
}
