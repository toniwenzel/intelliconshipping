﻿using System;
using DevExpress.XtraEditors;

namespace intelliconShipping
{
    public partial class FormError : XtraForm
    {
        private readonly Exception _exception;

        public FormError()
        {
            InitializeComponent();
        }

        public FormError(Exception ex)
            : this()
        {
            if (ex != null)
            {
                labelControlTitle.Text = string.Format(labelControlTitle.Text, ex.GetType().Name);
                memoEditDetails.Text = ex.ToString();
                Text += " - " + ex.GetType().Name;
            }

            _exception = ex;
        }
    }
}
