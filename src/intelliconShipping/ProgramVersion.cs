﻿using System;
using System.Reflection;
using intellicon.Utilities.Services;

namespace intelliconShipping
{
    public class ProgramVersion : IProgramVersion
    {
        public Version Version { get; }
        public string VersionString { get; }
        public string InformationalVersion { get; }

        public static ProgramVersion Instance { get; } = new ProgramVersion();

        private ProgramVersion()
        {
            var assembly = Assembly.GetEntryAssembly();

            if (assembly != null)
            {
                Version = assembly.GetName().Version;
                VersionString = GetVersionString(Version);
                InformationalVersion = GetInformationalVersion(assembly);
            }
            else
            {
                Version = new Version(9, 9);
                VersionString = "dev@designTime";
                InformationalVersion = "designtime";
            }
        }

        private static string GetInformationalVersion(Assembly assembly)
        {
            var attribute = assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>();

            if (attribute != null)
                return attribute.InformationalVersion;

            return string.Empty;
        }

        private static string GetVersionString(Version version)
        {
            if (version.Revision > 0)
                return $"{version.Major}.{version.Minor}.{version.Build}.{version.Revision}";

            if (version.Build > 0)
                return $"{version.Major}.{version.Minor}.{version.Build}";

            return $"{version.Major}.{version.Minor}";
        }
    }
}
