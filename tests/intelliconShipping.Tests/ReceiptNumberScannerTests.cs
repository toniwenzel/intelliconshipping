﻿using FluentAssertions;
using intelliconShipping.Core.Services;
using NUnit.Framework;

namespace intelliconShipping.Tests
{
    public class ReceiptNumberScannerTests
    {
        private ReceiptNumberScanner _scanner;

        [SetUp]
        public void Setup()
        {
            _scanner = new ReceiptNumberScanner();
        }

        [TestCase("1", 1)]
        [TestCase("44", 44)]
        public void Returns_Mandant(string mandandId, int expected)
        {
            var result = _scanner.Scan($"$M{mandandId}$I22");
            result.Mandant.Should().Be(expected);
        }

        [TestCase("$M1$I22", 1, 22, false)]
        [TestCase("$M44$I5", 44, 5, false)]
        [TestCase("$M44$I5678", 44, 5678, false)]
        [TestCase("$M44$I5678$E", 44, 5678, true)]
        [TestCase("$I22", 0, 22, false)]
        [TestCase("$I5678$E", 0, 5678, true)]
        public void Returns_Mandant_And_ReceiptId_And_Purchase(string text, int expectedMandant, int expectedReceipt, bool expectedPurchase)
        {
            var result = _scanner.Scan(text);
            result.Mandant.Should().Be(expectedMandant);
            result.ReceiptId.Should().Be(expectedReceipt);
            result.IsPurchase.Should().Be(expectedPurchase);
        }

        [TestCase("2016-123456", 2016, 123456, false)]
        [TestCase("E2021-12", 2021, 12, true)]
        [TestCase("201-123456", 0, 0, false)]
        public void Returns_ReceiptNumber(string text, short exprectedYear, int expectedNumber, bool expectedPurchase)
        {
            var result = _scanner.Scan(text);
            result.ReceiptNumber.Should().Be(expectedNumber);
            result.ReceiptYear.Should().Be(exprectedYear);
            result.IsPurchase.Should().Be(expectedPurchase);
        }
    }
}
