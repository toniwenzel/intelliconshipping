﻿using System.Reflection;
using FluentAssertions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using NUnit.Framework;

namespace intelliconShipping.Generators.Tests
{
    public class Tests
    {
        [Test]
        public void Add_PartialClass_With_Property()
        {
            // Create the 'input' compilation that the generator will act on
            Compilation inputCompilation = CreateCompilation(@"
namespace System.ComponentModel
{
 
    public interface INotifyPropertyChanged
    {       
        event PropertyChangedEventHandler? PropertyChanged;
    }
}

namespace MyCode
{   

    public partial class TestClass
    {
        [intelliconShipping.AutoNotify]
        public string _loginError;
    }
}
");

            var generatorResult = GenerateAndValidateOutput(inputCompilation);

            generatorResult.GeneratedSources[1].SourceText.ToString().Should().Be(@"
namespace MyCode
{
    public partial class TestClass : System.ComponentModel.INotifyPropertyChanged
    {
public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
public string LoginError 
{
    get 
    {
        return this._loginError;
    }
    set
    {
        this._loginError = value;
        this.PropertyChanged?.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs(nameof(LoginError)));
    }
}
} }");

        }

        private static GeneratorRunResult GenerateAndValidateOutput(Compilation inputCompilation)
        {
            // directly create an instance of the generator
            // (Note: in the compiler this is loaded from an assembly, and created via reflection at runtime)
            AutoNotifyGenerator generator = new AutoNotifyGenerator();

            // Create the driver that will control the generation, passing in our generator
            GeneratorDriver driver = CSharpGeneratorDriver.Create(generator);

            // Run the generation pass
            // (Note: the generator driver itself is immutable, and all calls return an updated version of the driver that you should use for subsequent calls)
            driver = driver.RunGeneratorsAndUpdateCompilation(inputCompilation, out var outputCompilation, out var diagnostics);

            // We can now assert things about the resulting compilation:
            diagnostics.IsEmpty.Should().BeTrue(); // there were no diagnostics created by the generators

            // Or we can look at the results directly:
            GeneratorDriverRunResult runResult = driver.GetRunResult();

            // The runResult contains the combined results of all generators passed to the driver
            runResult.GeneratedTrees.Should().HaveCount(2);
            runResult.Diagnostics.IsEmpty.Should().BeTrue();

            // Or you can access the individual results on a by-generator basis
            GeneratorRunResult generatorResult = runResult.Results[0];
            generatorResult.Generator.Should().Be(generator);
            generatorResult.Diagnostics.IsEmpty.Should().BeTrue();
            generatorResult.GeneratedSources.Should().HaveCount(2);
            generatorResult.Exception.Should().BeNull();
            return generatorResult;
        }

        [Test]
        public void Add_PartialClass_With_Property_Without_Interface()
        {
            // Create the 'input' compilation that the generator will act on
            Compilation inputCompilation = CreateCompilation(@"
namespace System.ComponentModel
{
    public interface INotifyPropertyChanged
    {
        event PropertyChangedEventHandler? PropertyChanged;
    }
}

namespace MyCode
{
    public class BaseClass :System.ComponentModel.INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;
    }

    public partial class TestClass :BaseClass
    {
        [intelliconShipping.AutoNotify]
        public string _loginError;
    }
}
");

            var generatorResult = GenerateAndValidateOutput(inputCompilation);

            generatorResult.GeneratedSources[1].SourceText.ToString().Should().Be(@"
namespace MyCode
{
    public partial class TestClass 
    {

public string LoginError 
{
    get 
    {
        return this._loginError;
    }
    set
    {
        this._loginError = value;
        this.PropertyChanged?.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs(nameof(LoginError)));
    }
}
} }");

        }

        [Test]
        public void Add_PartialClass_With_Property_And_OnChangedMethod()
        {
            // Create the 'input' compilation that the generator will act on
            Compilation inputCompilation = CreateCompilation(@"
namespace System.ComponentModel
{
 
    public interface INotifyPropertyChanged
    {       
        event PropertyChangedEventHandler? PropertyChanged;
    }
}

namespace MyCode
{   

    public partial class TestClass
    {
        [intelliconShipping.AutoNotify]
        public string _loginError;

        protected void OnLoginErrorChanged()
        {
        }
    }
}
");

            var generatorResult = GenerateAndValidateOutput(inputCompilation);

            generatorResult.GeneratedSources[1].SourceText.ToString().Should().Be(@"
namespace MyCode
{
    public partial class TestClass : System.ComponentModel.INotifyPropertyChanged
    {
public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
public string LoginError 
{
    get 
    {
        return this._loginError;
    }
    set
    {
        this._loginError = value;
        this.PropertyChanged?.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs(nameof(LoginError)));
        this.OnLoginErrorChanged();
    }
}
} }");

        }

        [Test]
        public void Add_PartialClass_With_Property_And_DependendProperty()
        {
            // Create the 'input' compilation that the generator will act on
            Compilation inputCompilation = CreateCompilation(@"
namespace System.ComponentModel
{
 
    public interface INotifyPropertyChanged
    {       
        event PropertyChangedEventHandler? PropertyChanged;
    }
}

namespace MyCode
{   

    public partial class TestClass
    {
        [intelliconShipping.AutoNotify]
        public string _loginError;

        [intelliconShipping.DependsOn(nameof(LoginError))]
        [intelliconShipping.DependsOn(nameof(LoginError))]
        public string OtherError => LoginError;
    }
}
");

            var generatorResult = GenerateAndValidateOutput(inputCompilation);

            generatorResult.GeneratedSources[1].SourceText.ToString().Should().Be(@"
namespace MyCode
{
    public partial class TestClass : System.ComponentModel.INotifyPropertyChanged
    {
public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
public string LoginError 
{
    get 
    {
        return this._loginError;
    }
    set
    {
        this._loginError = value;
        this.PropertyChanged?.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs(nameof(LoginError)));
        this.PropertyChanged?.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs(nameof(OtherError)));
        this.PropertyChanged?.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs(nameof(OtherError)));
    }
}
} }");

        }


        private static Compilation CreateCompilation(string source)
           => CSharpCompilation.Create("compilation",
               new[] { CSharpSyntaxTree.ParseText(source) },
               new[] { MetadataReference.CreateFromFile(typeof(Binder).GetTypeInfo().Assembly.Location) },
               new CSharpCompilationOptions(OutputKind.ConsoleApplication));
    }
}
